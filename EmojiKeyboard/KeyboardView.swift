//
//  KeyboardView.swift
//  EmojiKeyboard
//
//  Created by Marcelo on 8/6/18.
//  Copyright © 2018 AvilaTek. All rights reserved.
//

import UIKit

@IBDesignable
open class KeyboardView: UIView {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBInspectable
    open var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    open var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    open var numberOfRows: Int = 1 {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var keyboardProtocol: KeyboardProtocol?
    
    var emojis: [EmojisDetails] = []
    
    private var allNibs: [UIView]!
    
    private var view: UIView!
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
        setup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
        setup()
    }
    
    // MARK: - Private methods
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        addSubview(view)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "KeyboardView", bundle: bundle)
        
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    private func setup() {
        collectionView.register(UINib(nibName: "KeyboardCell", bundle: nil), forCellWithReuseIdentifier: "KeyboardCell")
    }
    
}

extension KeyboardView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return numberOfRows
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return emojis.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KeyboardCell", for: indexPath) as! KeyboardCell
        let imageName = emojis[indexPath.row].icon
        cell.imageView.image = UIImage(named: imageName)
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        keyboardProtocol?.addImage(emojiUri: emojis[indexPath.row].uri)
    }

}

extension KeyboardView: UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 55, height: 55)
    }
    
}
