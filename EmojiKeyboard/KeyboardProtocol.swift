//
//  KeyboardProtocol.swift
//  EmojiKeyboard
//
//  Created by Marcelo on 8/6/18.
//  Copyright © 2018 AvilaTek. All rights reserved.
//

import UIKit

//We define the protocol. We will add all methods in here
protocol KeyboardProtocol {
    func addImage(emojiUri: String)
}
