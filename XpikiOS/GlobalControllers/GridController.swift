//
//  GridControllers.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/24/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class GridController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //Grid collection variable
    var gridCollection: UICollectionView!
    
    //Index of the cell selected by long press
    var indexCell: IndexPath!
    
    //Navigation bar variable
    var navigationBar: UIView!
    
    //Search text (textfield) variable
    var searchText: TextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Cell layout configuration
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 2, left: 5, bottom: 5, right: 5)
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width/2 - 10, height: 100)
        
        //Grid collection configuration
        gridCollection = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        gridCollection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "CellIdentifier")
        gridCollection.backgroundColor = UIColor.white
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Shared.shared.currentXpikDefault.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellIdentifier", for: indexPath) as UICollectionViewCell
        
        let textLabel = UILabel(frame:CGRect(x: 5, y: 0, width: cell.frame.size.width - 10, height: cell.frame.size.height))
        textLabel.textColor = UIColor.white
        textLabel.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        let textForLabel = Shared.shared.currentXpikDefault[indexPath.row].title
        textLabel.numberOfLines = 5
        
        textLabel.text = textForLabel
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 2, left: 5, bottom: 5, right: 5)
        cell.contentView.addSubview(textLabel)
        
        //Creating dropdownButton
        let dropdownButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        //Centering dropdownButton button into backButtonContainer
        dropdownButton.center = CGPoint(x: textLabel.bounds.width, y: cell.bounds.height/2)
        //Setting icon color
        dropdownButton.setTitleColor(UIColor.white, for: .normal)
        //Setting icon size to dropdownButton
        dropdownButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 20)
        //Setting icon to dropdownButton
        dropdownButton.setTitle(String.fontAwesomeIcon(name: .sortDesc), for: .normal)
        //Adding dropdownButton to backButtonContainer
        //cell.contentView.addSubview(dropdownButton)
        
        cell.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        
        return cell
    }
    
    func headerSearchView() {
        
        //Creating navigation bar
        navigationBar = UIView(frame:CGRect(x: 0, y: 22, width: UIScreen.main.bounds.width, height: 65))
        //Setting the background color to the navigation bar
        navigationBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        
        /**
         * Cancel button
         */
        
        //Creating cancel container
        let cancelContainer = UIView(frame:CGRect(x: UIScreen.main.bounds.width - (UIScreen.main.bounds.width/6), y: 0, width: UIScreen.main.bounds.width/6, height: navigationBar.bounds.height))
        //Creating cancel button
        let cancelButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        //Centering cancelButton button into cancelContainer
        cancelButton.center = CGPoint(x: cancelContainer.bounds.width/2, y: cancelContainer.bounds.height/2)
        //Setting icon color
        cancelButton.setTitleColor(UIColor.white, for: .normal)
        //Setting icon size to cancelButton
        cancelButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 25)
        //Setting icon to cancelButton
        cancelButton.setTitle(String.fontAwesomeIcon(name: .close), for: .normal)
        //Adding cancelButton to cancelContainer
        cancelContainer.addSubview(cancelButton)
        //Setting target to cancel button of search view
        cancelButton.addTarget(self, action: #selector(cancelSearch), for: .touchUpInside)
        //Adding cancelContainer to navigationBar
        navigationBar.addSubview(cancelContainer)
        
        /**
         * Search input
         */
        
        //Creating search text view
        searchText = TextField(frame:CGRect(x: 0, y: 0, width: navigationBar.bounds.width - cancelContainer.bounds.width, height: navigationBar.bounds.height))
        //Centering searchText into navigationBar
        //searchText.center = CGPoint(x: 0, y: navigationBar.bounds.height/2)
        //Placing the searchText text and setting placeholder text color
        searchText.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        //Setting text field color
        searchText.textColor = UIColor.white
        //Open keyboard automatically
        searchText.becomeFirstResponder()
        //Adding search text event changes
        searchText.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        //Adding searchText into navigationBar
        navigationBar.addSubview(searchText)
        
        //Adding navigationBar to mainView
        self.view.addSubview(navigationBar)
        
    }
    
    /**
     * Search textfield changes event: detect the characters from searchText
     */
    
    func textFieldDidChange(_ textField: UITextField) {
        
        Shared.shared.currentXpikDefault = Shared.shared.dataCollectionArray
        
        // Check for empty fields
        if !(searchText.text?.isEmpty)! {
            
            let obarr = Shared.shared.currentXpikDefault.filter { $0.title.lowercased().contains(textField.text!.lowercased()) }
            Shared.shared.currentXpikDefault = obarr
        }
        
        gridCollection.reloadData()
    }
    
    /**
     * Cancel search method: close the search view
     */
    
    func cancelSearch() {
        
        // Check for empty fields
        if searchText.text?.isEmpty ?? true {
            
            navigationBar.removeFromSuperview()
            
        }else{
            
            searchText.text = ""
            Shared.shared.currentXpikDefault = Shared.shared.dataCollectionArray
            gridCollection.reloadData()
        }
        
    }
}
