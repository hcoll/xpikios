//
//  LaunchController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/10/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class LauncherController: UIViewController {
    
    //Splash screen view variable
    var splashView: UIView!
    
    //Tutorial 1 UIImageView variable
    var tutorial_1: UIImageView!
    
    //Tutorial 2 UIImageView variable
    var tutorial_2: UIImageView!
    
    //Tutorial 3 UIImageView variable
    var tutorial_3: UIImageView!
    
    //Tutorial 4 UIImageView variable
    var tutorial_4: UIImageView!
    
    //Language code variable
    var languageCode: String!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Or to rotate and lock
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    /**
     * Notifies the view controller that its view is about to be added to a view hierarchy
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //Setting language code with iso format
        languageCode = isoLanguageCode(languageCode: Shared.shared.currentLanguageCode)
        
        //Showing splash screen or tutorial
        if UserDefaults.standard.string(forKey: "tutorialEnabled") != nil{
            
            splashScreen()
            
        }else{
            
            tutorial1()
        }
    }
    
    /**
     * Splashscreen View
     */
    
    func splashScreen(){
        
        //Creating splashView variable
        splashView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Setting background color to splashView
        splashView.backgroundColor = UIColor.white
        //Adding splashView to principal view
        self.view.addSubview(splashView)
        
        /**
         * Xpik logo
         */
        
        //Creating logoImageView
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 120, height: 120))
        //Centering logoImageView button into titleContainer
        logoImageView.center = CGPoint(x: splashView.bounds.width/2, y: splashView.bounds.height/2)
        //Adding xpikLogoTextWhite to logoImageView
        logoImageView.image = UIImage(named:"xpik_logo")
        //Adding the logoImageView into the view
        splashView.addSubview(logoImageView)
        
        /**
         * Xpik bottom logo
         */
        
        //Creating bottomLogoImageView
        let bottomLogoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 70))
        //Centering bottomLogoImageView button into titleContainer
        bottomLogoImageView.center = CGPoint(x: splashView.bounds.width/2, y: splashView.bounds.height - 90)
        //Adding xpikLogoTextWhite to bottomLogoImageView
        bottomLogoImageView.image = UIImage(named:"bottom_logo")
        //Adding the logoImageView into the view
        splashView.addSubview(bottomLogoImageView)
        
        //Getting categories and libraries
        getCategories()
    
    }
    
    /**
     * Showing the first view of tutorial
     */
    
    func tutorial1(){
        
        //Creating tutorial 1 view
        tutorial_1 = UIImageView(frame: CGRect(x: 0, y: 22, width: self.view.bounds.width, height: self.view.bounds.height - 22))
        
        //Adding tutorial 1 image to view
        tutorial_1.image = UIImage(named:"tutorial_1_"+languageCode)
        tutorial_1.contentMode = .scaleAspectFit
        
        //Setting tap gesture to tutorial 1 view
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tutorial2))
        tutorial_1.isUserInteractionEnabled = true
        tutorial_1.addGestureRecognizer(tapGesture)
        
        //Adding the first view of tutorial
        self.view.addSubview(self.tutorial_1)
    }
    
    /**
     * Showing the second view of tutorial
     */
    
    func tutorial2(){
        
        //Removing tutorial 1 view
        tutorial_1.removeFromSuperview()
        
        //Creating tutorial 2 view
        tutorial_2 = UIImageView(frame: CGRect(x: 0, y: 22, width: self.view.bounds.width, height: self.view.bounds.height - 22))
        
        //Adding tutorial 2 image to view
        tutorial_2.image = UIImage(named:"tutorial_2_"+languageCode)
        tutorial_2.contentMode = .scaleAspectFit
        
        //Setting tap gesture to tutorial 2 view
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tutorial3))
        tutorial_2.isUserInteractionEnabled = true
        tutorial_2.addGestureRecognizer(tapGesture)
        
        //Adding the second view of tutorial
        self.view.addSubview(tutorial_2)
    }
    
    /**
     * Showing the third view of tutorial
     */
    
    func tutorial3(){
        
        //Removing tutorial 2 view
        tutorial_2.removeFromSuperview()
        
        //Creating tutorial 3 view
        tutorial_3 = UIImageView(frame: CGRect(x: 0, y: 22, width: self.view.bounds.width, height: self.view.bounds.height - 22))
        
        //Adding tutorial 3 image to view
        tutorial_3.image = UIImage(named:"tutorial_3_"+languageCode)
        tutorial_3.contentMode = .scaleAspectFit
        
        //Setting tap gesture to tutorial 3 view
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tutorial4))
        tutorial_3.isUserInteractionEnabled = true
        tutorial_3.addGestureRecognizer(tapGesture)
        
        //Adding the third view of tutorial
        self.view.addSubview(tutorial_3)
    }
    
    /**
     * Showing the fourth view of tutorial
     */
    
    func tutorial4(){
        
        //Removing tutorial 3 view
        tutorial_3.removeFromSuperview()
        
        //Creating tutorial 4 view
        tutorial_4 = UIImageView(frame: CGRect(x: 0, y: 22, width: self.view.bounds.width, height: self.view.bounds.height - 22))
        
        //Adding tutorial 4 image to view
        tutorial_4.image = UIImage(named:"tutorial_4_"+languageCode)
        tutorial_4.contentMode = .scaleAspectFit
        
        //Setting tap gesture to tutorial 4 view
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(final))
        tutorial_4.isUserInteractionEnabled = true
        tutorial_4.addGestureRecognizer(tapGesture)
        
        //Adding the fourth view of tutorial
        self.view.addSubview(tutorial_4)
    }
    
    /**
     * Tutorial finished
     */
    
    func final(){
        
        //Removing tutorial 4 view
        tutorial_4.removeFromSuperview()
        
        //Showinf splash screen
        splashScreen()
        
        //Setting tutorialEnabled to user defaults variable
        UserDefaults.standard.set("", forKey: "tutorialEnabled")

    }
    
    /**
     * Getting Categories from Firebase
     */
    
    func getCategories() {
        
        //Creating Firebase service instance
        let firebaseService = FirebaseService()
        
        Shared.shared.categories = []
        
        firebaseService.getLibraries()
        
        Shared.shared.homeController = HomePageController()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
            
            //Initilizing home page controller
            self.navigationController?.pushViewController(Shared.shared.homeController, animated: false)
        }
        
        
    }
}
