//
//  LanguagesTableViewController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 2/9/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class LanguagesTableViewController: UIViewController{
    
    //Language detail structure to populate the table view
    struct languagesDetail {
        let code: String,
            flag: String
    }
    
    //Languages array with languagesDetail attributes
    var languagesArray = [
        languagesDetail(code: Locale.current.languageCode!, flag: "ic_region"),
        languagesDetail(code: "en-US", flag: "ic_usa"),
        languagesDetail(code: "es", flag: "ic_spain"),
        languagesDetail(code: "it", flag: "ic_italy"),
        languagesDetail(code: "ru", flag: "ic_russia"),
        languagesDetail(code: "de", flag: "ic_germany"),
        languagesDetail(code: "fr", flag: "ic_france"),
        languagesDetail(code: "pt-PT", flag: "ic_portugal"),
        languagesDetail(code: "ar", flag: "ic_arabia"),
        languagesDetail(code: "ja", flag: "ic_japan"),
        languagesDetail(code: "hi", flag: "ic_india"),
        languagesDetail(code: "ko", flag: "ic_korea"),
        languagesDetail(code: "zh-Hant", flag: "ic_chiTr"),
        languagesDetail(code: "he", flag: "ic_hebrew"),
        languagesDetail(code: "tr", flag: "ic_turkey")
    ]
    
    //Languages array with languagesDetail attributes
    var flagsArray = [["ic_region","ic_usa","ic_spain"],["ic_italy","ic_russia","ic_germany"],["ic_france","ic_portugal","ic_arabia"],["ic_japan","ic_india","ic_korea"],["ic_chiTr","ic_turkey","ic_hebrew"],["ic_close_white"]]
    
    var languagesContainer: UIView!
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.languageDidChangeNotification),name: NSNotification.Name(rawValue: "LANGUAGE_DID_CHANGE"),object: nil)
        languageDidChange()
        
        //Setting transparent background to principal view
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0)
        
        //Creating a main view with all the elements involved
        let mainView = UIView(frame:CGRect(x: 0, y: 87, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 160))
        //Setting the background color (gradient) to the main view
        mainView.backgroundColor = UIColor.white
        
        //Creating a main view with all the elements involved
        languagesContainer = UIView(frame:CGRect(x: 0, y: 0, width: 180, height: 360))
        languagesContainer.center = CGPoint(x: mainView.frame.width/2, y: mainView.frame.height/2)
        mainView.addSubview(languagesContainer)
        
        //Initializing position row
        var yPosition: Int = 0
        let flagContainerHeight = 60
        
        //Adding flags container
        for i in 0..<6 {
            
            //Creating the container for flags
            let flagsContainer = UIView(frame:CGRect(x: 0, y: yPosition, width: 180, height: flagContainerHeight))
            //Adding flagsContainer to languagesContainer
            languagesContainer.addSubview(flagsContainer)
            
            //Initializing position column
            var xPosition: CGFloat = 0
            
            for j in 0..<flagsArray[i].count {
                
                //Creating the container for single flag
                let flagContainer = UIView(frame:CGRect(x: xPosition, y: 0, width: 60, height: 60))
                //Adding flagContainer to flagsContainer
                flagsContainer.addSubview(flagContainer)
                //Changing the x position
                xPosition = xPosition + flagContainer.frame.width
                
                if let index = languagesArray.index(where: {$0.flag == flagsArray[i][j]}) {
                    //Creating flagTapGesture and add target
                    let flagTapGesture = UITapGestureRecognizer(target: self, action: #selector(didSelectButton))
                    //Setting to flagsContainer isUserInteractionEnabled true
                    flagContainer.isUserInteractionEnabled = true
                    //Adding tag to flagsContainer
                    flagContainer.tag = index
                    //Adding to flagsContainer gesture
                    flagContainer.addGestureRecognizer(flagTapGesture)
                }
                
                //Creating the flag icon UIImageView
                let flagIcon = UIImageView(frame:CGRect(x: 0, y: 0, width: 45, height: 45))
                
                if(i == 5){
                    flagContainer.center = CGPoint(x: flagsContainer.frame.width/2, y: flagsContainer.frame.height/2)
                    flagIcon.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                    //Creating flagTapGesture and add target
                    let flagTapGesture = UITapGestureRecognizer(target: self, action: #selector(closeLanguageModal))
                    //Setting to flagsContainer isUserInteractionEnabled true
                    flagContainer.isUserInteractionEnabled = true
                    //Adding to flagsContainer gesture
                    flagContainer.addGestureRecognizer(flagTapGesture)
                }
                
                //Centering flagIcon in flagContainer
                flagIcon.center = CGPoint(x: flagContainer.bounds.width/2, y: flagContainer.bounds.height/2)
                //Adding flag to flagIcon
                flagIcon.image = UIImage(named: flagsArray[i][j])
                //Adding flagIcon to flagContainer
                flagContainer.addSubview(flagIcon)
                
            }
            
            //Changing the y position
            yPosition = yPosition + Int(flagsContainer.frame.height)
            
        }
        
        self.view.addSubview(mainView)
        
    }
    
    
    
    func languageDidChangeNotification(notification:NSNotification){
        languageDidChange()
    }
    
    func languageDidChange(){}
    
    
    /**
     * Select language method
     */
    
    @objc func didSelectButton(sender: UITapGestureRecognizer) {
       
        if sender.view!.tag != nil{
            
            if(Shared.shared.languageTableType){
                
                    changeLanguage(index: sender.view!.tag)
                }else{
                
                    changeSpeechLanguage(index: sender.view!.tag)
                }
            }else{
            
                closeLanguageModal()
            }
        
    }
    
    func changeLanguage(index: Int){
        
        //Creating Firebase service instance
        let firebaseService = FirebaseService()
        
        Shared.shared.categories = []
        
        firebaseService.getLibraries()
        
        var localeString:String?
        localeString = languagesArray[index].code
        
        if localeString != nil {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LANGUAGE_WILL_CHANGE"), object: localeString)
        }
        
        UserDefaults.standard.set(languagesArray[index].code, forKey: "currentLanguageCode")
        
        Shared.shared.currentLanguageCode = languagesArray[index].code
        
        closeLanguageModal()
        
        let auxHomeCont = Shared.shared.homeController
        
        Shared.shared.homeController = HomePageController()
        
        auxHomeCont?.navigationController?.pushViewController(Shared.shared.homeController, animated: false)
    }
    
    func changeSpeechLanguage(index: Int){
        
        UserDefaults.standard.set(languagesArray[index].code, forKey: "currentSpeechLanguageCode")
        
        Shared.shared.currentSpeechLanguageCode = languagesArray[index].code
        
        closeLanguageModal()
        
        let auxHomeCont = Shared.shared.homeController
        
        Shared.shared.homeController = HomePageController()
        
        auxHomeCont?.navigationController?.pushViewController(Shared.shared.homeController, animated: false)
    }
    
    func closeLanguageModal() {
        
        //Back to preferences controller
        navigationController?.popViewController(animated: false)
        
        //Dismiss current view controller
        dismiss(animated: true, completion: nil)
        
    }
}




/*//
//  LanguagesTableViewController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 2/9/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class LanguagesTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SSRadioButtonControllerDelegate {
    
    var tableView: UITableView!
    
    //Language detail structure to populate the table view
    struct languagesDetail {
        let radioButton: SSRadioButton
        let language: String
        let code: String
        let flag: String
    }

    //Languages array with languagesDetail attributes
    var languagesArray = [
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Default", comment: ""), code: Locale.current.languageCode!, flag: "ic_region"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("English", comment: ""), code: "en-US", flag: "ic_usa"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Spanish", comment: ""), code: "es", flag: "ic_spain"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Italian", comment: ""), code: "it", flag: "ic_italy"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Russian", comment: ""), code: "ru", flag: "ic_russia"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("German", comment: ""), code: "de", flag: "ic_germany"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("French", comment: ""), code: "fr", flag: "ic_france"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Portuguese", comment: ""), code: "pt-PT", flag: "ic_portugal"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Arabic", comment: ""), code: "ar", flag: "ic_arabia"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Japanese", comment: ""), code: "ja", flag: "ic_japan"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Hindi", comment: ""), code: "hi", flag: "ic_india"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Korean", comment: ""), code: "ko", flag: "ic_south_korea"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Traditional_Chinese", comment: ""), code: "zh-Hant", flag: "ic_chiTr"),
        languagesDetail(radioButton: SSRadioButton(), language: NSLocalizedString("Simplified_Chinese", comment: ""), code: "zh-Hans", flag: "ic_chiSm")
    ]
    
    var selectedIndex: IndexPath?
    
    var languagesContainer: UIView!
    
    var radioButtonController: SSRadioButtonsController?
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.languageDidChangeNotification),name: NSNotification.Name(rawValue: "LANGUAGE_DID_CHANGE"),object: nil)
        languageDidChange()
        
        radioButtonController = SSRadioButtonsController()
        
        //Setting transparent background to principal view
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Creating a main view with all the elements involved
        let mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 50, height: UIScreen.main.bounds.height - 100))
        //Centering mainView into principal view
        mainView.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        //Setting the background color (gradient) to the main view
        mainView.backgroundColor = UIColor.white
        //Adding corner radius to the mainView
        mainView.layer.cornerRadius = 40
        //Adding border width
        mainView.layer.borderWidth = 4
        //Adding border color
        mainView.layer.borderColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0).cgColor
        
        /**
         * Title UILabel
         */
        
        //Creating title UILabel
        let titleLabel = UILabel(frame: CGRect(x: 20, y: 20, width: mainView.bounds.width - 20, height: 30))
        //Aligning text into titleLabel
        titleLabel.textAlignment = .left
        //Setting font to text
        titleLabel.font = UIFont(name: "Helvetica-bold", size: 20)
        //Setting color to text
        titleLabel.textColor = UIColor.black
        //Setting text
        titleLabel.text = NSLocalizedString("Choose_Language", comment: "") 
        //Adding titleLabel to mainView
        mainView.addSubview(titleLabel)
        
        /**
         * Bottom Buttons container
         */
        
        //Creating a container for bottom buttons
        let bottomLabel = UIView(frame: CGRect(x: 0, y: mainView.bounds.height - 50, width: mainView.frame.width, height: 50))
        //Adding monthContainer to mainView
        mainView.addSubview(bottomLabel)
        
        /**
         * Accept button
         */
        
        //Creating cancel button
        let cancelButton = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        //Setting title to acceptButton
        cancelButton.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
        //Setting text color
        cancelButton.setTitleColor(UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0), for: .normal)
        //Centering cancel button into bottom label
        cancelButton.center = CGPoint(x: bottomLabel.frame.width/2, y: bottomLabel.frame.height/2)
        //Adding target to cancelButton
        cancelButton.addTarget(self, action: #selector(closeLanguageModal), for: .touchUpInside)
        //Adding acceptButton to bottomLabel
        bottomLabel.addSubview(cancelButton)
        
        //Creating a main view with all the elements involved
        languagesContainer = UIView(frame:CGRect(x: 0, y: titleLabel.frame.maxY + 10, width: mainView.bounds.width, height: mainView.bounds.height - titleLabel.bounds.height - bottomLabel.bounds.height - 20))
        mainView.addSubview(languagesContainer)
        
        tableView = UITableView(frame:CGRect(x: 0, y: 0, width: languagesContainer.bounds.width, height: languagesContainer.bounds.height))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //Setting none the separator style
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        languagesContainer.addSubview(self.tableView)
        
        self.view.addSubview(mainView)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        let rowHeight = languagesContainer.bounds.height / CGFloat(languagesArray.count)
        
        return rowHeight
    }
    
    /**
     * Return the number of rows in the section
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return languagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "Cell")
        
        
        //Creating a view with all the elements involved in the cell
        let cellView = UIView(frame:CGRect(x: 0, y: 0, width: cell.bounds.width, height: cell.bounds.height))
        cell.addSubview(cellView)
       
        if(Shared.shared.languageTableType){
            
            switchLanguage(userNotif: "currentLanguageCode")
            
        }else{
            
            var auxArr: [languagesDetail] = []
            
            for i in 0..<languagesArray.count-1{
                
                auxArr.append(languagesDetail(radioButton: SSRadioButton(), language: languagesArray[i].language, code: languagesArray[i].code, flag: languagesArray[i].flag))
            }
            auxArr.append(languagesDetail(radioButton: SSRadioButton(), language: "", code: "", flag: ""))
            auxArr.last?.radioButton.isHidden = true
            languagesArray = auxArr
            
            switchLanguage(userNotif: "currentSpeechLanguageCode")
            
        }
        
        //let radioButton = SSRadioButton(frame: CGRect(x: 20, y: 0, width: cellView.bounds.width, height: cellView.bounds.height))
        languagesArray[indexPath.row].radioButton.frame = CGRect(x: 20, y: 0, width: cellView.bounds.width, height: cellView.bounds.height)
        //Adding tag to radioButton
        languagesArray[indexPath.row].radioButton.tag = indexPath.row
        languagesArray[indexPath.row].radioButton.circleColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        radioButtonController?.addButton( languagesArray[indexPath.row].radioButton)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        
        let languageLabel = UILabel(frame: CGRect(x: 60, y: 0, width: cell.bounds.width - 160, height: cell.bounds.height))
        languageLabel.text = languagesArray[indexPath.row].language
        
        var width = 35
        var height = 35
        
        if indexPath.row > 0{
            width = 30
            height = 30
        }
        
        let flag = UIImageView(frame: CGRect(x: languagesContainer.frame.width - 2*CGFloat(width), y: 0, width: CGFloat(width), height: CGFloat(height)))
        flag.contentMode = .scaleAspectFit
        flag.image = UIImage(named: languagesArray[indexPath.row].flag)
        
        cellView.addSubview(languagesArray[indexPath.row].radioButton)
        
        cellView.addSubview(languageLabel)
        
        cellView.addSubview(flag)
        
        cell.addSubview(cellView)
        
        return cell
    }
    
    func switchLanguage(userNotif: String){
        
        switch UserDefaults.standard.string(forKey: userNotif) {
        case "default"?:
            //Selecting the radio button with the languages index selected
            languagesArray[0].radioButton.isSelected = true
        case "en-US"?:
            //Selecting the radio button with the languages index selected
            languagesArray[1].radioButton.isSelected = true
        case "es"?:
            //Selecting the radio button with the languages index selected
            languagesArray[2].radioButton.isSelected = true
        case "it"?:
            //Selecting the radio button with the languages index selected
            languagesArray[3].radioButton.isSelected = true
        case "ru"?:
            //Selecting the radio button with the languages index selected
            languagesArray[4].radioButton.isSelected = true
        case "de"?:
            //Selecting the radio button with the languages index selected
            languagesArray[5].radioButton.isSelected = true
        case "fr"?:
            //Selecting the radio button with the languages index selected
            languagesArray[6].radioButton.isSelected = true
        case "pt-PT"?:
            //Selecting the radio button with the languages index selected
            languagesArray[7].radioButton.isSelected = true
        case "ar"?:
            //Selecting the radio button with the languages index selected
            languagesArray[8].radioButton.isSelected = true
        case "ja"?:
            //Selecting the radio button with the languages index selected
            languagesArray[9].radioButton.isSelected = true
        case "hi"?:
            //Selecting the radio button with the languages index selected
            languagesArray[10].radioButton.isSelected = true
        case "ko"?:
            //Selecting the radio button with the languages index selected
            languagesArray[11].radioButton.isSelected = true
        case "zh-Hant"?:
            //Selecting the radio button with the languages index selected
            languagesArray[12].radioButton.isSelected = true
        case "zh-Hans"?:
            //Selecting the radio button with the languages index selected
            languagesArray[13].radioButton.isSelected = true
        default:
            //Selecting the radio button with the languages index selected
            languagesArray[0].radioButton.isSelected = true
        }
    }
    
    func languageDidChangeNotification(notification:NSNotification){
        languageDidChange()
    }
    
    func languageDidChange(){}

    
    /**
     * Select language method
     */
    
    func didSelectButton(selectedButton: UIButton?) {
        
        if selectedButton?.tag != nil{
            if(Shared.shared.languageTableType){
                
                changeLanguage(selectedButton: selectedButton)
            }else{
                
                changeSpeechLanguage(selectedButton: selectedButton)
            }
        }else{
            
            closeLanguageModal()
        }
    }
    
    func changeLanguage(selectedButton: UIButton?){
        
        //Creating Firebase service instance
        let firebaseService = FirebaseService()
        
        Shared.shared.categories = []
        
        firebaseService.getLibraries()
        
        var localeString:String?
        localeString = languagesArray[(selectedButton?.tag)!].code
        
        if localeString != nil {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LANGUAGE_WILL_CHANGE"), object: localeString)
        }
        
        UserDefaults.standard.set(languagesArray[(selectedButton?.tag)!].code, forKey: "currentLanguageCode")
        
        Shared.shared.currentLanguageCode = languagesArray[(selectedButton?.tag)!].code
        
        closeLanguageModal()
        
        let auxHomeCont = Shared.shared.homeController
        
        Shared.shared.homeController = HomePageController()
        
        auxHomeCont?.navigationController?.pushViewController(Shared.shared.homeController, animated: false)
    }
    
    func changeSpeechLanguage(selectedButton: UIButton?){
        
       UserDefaults.standard.set(languagesArray[(selectedButton?.tag)!].code, forKey: "currentSpeechLanguageCode")
        
        Shared.shared.currentSpeechLanguageCode = languagesArray[(selectedButton?.tag)!].code
        
        closeLanguageModal()
        
        let auxHomeCont = Shared.shared.homeController
        
        Shared.shared.homeController = HomePageController()
        
        auxHomeCont?.navigationController?.pushViewController(Shared.shared.homeController, animated: false)
    }
    
    func closeLanguageModal() {
        
        //Back to preferences controller
        navigationController?.popViewController(animated: false)
        
        //Dismiss current view controller
        dismiss(animated: true, completion: nil)
        
    }
}*/
