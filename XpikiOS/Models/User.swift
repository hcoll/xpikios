//
//  User.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 4/18/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation

class User {
    
    /**
     * Class Attributes
     */
    
    var name: String
    var email: String
    
    /**
     * Constructor
     */
    
    init(_ name: String, _ email: String) {
        
        self.name = name
        self.email = email
    }
}
