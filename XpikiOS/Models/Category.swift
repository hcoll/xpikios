//
//  Categories.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/2/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation

class Category {
    
    /**
     * Class Attributes
     */
    
    var name: String
    var subcategories: [Subcategory]
    
    /**
     * Constructor
     */
    
    init(_ name: String, _ subcategories: [Subcategory]) {
        
        self.name = name
        self.subcategories = subcategories
    }
}
