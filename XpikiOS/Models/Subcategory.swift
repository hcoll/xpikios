//
//  Subcategories.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/19/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation

class Subcategory {
    
    /**
     * Class Attributes
     */
    
    var name: String
    var libraries: [LibraryDetail]
    
    /**
     * Constructor
     */
    
    init(_ name: String, _ libraries: [LibraryDetail]) {
        
        self.name = name
        self.libraries = libraries
    }
}
