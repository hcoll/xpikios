//
//  Carousel.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/16/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import Foundation

class Carousel: NSObject, NSItemProviderWriting {
    
    
    
    /**
     * Class Attributes
     */
    
    var word: String
    var webpage: [String]
    var link: [String]
    var original: [String]
    var thumbnail: [String]
    var position: Int
    var type: Int
    var currentType: Int
    
    
    /**
     * Constructor
     */
    
    init(_ word: String, _ webpage: [String], _ link: [String], _ original: [String], _ thumbnail: [String], _ position: Int, _ type: Int, _ currentType: Int) {
        
        self.word = word
        self.webpage = webpage
        self.link = link
        self.original = original
        self.thumbnail = thumbnail
        self.position = position
        self.type = type
        self.currentType = currentType
    }
    
    public static var writableTypeIdentifiersForItemProvider: [String] {
        return [] // something here
    }
    
    public func loadData(withTypeIdentifier typeIdentifier: String, forItemProviderCompletionHandler completionHandler: @escaping (Data?, Error?) -> Swift.Void) -> Progress? {
        return nil // something here
    }
}
