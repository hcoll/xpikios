//
//  Favorites.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/10/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation

class DataCollection {
    
    /**
     * Class Attributes
     */
    
    var title: String
    var carousels: [Carousel]
    
    /**
     * Constructor
     */
    
    init(_ title: String, _ carousels: [Carousel]) {
        
        self.title = title
        self.carousels = carousels
    }
}
