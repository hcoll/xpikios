//
//  Libraries.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 2/26/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation

class Libraries {
    
    /**
     * Class Attributes
     */
    
    var id: String
    var carousels: [Carousel]
    
    /**
     * Constructor
     */
    
    init(_ id: String, _ carousels: [Carousel]) {
        
        self.id = id
        self.carousels = carousels
    }
}
