//
//  LibraryDetail.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/5/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation

//Library detail structure
struct LibraryDetail {
    let title: String
    let carousel: [Carousel]
}
