//
//  EmojiDetails.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 6/8/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation

//Emojis detail structure to populate the emojis collectionview
struct EmojisDetails {
    let uri: String
    let icon: String
}
