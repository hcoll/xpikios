//
//  FirebaseService.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/8/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import FirebaseFirestore
import SwiftyJSON
import FirebaseStorage

class FirebaseService{
    
    /**
     * Class Attributes
     */
    
    //Firestore instance
    let defaultStore = Firestore.firestore()
    
    /**
     * Constructor
     */
    
    init() {
     
        let settings = defaultStore.settings
        settings.areTimestampsInSnapshotsEnabled = true
        defaultStore.settings = settings
        
    }
    
    /**
     * Get xpik: Retrieve xpik of user from Firebase
     */
    
    func getXpik(isLibrary: Bool, user: [String: Any]) -> [Carousel]{
       
     var carousels: [Carousel] = []
        
       if let xpik = user["xpik"] as? [Any] {
                        
            // access xpik values by key
            for carousel in xpik {
                
                var links: [String] = []
                
                var thumbnails: [String] = []
                
                var webpages: [String] = []
                
                var word: String!
                
                var position: Int!
                
                var type: Int!
                
                // access to each carousel values by key
                if let carouselValues = carousel as? [String: Any] {
                    
                    if let link = carouselValues["link"] as? [String] {
                        
                        // access to each link value by key
                        for linkValue in link {
                            
                            links.append(linkValue)
                        }
                    }
                    
                    if let thumbnail = carouselValues["thumbnail"] as? [String] {
                        
                        // access to each thumbnail value by key
                        for thumbnailValue in thumbnail {
                            
                            thumbnails.append(thumbnailValue)
                        }
                    }
                    
                    if let webpage = carouselValues["webpage"] as? [String] {
                        
                        // access to each webpage value by key
                        for webpageValue in webpage {
                            
                            webpages.append(webpageValue)
                        }
                    }
                    
                    if isLibrary{
                        
                        word = "val"
                        
                    }else{
                        
                        if let wordValue = carouselValues["word"] as? String {
                            
                            // access to word value by key
                            word = wordValue
                        }
                    }
                    
                    if let positionValue = carouselValues["position"] as? Int {
                        
                        // access to position value by key
                        position = positionValue
                    }
                    
                    if let typeValue = carouselValues["type"] as? Int {
                        
                        // access to type value by key
                        type = typeValue
                    }
                }
                
                carousels.append(Carousel(word, webpages, links, links, thumbnails, position, type, type))
                
            }
        
        }else {print("Document does not exist")}
        
        return carousels
    }
    
    /**
     * Get libraries: Retrieve libraries from Firebase
     */
    
    func getLibraries(){
        
        var librariesArray: [Libraries] = []
        
        //Getting libraries data
        let librariesRef = defaultStore.collection("libraries")
        
        //Retrieving data from document libraries
        librariesRef.getDocuments() { (querySnapshot, error) in
            
            for document in querySnapshot!.documents {
                
                librariesArray.append(Libraries(document.documentID, self.getXpik(isLibrary: true, user: document.data())))
            }
            
            self.getCategories(librariesArray: librariesArray)
        }
    }
    
    /**
     * Get categories: Retrieve categories from Firebase
     */
    
    func getCategories(librariesArray: [Libraries]){
        
        //Setting speech language
        var currentLanguageCode: String!
        if let usercurrentLanguageCode = UserDefaults.standard.string(forKey: "currentLanguageCode"){
            currentLanguageCode = usercurrentLanguageCode
        }else{
            
            currentLanguageCode = Locale.current.languageCode!
        }
        
        //Getting categories data
        let categoriesRef = defaultStore.collection("languages").document(convertToISO639(languageCode: currentLanguageCode!)).collection("categories")
        
        categoriesRef.getDocuments() { (category, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                
                var categoriesArray: [String] = []
                
                for category in category!.documents {
                    
                    categoriesArray.append(category.documentID)
                    
                }
                
                self.getSubcategories(rootRef: categoriesRef, categoriesArray: categoriesArray, librariesArray: librariesArray)
            }
        }
    }
    
    /**
     * Convert To ISO 639 method: Convert the language code to ISO 639
     */
    
    func convertToISO639(languageCode: String) -> String{

        switch languageCode {
            case "en-US":
                return "en"
            case "de":
                return "en"
            case "pt-PT":
                return "pt"
            case "zh-Hant":
                return "zh"
            case "zh-Hans":
                return "zh_TW"
            default:
                return languageCode
            }
    }
    
    /**
     * Get Subcategories: Retrieve subcategories from Firebase
     */
    
    func getSubcategories(rootRef: CollectionReference, categoriesArray: [String], librariesArray: [Libraries]){
        
        for category in categoriesArray{
            
            //Getting subcategories data
            let subcategoriesRef = rootRef.document(category).collection("subcategories")
            
            subcategoriesRef.getDocuments() { (subcategory, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    
                    var subcategoriesArray: [String] = []
                    
                    for subcategory in subcategory!.documents {
                        
                        subcategoriesArray.append(subcategory.documentID)
                    }
                    
                    self.getReferences(subcategoriesRef: subcategoriesRef, category: category, subcategoriesArray: subcategoriesArray, librariesArray: librariesArray)
                }
            }
        }
        
    }
    
    /**
     * Get references: Retrieve references from Firebase
     */
    
    func getReferences(subcategoriesRef: CollectionReference, category: String, subcategoriesArray: [String], librariesArray: [Libraries]){

        var subcategoriesArr: [Subcategory] = []
        var cont = -1
        
        for subcategory in subcategoriesArray {
            
            //Getting references data
            let referencesRef = subcategoriesRef.document(subcategory).collection("references")
 
            referencesRef.getDocuments() { (reference, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    cont = cont + 1
                    var libArray: [LibraryDetail] = []
                    
                    for reference in reference!.documents {
                        
                        if let reference = reference.data() as? [String: Any] {
                            
                            if let references = reference["references"] as? [Any] {
                                
                                // access reference values by key
                                for referenceValues in references {
                                    
                                    // access to each reference object by key
                                    if let referenceObj = referenceValues as? [String: Any] {
                                        
                                        var titleLib: String!
                                        
                                        if let title = referenceObj["title"] as? String {
                                            titleLib = title
                                        }
                                        
                                        if let reference_id = referenceObj["reference_id"] as? String {
                                            
                                            for libraryObj in librariesArray {
                                                
                                                if (reference_id == libraryObj.id){
                                                    
                                                    libArray.append(LibraryDetail(title: titleLib, carousel: libraryObj.carousels))
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    subcategoriesArr.append(Subcategory(subcategory, libArray))
                    
                    if((cont + 1) == (subcategoriesArray.count)){
                        self.addToCategories(category: category, subcategoriesArray: subcategoriesArr)
                    }
                }
            }
        }
    }
    
    /**
     * Add To Categories: Adding information to categories array
     */
    
    func addToCategories(category: String, subcategoriesArray: [Subcategory]){
        
        Shared.shared.categories.append(Category(category,subcategoriesArray))
    }
    
    /**
     * Upload image: Upload image to firebase
     */

    static func uploadImage(_ image: UIImage, at reference: StorageReference, completion: @escaping (URL?) -> Void) {
       
        guard let imageData = UIImageJPEGRepresentation(image, 1) else {
            return completion(nil)
        }
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        reference.putData(imageData, metadata: metadata, completion: { (metadata, error) in
          
            if let error = error {
                assertionFailure(error.localizedDescription)
                return completion(nil)
            }
            
            completion(metadata?.downloadURL())
        })
    }
    
}
