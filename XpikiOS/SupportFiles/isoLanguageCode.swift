//
//  isoLanguageCode.swift
//  XpikiOS
//
//  Created by Xpik Developer on 8/8/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation

/**
 * Convert To ISO 639 method: Convert the language code to ISO 639
 */

func isoLanguageCode(languageCode: String) -> String {
    
    switch languageCode {
    case "en-US":
        return "en"
    case "de":
        return "en"
    case "pt-PT":
        return "pt"
    case "zh-Hant":
        return "zh"
    case "zh-Hans":
        return "tw"
    default:
        return languageCode
    }
    
}
