//
//  AppDelegate.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/16/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import UIKit
import Firebase
import Fabric
import Crashlytics
import Siren

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    //Declaring window variable
    var window: UIWindow?
    
    //Declaring navigation controller variable
    var navigationController : UINavigationController!
    
    /// set orientations you want to be allowed in this property by default
    var orientationLock = UIInterfaceOrientationMask.all
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        //App configuration
        config()
        
        //Launcher
        launcher()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        Siren.shared.checkVersion(checkType: .immediately)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        /*
         Useful if user returns to your app from the background after being sent to the
         App Store, but doesn't update their app before coming back to your app.
         
         ONLY USE WITH Siren.AlertType.immediately
         */
        
        Siren.shared.checkVersion(checkType: .immediately)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        /*
         Perform daily (.daily) or weekly (.weekly) checks for new version of your app.
         Useful if user returns to your app from the background after extended period of time.
         Place in applicationDidBecomeActive(_:).    */
        
        Siren.shared.checkVersion(checkType: .daily)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
   
    /**
     * Set speech language method
     */
    
    func setSpeechLanguage() {
        
        if let userCurrentSpeechLanguageCode = UserDefaults.standard.string(forKey: "currentSpeechLanguageCode"){
            
            Shared.shared.currentSpeechLanguageCode = userCurrentSpeechLanguageCode

        }else{
            
            Shared.shared.currentSpeechLanguageCode = Locale.current.languageCode!

        }
    }
    
    /**
     * Set app language method
     */
    
    func setAppLanguage() {
        
        if let userCurrentLanguageCode = UserDefaults.standard.string(forKey: "currentLanguageCode"){
            
            Shared.shared.currentLanguageCode = userCurrentLanguageCode

        }else{
            
            Shared.shared.currentLanguageCode = Locale.current.languageCode!

        }
    }
    
    /**
     * App language notification: change the app language
     */
    
    @objc func languageWillChange(notification:NSNotification){
        let targetLang = notification.object as! String
        UserDefaults.standard.set(targetLang, forKey: "selectedLanguage")
        Bundle.setLanguage(targetLang)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LANGUAGE_DID_CHANGE"), object: targetLang)

    }
    
    /**
     * Configuration method: Set
     */
    
    func config() {
        
        // Siren config
        let siren = Siren.shared
        siren.delegate = self
        siren.showAlertAfterCurrentVersionHasBeenReleasedForDays = 0
        siren.checkVersion(checkType: .immediately)
        
        //Crashlytics initializing
        Fabric.with([Crashlytics.self])
        
        //Firebase configuration: get the firebase project information
        FirebaseApp.configure()
        
        //Status bar style
        UIApplication.shared.statusBarStyle = .lightContent
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.languageWillChange),name: NSNotification.Name(rawValue: "LANGUAGE_WILL_CHANGE"),object: nil)

        let targetLang = UserDefaults.standard.object(forKey: "selectedLanguage") as? String
        Bundle.setLanguage((targetLang != nil) ? targetLang : Locale.current.languageCode!)

        //Set speech language
        setSpeechLanguage()
        
        //Set app language
        setAppLanguage()
    }
    
    /**
     * Launcher method: Initializing launcher controller
     */
    
    func launcher() {
       
        let launcherController = LauncherController()
        
        //Setting background color to navigation bar
        navigationController = UINavigationController(rootViewController: launcherController)
        
        //Setting navigation bar color (orange)
        navigationController.navigationBar.barTintColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        
        window?.rootViewController = navigationController
    }
    
}

extension AppDelegate: SirenDelegate {
    // Returns a localized message to this delegate method upon performing a successful version check
    func sirenDidDetectNewVersionWithoutAlert(message: String, updateType: UpdateType) {
        print("\(message)")
    }
    
}
