//
//  TextField.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/22/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import Foundation
import UIKit

/**
 * This class setup the padding to any Text Field
 */

class TextField: UITextField {
    
    //Setup padding
    let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0);
    
    //Set the toolbar to keyboard with 'Ok' button
    func settingToolBar(textField: UITextField){
        
        //Creating toolbar
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: UIScreen.main.bounds.width, height: 30))
        //Creating left side empty space so that done button set on right side
        let flexSpace = UIBarButtonItem(barButtonSystemItem:    .flexibleSpace, target: nil, action: nil)
        //Creating doneButton
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Ok", comment: ""), style: .done, target: self, action: #selector(doneButtonAction))
        //Setting flexSpace to toolbar
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        //Sizing toolbar to fit
        toolbar.sizeToFit()
        //Setting toolbar as inputAccessoryView
        textField.inputAccessoryView = toolbar
    }
    
    //Dismiss keyboard
    @objc func doneButtonAction() {
        endEditing(true)
    }
    
    //Returns the drawing rectangle for the text field’s text.
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    //Returns the drawing rectangle for the text field’s placeholder text
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    //Returns the rectangle in which editable text can be displayed
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
}
