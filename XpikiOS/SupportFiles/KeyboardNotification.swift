//
//  keyboardNotification.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/16/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class KeyboardNotification {
    
    /**
     * Class Attributes
     */
    
    var moveView: UIView
    
    var offsetY:CGFloat = 0
    
    /**
     * Constructor
     */
    
    init(_ moveView: UIView) {
        
        self.moveView = moveView
    }
    
    /**
     * Keyboard frame change notification: move the view when keyboard is open
     */
    
    func keyboardFrameChangeNotification(notification: Notification) {
        
        if let userInfo = notification.userInfo {
            let endFrame = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect
            let animationDuration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double ?? 0
            let animationCurveRawValue = (userInfo[UIKeyboardAnimationCurveUserInfoKey] as? Int) ?? Int(UIViewAnimationOptions.curveEaseInOut.rawValue)
            let animationCurve = UIViewAnimationOptions(rawValue: UInt(animationCurveRawValue))
            if let _ = endFrame, endFrame!.intersects(self.moveView.frame) {
                offsetY = self.moveView.frame.maxY - endFrame!.minY
                UIView.animate(withDuration: animationDuration, delay: TimeInterval(0), options: animationCurve, animations: {
                    self.moveView.frame.origin.y = self.moveView.frame.origin.y - self.offsetY
                }, completion: nil)
            } else {
                if offsetY != 0 {
                    UIView.animate(withDuration: animationDuration, delay: TimeInterval(0), options: animationCurve, animations: {
                        self.moveView.frame.origin.y = self.moveView.frame.origin.y + self.offsetY
                        self.offsetY = 0
                    }, completion: nil)
                }
            }
        }
    }
    
}
