//
//  NodeButton.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 6/28/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class NodeButton: UIButton {
    
    var yPosition: CGFloat?
    var cellFrame: CGRect?
    
}
