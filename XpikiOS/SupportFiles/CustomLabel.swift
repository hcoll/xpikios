//
//  CustomLabel.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/23/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import Foundation
import UIKit

/**
 * This class setup the padding to any Text Field
 */

class CustomLabel: UILabel {
    
    //Setup padding
    let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0);
    
    override func drawText(in bounds: CGRect){
        super.drawText(in: UIEdgeInsetsInsetRect(bounds, padding))
    }

}
