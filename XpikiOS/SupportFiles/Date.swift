//
//  Date.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/27/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import Foundation
/**
 * Date class
 */

extension Date {
    
    /**
     * This method return the name of the month
     */
    
    func getMonthName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    
    func getDayMonth() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d/MM"
        let strDayMonth = dateFormatter.string(from: self)
        return strDayMonth
        
    }
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}
