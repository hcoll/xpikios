//
//  NSBundle+Language.h
//  XpikiOS
//
//  Created by Antonio Nardi on 3/24/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)
+(void)setLanguage:(NSString*)language;

@end
