//
//  DetectLanguage.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/24/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation

extension String {
    func detectLanguage() -> String {
        let length = self.utf16.count
        let languageCode = CFStringTokenizerCopyBestStringLanguage(self as CFString, CFRange(location: 0, length: length)) as String? ?? ""
        
        let locale = Locale(identifier: languageCode)
        return locale.localizedString(forLanguageCode: languageCode) ?? "Unknown"
    }
}
