//
//  Shared.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/1/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

final class Shared {
    static let shared = Shared()
    
    //Home controller shared variable
    var homeController: HomePageController!
    
    //Editor controller shared variable
    var editorController: EditorController!
    
    //Library controller shared variable
    var libraryController: LibrariesViewController!
    
    //Current controller shared variable
    var currentController: UIViewController!
    
    //Current carousel shared variable
    var currentCarousel: [Carousel] = []
    
    //Current carousel home shared variable
    var currentHomeCarousel: [Carousel] = []
    
    //Carousels shared variable
    //var carousels: [Carousel] = []
    
    //Current xpikDefault shared variable
    var currentXpikDefault: [DataCollection] = []
    
    //Data collection array
    var dataCollectionArray: [DataCollection] = []
    
    //Collection data index selected shared variable
    var collectionDataIndex: Int!
    
    //Categories shared variable
    var categories: [Category]!
    
    //Language table type variable
    var languageTableType: Bool!
    
    //In homepage variable
    var inHomePage: Bool = true
    
    //Hidden nodes variable
    var hiddenNodes: Bool = false
    
    //Current language code
    var currentLanguageCode: String = "en"
    
    //Current speech language code
    var currentSpeechLanguageCode: String = "en"
}
