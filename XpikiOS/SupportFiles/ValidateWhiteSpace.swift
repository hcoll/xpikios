//
//  ValidateWhiteSpace.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 4/11/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

func validate(textView: UITextField) -> Bool {
    guard let text = textView.text,
        !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
            // this will be reached if the text is nil (unlikely)
            // or if the text only contains white spaces
            // or no text at all
            return false
    }
    
    return true
}
