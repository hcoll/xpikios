//
//  SideMenuView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/8/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class SideMenuView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Black view variable
    var blackView: UIView!
    
    //Side view variable
    var sideView: UIView!
    
    //Menu detail structure to populate the table view
    struct menuDetail {
        let icon: String
    }
    
    //Layouts
    let menuViews = [
        menuDetail(icon: "ic_close"),
        menuDetail(icon: "ic_home_gray"),
        menuDetail(icon: "ic_categories"),
        menuDetail(icon: "ic_settings"),
        menuDetail(icon: "ic_help")
    ]
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Black view
        blackView = UIView(frame:CGRect(x: -UIScreen.main.bounds.width, y: 22, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        //Side view frame
        sideView = UIView(frame: CGRect(x: -UIScreen.main.bounds.width, y: 22, width: 80, height: 330))
        sideView.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        
        /**
         * Menu pages
         */
        
        //Initializing position row
        var yPosition: CGFloat = 0
        
        //Adding menu pages container
        for i in 0..<menuViews.count {
            
            //Creating the container for menu pages
            let menuPagesContainer = UIView(frame:CGRect(x: 0, y: yPosition, width: sideView.bounds.width, height: 65))
            //Adding menuPagesContainer to sideView
            sideView.addSubview(menuPagesContainer)
            
            //Creating menuTapGesture and add target
            let menuTapGesture = UITapGestureRecognizer(target: self, action: #selector(changeView))
            //Setting to menuPagesContainer isUserInteractionEnabled true
            menuPagesContainer.isUserInteractionEnabled = true
            //Adding to menuPagesContainer gesture
            menuPagesContainer.addGestureRecognizer(menuTapGesture)
            //Adding tag to menuPagesContainer
            menuPagesContainer.tag = i
            
            //Creating the menu page logo UIImageView
            var menuLogo: UIImageView!
            //Configuring menu logo width/height
            menuLogo = UIImageView(frame:CGRect(x: 0, y: 0, width: 60, height: 60))
            //Centering menuLogo in menuPagesContainer
            menuLogo.center = CGPoint(x: menuPagesContainer.bounds.width - 30, y: menuPagesContainer.bounds.height - 20)
            //Adding logo from menuViews array to menuLogo
            menuLogo.image = UIImage(named: menuViews[i].icon)
            //Adding menuLogo to menuPagesContainer
            menuPagesContainer.addSubview(menuLogo)
            
            //Highlight home page menu
            if (i == 1){
                //Adding background color to home page menu
                menuPagesContainer.backgroundColor = UIColor.gray
            }else if(i == 2){
                //Adding background color to categories page menu
                menuPagesContainer.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
            }
            
            //Changing the y position
            yPosition = yPosition + menuPagesContainer.frame.height
        }
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func changeView(sender: UITapGestureRecognizer) {
        
        let tag = sender.view!.tag
        
        Shared.shared.homeController.changeView(menu: tag)
        
    }
}


/*//
//  SideMenuView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/8/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class SideMenuView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Black view variable
    var blackView: UIView!
    
    //Side view variable
    var sideView: UIView!
    
    //Menu detail structure to populate the table view
    struct menuDetail {
        let title: String,
        icon: String
    }
    
    //Layouts
    let menuViews = [
        menuDetail(title: NSLocalizedString("Homepage", comment: ""), icon: "menu_home"),
        menuDetail(title: NSLocalizedString("Libraries", comment: ""), icon: "ic_categories"),
        menuDetail(title: NSLocalizedString("Preferences", comment: ""), icon: "menu_settings"),
        menuDetail(title: NSLocalizedString("Help", comment: ""), icon: "ic_help")
    ]
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Black view
        blackView = UIView(frame:CGRect(x: -UIScreen.main.bounds.width, y: 22, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        //Side view frame
        sideView = UIView(frame: CGRect(x: -UIScreen.main.bounds.width, y: 22, width: 250, height: 300))
        sideView.backgroundColor = UIColor.white
        
        /**
         * User info
         */
        
        // Creating menu header container
        let menuHeaderContainer = UIView(frame: CGRect(x: 0, y: 0, width: sideView.frame.width, height: 120))
        //Adding background color to menuHeaderContainer
        menuHeaderContainer.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding menuHeaderContainer to sideView
        sideView.addSubview(menuHeaderContainer)
        
        /**
         * xpikLogoTextContainer with the xpikLogoText
         */
        
        //Creating the container for the xpikLogoText
        let xpikLogoTextContainer = UIView(frame:CGRect(x: 0, y: 10, width: menuHeaderContainer.bounds.width, height: 80))
        //Creating the xpikLogoText UIImageView
        let xpikLogoText = UIImageView(frame:CGRect(x: 0, y: 0, width: 140, height: 80))
        //Setting scale to logo image
        xpikLogoText.contentMode = .scaleAspectFit
        //Centering the xpikLogoText into the xpikLogoTextContainer
        xpikLogoText.center = CGPoint(x: 80, y: xpikLogoTextContainer.center.y)
        //Adding xpikLogoTextWhite to xpikLogoText
        xpikLogoText.image = UIImage(named:"ic_xpik_white")
        //Adding the xpikLogoText into the xpikLogoTextContainer
        xpikLogoTextContainer.addSubview(xpikLogoText)
        //Adding xpikLogoTextContainer to menuHeaderContainer
        menuHeaderContainer.addSubview(xpikLogoTextContainer)
        
        /**
         * Menu container
         */
        
        //Creating the container for the menu
        let menuContainer = UIView(frame:CGRect(x: 0, y: menuHeaderContainer.frame.maxY, width: sideView.bounds.width, height: 170))
        //Adding menuContainer to sideView
        sideView.addSubview(menuContainer)
        
        /**
         * Menu pages
         */
        
        //Initializing position row
        var yPosition: CGFloat = menuHeaderContainer.frame.maxY
        
        //Adding menu pages container
        for i in 0..<menuViews.count {
            
            //Creating the container for menu pages
            let menuPagesContainer = UIView(frame:CGRect(x: 0, y: yPosition, width: sideView.bounds.width, height: menuContainer.frame.height/CGFloat(menuViews.count)))
            //Adding menuPagesContainer to sideView
            sideView.addSubview(menuPagesContainer)
            
            //Creating menuTapGesture and add target
            let menuTapGesture = UITapGestureRecognizer(target: self, action: #selector(changeView))
            //Setting to menuPagesContainer isUserInteractionEnabled true
            menuPagesContainer.isUserInteractionEnabled = true
            //Adding to menuPagesContainer gesture
            menuPagesContainer.addGestureRecognizer(menuTapGesture)
            //Adding tag to menuPagesContainer
            menuPagesContainer.tag = i
            
            //Creating the menu page logo UIImageView
            var menuLogo: UIImageView!
            
            //Configuring menu logo width/height
            menuLogo = UIImageView(frame:CGRect(x: 0, y: 0, width: 42, height: 42))
            
            if(i == 0 || i == 3){
                
                //Configuring menu logo width/height
                menuLogo = UIImageView(frame:CGRect(x: 0, y: 0, width: 30, height: 30))
            
            }else{
                
                //Configuring menu logo width/height
                menuLogo = UIImageView(frame:CGRect(x: 0, y: 0, width: 42, height: 42))
            }
            
            //Centering menuLogo in menuPagesContainer
            menuLogo.center = CGPoint(x: 25, y: menuPagesContainer.bounds.height/2)
            //Adding logo from menuViews array to menuLogo
            menuLogo.image = UIImage(named: menuViews[i].icon)
            //Adding menuLogo to menuPagesContainer
            menuPagesContainer.addSubview(menuLogo)
            
            //Creating pageLabel UILabel
            let pageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: menuPagesContainer.bounds.width - 60, height: menuPagesContainer.bounds.height))
            //Centering pageLabel in menuPagesContainer
            pageLabel.center = CGPoint(x: menuPagesContainer.center.x + 30, y: menuPagesContainer.bounds.height/2)
            //Setting font to text
            pageLabel.font = UIFont(name: "Quicksand-bold", size: 16)
            //Setting text to TextView
            pageLabel.text = menuViews[i].title
            //Adding pageLabel into menuPagesContainer
            menuPagesContainer.addSubview(pageLabel)
            
            //Highlight home page menu
            if (i == 0){
                
                //Adding background color to home page menu
                menuPagesContainer.backgroundColor = UIColor.gray.withAlphaComponent(0.2)
                //Setting home page text color
                pageLabel.textColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
            }
            
            //Changing the y position
            yPosition = yPosition + menuPagesContainer.frame.height
        }
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func changeView(sender: UITapGestureRecognizer) {
        
        let tag = sender.view!.tag
    
        Shared.shared.homeController.changeView(menu: tag)
        
    }
}*/
