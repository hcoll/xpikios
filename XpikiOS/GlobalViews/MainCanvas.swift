//
//  MainCanvas.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/23/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class MainCanvas: UIView {
    
    /**
     * Class Attributes
     */
    
    //Canvas main view variable
    var mainView: UIView!
    
    //Left Button Container variable
    var leftButtonContainer: UIView!
    
    //Right Button Container variable
    var rightButtonContainer: UIView!
    
    //Right-Left Button Container variable
    var rightLeftButtonContainer: UIView!
    
    //Title container variable
    var titleContainer: UIView!
    
    //Navigation bar variable
    var navigationBar: UIView!
    
    //Content Container variable
    var contentContainer: UIView!
    
    //Bottom container variable
    var bottomContainer: UIView!
    
    //Status bar variable
    var statusBar: UIView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Adding background color to mainView
        mainView.backgroundColor = UIColor.white
        
        //Creating status bar
        statusBar = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 22))
        //Setting the background color to the status bar
        statusBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding statusBar to mainView
        mainView.addSubview(statusBar)
        
        //Creating navigation bar
        navigationBar = UIView(frame:CGRect(x: 0, y: statusBar.frame.maxY, width: UIScreen.main.bounds.width, height: 65))
        //Setting the background color to the navigation bar
        navigationBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding navigationBar to mainView
        mainView.addSubview(navigationBar)
        
        /**
         * Left button container
         */
        
        //Creating left button container
        leftButtonContainer = UIView(frame:CGRect(x: 0, y: 0, width: navigationBar.bounds.width/6, height: navigationBar.bounds.height))
        //Adding leftButtonContainer to navigationBar
        navigationBar.addSubview(leftButtonContainer)
        
        /**
         * Right button container
         */
        
        //Creating right button container
        rightButtonContainer = UIView(frame: CGRect(x: navigationBar.bounds.width - (navigationBar.bounds.width/6), y: 0, width: navigationBar.bounds.width/6, height: navigationBar.bounds.height))
        //Adding rightButtonContainer to navigationBar
        navigationBar.addSubview(rightButtonContainer)
        
        /**
         * Right-Left button container
         */
        
        //Creating right-left button container
        rightLeftButtonContainer = UIView(frame: CGRect(x: navigationBar.bounds.width - (navigationBar.bounds.width/3), y: 0, width: navigationBar.bounds.width/6, height: navigationBar.bounds.height))
        //Adding rightLeftButtonContainer to navigationBar
        navigationBar.addSubview(rightLeftButtonContainer)
        
        /**
         * Xpik logo
         */
        
        //Creating title container
        titleContainer = UIView(frame:CGRect(x: 0, y: 0, width: 90, height: navigationBar.bounds.height))
        titleContainer.center.x = navigationBar.frame.width/2
        //Creating logoImageView
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 45))
        //Centering logoImageView button into titleContainer
        logoImageView.center = CGPoint(x: titleContainer.bounds.width/2, y: titleContainer.bounds.height/2)
        //Adding xpikLogoTextWhite to logoImageView
        logoImageView.image = UIImage(named:"ic_logo_text_xpik")
        //Adding the logoImageView into the titleContainer
        titleContainer.addSubview(logoImageView)
        //Adding titleContainer to navigationBar
        navigationBar.addSubview(titleContainer)
        
        /**
         * Bottom container
         */
        
        //Creating the bottom container
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                bottomContainer = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 90, width: UIScreen.main.bounds.width, height: 60))
                
                /**
                 * Content container
                 */
                
                //Creating the content container
                contentContainer = UIView(frame: CGRect(x: 0, y: navigationBar.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (navigationBar.bounds.height + statusBar.bounds.height + 90)))
                
            default:
                bottomContainer = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 70, width: UIScreen.main.bounds.width, height: 70))
                
                /**
                 * Content container
                 */
                
                //Creating the content container
                contentContainer = UIView(frame: CGRect(x: 0, y: navigationBar.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (navigationBar.bounds.height + statusBar.bounds.height + 70)))
            }
        }else{
            
            bottomContainer = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 70, width: UIScreen.main.bounds.width, height: 70))
            
            /**
             * Content container
             */
            
            //Creating the content container
            contentContainer = UIView(frame: CGRect(x: 0, y: navigationBar.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (navigationBar.bounds.height + statusBar.bounds.height + 70)))
        }
        
        //Adding content container to mainView
        mainView.addSubview(contentContainer)
        
        //Adding background to bottom container
        bottomContainer.backgroundColor = UIColor.white
        //Adding border top to bottomContainer
        bottomContainer.addTopBorderWithColor(color: UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0), width: 2.5)
        //Adding bottom container to mainView
        mainView.addSubview(bottomContainer)
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
