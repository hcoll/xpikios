//
//  SecondaryCanvas.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/23/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class SecondaryCanvas: UIView {
    
    /**
     * Class Attributes
     */
    
    //Canvas main view variable
    var mainView: UIView!
    
    //Left Button Container variable
    var leftButtonContainer: UIView!
    
    //Right Button Container variable
    var rightButtonContainer: UIView!
    
    //Content Container variable
    var contentContainer: UIView!
    
    //Bottom container variable
    var bottomContainer: UIView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Setting the background color to the main view
        mainView.backgroundColor = UIColor.white
        
        //Creating status bar
        let statusBar = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 22))
        //Setting the background color to the status bar
        statusBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding statusBar to mainView
        mainView.addSubview(statusBar)
        
        //Creating navigation bar
        let navigationBar = UIView(frame:CGRect(x: 0, y: statusBar.frame.maxY, width: UIScreen.main.bounds.width, height: 65))
        //Setting the background color to the navigation bar
        navigationBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding navigationBar to mainView
        mainView.addSubview(navigationBar)
        
        /**
         * Left button container
         */
        
        //Creating left button container
        leftButtonContainer = UIView(frame:CGRect(x: 0, y: 0, width: 80, height: navigationBar.bounds.height))
        //Adding leftButtonContainer to navigationBar
        navigationBar.addSubview(leftButtonContainer)
        
        /**
         * Right button container
         */
        
        //Creating right button container
        rightButtonContainer = UIView(frame:CGRect(x: navigationBar.bounds.width - (navigationBar.bounds.width/6), y: 0, width: navigationBar.bounds.width/6, height: navigationBar.bounds.height))
        //Adding rightButtonContainer to navigationBar
        navigationBar.addSubview(rightButtonContainer)
        
        /**
         * Xpik logo
         */
        
        //Creating title container
        let titleContainer = UIView(frame:CGRect(x: leftButtonContainer.frame.maxX, y: 0, width: navigationBar.bounds.width - (leftButtonContainer.bounds.width + rightButtonContainer.bounds.width), height: navigationBar.bounds.height))
        //Creating logoImageView
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 45))
        //Centering logoImageView button into titleContainer
        logoImageView.center = CGPoint(x: titleContainer.bounds.width/2, y: titleContainer.bounds.height/2)
        //Adding xpikLogoTextWhite to logoImageView
        logoImageView.image = UIImage(named:"ic_logo_text_xpik")
        //Adding the logoImageView into the titleContainer
        titleContainer.addSubview(logoImageView)
        //Adding titleContainer to navigationBar
        navigationBar.addSubview(titleContainer)
        
        /**
         * Bottom container
         */
        
        //Creating the bottom container
        bottomContainer = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 3, width: UIScreen.main.bounds.width, height: 3))
        //Adding border top to bottomContainer
        bottomContainer.addTopBorderWithColor(color: UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0), width: 2.5)
        //Adding bottom container to mainView
        mainView.addSubview(bottomContainer)
        
        /**
         * Content container
         */
        
        //Creating the content container
        contentContainer = UIView(frame: CGRect(x: 0, y: navigationBar.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (navigationBar.bounds.height + statusBar.bounds.height + bottomContainer.bounds.height)))
        //Adding content container to mainView
        mainView.addSubview(contentContainer)
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
