//
//  FavoritesView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/16/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class DataCollectionView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Back button variable
    var backButton: UIButton!
    
    //Search button variable
    var searchButton: UIButton!
    
    //Content view variable
    var contentContainer: UIView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Creating a mainCanvas object
        let secondaryCanvas = SecondaryCanvas(frame: CGRect.zero)
        //Adding mainView from secondaryCanvas to principal view
        addSubview(secondaryCanvas.mainView)
        
        /**
         * Back button
         */
        
        //Creating backButton
        backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering backButton button into leftButtonContainer
        backButton.center = CGPoint(x: secondaryCanvas.leftButtonContainer.bounds.width/2, y: secondaryCanvas.leftButtonContainer.bounds.height - 20)
        //Adding back image
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        //Adding backButton to leftButtonContainer
        secondaryCanvas.leftButtonContainer.addSubview(backButton)
        
        /**
         * Search button
         */
        
        //Creating searchButton
        searchButton = UIButton(frame: CGRect(x: 0, y: 0, width: 55, height: 55))
        //Centering searchButton button into rightButtonContainer
        searchButton.center = CGPoint(x: secondaryCanvas.rightButtonContainer.bounds.width/2, y: secondaryCanvas.rightButtonContainer.bounds.height - 20)
        //Adding menu icon to searchButton
        searchButton.setImage(UIImage(named: "ic_search"), for: .normal)
        //Adding tint color to searchButton
        searchButton.tintColor = UIColor.white
        //Adding searchButton to rightButtonContainer
        secondaryCanvas.rightButtonContainer.addSubview(searchButton)
        
        //Getting content container variable from secondary canvas
        contentContainer = secondaryCanvas.contentContainer
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
