//
//  DeleteFavorite.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 2/1/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class DeleteFavoriteView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Main view variable
    var mainView: UIView!
    
    //Delete favorite container variable
    var deleteFavContainer: UIView!
    
    //Cancel button variable
    var cancelButton: UIButton!
    
    //Accept button variable
    var acceptButton: UIButton!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the DeleteFavoriteView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Setting the background color to the main view
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Adding background label
        let backgroundLabel = UIImageView(frame:CGRect(x: 0, y: 0, width: mainView.bounds.width, height: mainView.bounds.height))
        backgroundLabel.image = UIImage(named: "label_background")
        mainView.addSubview(backgroundLabel)
        
        /**
         * Delete favorites container
         */
        
        //Creating delete favorite container
        deleteFavContainer = UIView(frame:CGRect(x: 0, y: 0, width: mainView.bounds.width - 20, height: 170))
        //Centering deleteFavContainer into mainView
        deleteFavContainer.center = CGPoint(x: mainView.bounds.width/2, y: mainView.bounds.height/2 - 10)
        //Adding deleteFavContainer into mainView
        mainView.addSubview(deleteFavContainer)
        
        /**
         * Title label
         */
        
        //Creating title label
        let titleLabel = UILabel(frame:CGRect(x: 0, y: 10, width: deleteFavContainer.bounds.width, height: 80))
        //Centering the text into titleLabel
        titleLabel.textAlignment = .center
        //Setting text font to titleLabel
        titleLabel.font = UIFont(name: "Helvetica-bold", size: 20)
        //Setting text color to titleLabel
        titleLabel.textColor = UIColor.white
        //Number of lines in the label
        titleLabel.numberOfLines = 2
        //Setting text to titleLabel
        titleLabel.text = NSLocalizedString("Clear_Strip", comment: "")
        //Adding titleLabel into deleteFavContainer
        deleteFavContainer.addSubview(titleLabel)
        
        /**
         * Buttons container
         */
        
        //Creating buttons container
        let buttonsContainer = UIView(frame:CGRect(x: 0, y: titleLabel.frame.maxY, width: deleteFavContainer.bounds.width, height: 60))
        //Adding buttonsContainer into deleteFavContainer
        deleteFavContainer.addSubview(buttonsContainer)
        
        /**
         * Cancel button
         */
        
        //Creating cancel button
        cancelButton = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        //Centering cancelButton into buttonsContainer
        cancelButton.center = CGPoint(x: buttonsContainer.bounds.width/2 - 35, y: buttonsContainer.bounds.height/2)
        //Setting title to cancelButton
        cancelButton.setImage(#imageLiteral(resourceName: "ic_close"), for: .normal)
        //Adding cancelButton into buttonsContainer
        buttonsContainer.addSubview(cancelButton)
        
        /**
         * Accept button
         */
        
        //Creating accept button
        acceptButton = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        //Centering acceptButton into buttonsContainer
        acceptButton.center = CGPoint(x: buttonsContainer.bounds.width/2 + 70, y: buttonsContainer.bounds.height/2)
        //Setting title to acceptButton
        acceptButton.setImage(#imageLiteral(resourceName: "ic_check_btn"), for: .normal)
        //Adding acceptButton into buttonsContainer
        buttonsContainer.addSubview(acceptButton)
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
