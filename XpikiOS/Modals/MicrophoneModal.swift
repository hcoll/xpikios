//
//  MicrophoneModal.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 6/4/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class MicrophoneModal: UIView {
    
    /**
     * Class Attributes
     */
    
    //Main view variable
    var mainView: UIView!
    
    var listeningIcon: UIButton!
    
    //Share container variable
    var microphoneIcon: UIImageView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SearchView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        /**
         * Listening icon
         */
        
        //Creating listeningIcon
        listeningIcon = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        //Centering listeningIcon button into leftButtonContainer
        listeningIcon.center = CGPoint(x: mainView.bounds.width/2, y: UIScreen.main.bounds.height - 100)
        //Adding menu icon to listeningIcon
        listeningIcon.setImage(UIImage(named: "listening_icon"), for: .normal)
        //Adding tint color to listeningIcon
        listeningIcon.backgroundColor = UIColor.white
        //Adding corner radius to listeningIcon
        listeningIcon.layer.cornerRadius = 0.5 * listeningIcon.bounds.size.width
        //Cliping listeningIcon to bounds
        listeningIcon.clipsToBounds = true
        //Adding listeningIcon to mainView
        mainView.addSubview(listeningIcon)
        
        self.fadeOut(finished: true)
    }
    
    /**
     * Fade in effect
     */
    
    func fadeIn(finished: Bool) {
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: { self.listeningIcon.alpha = 1 } , completion: self.fadeOut)
    }
    
    /**
     * Fade out effect
     */
    
    func fadeOut(finished: Bool) {
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: { self.listeningIcon.alpha = 0 } , completion: self.fadeIn)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
