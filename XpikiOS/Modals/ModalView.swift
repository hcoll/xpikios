//
//  SearchView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/15/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class ModalView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Main view variable
    var mainView: UIView!
    
    //Search container variable
    var searchContainer: UIView!
    
    //Buttons container variable
    var buttonsContainer: UIView!
    
    //Cancel button variable
    var cancelButton: UIButton!
    
    //Accept button variable
    var acceptButton: UIButton!
    
    //Search text variable
    var searchText: TextField!
    
    //Modal view title label variable
    var titleLabel: UnderlinedLabel!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SearchView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Setting the background color to the main view
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Adding background label
        let backgroundLabel = UIImageView(frame:CGRect(x: 0, y: 0, width: mainView.bounds.width, height: mainView.bounds.height))
        backgroundLabel.image = UIImage(named: "label_background")
        mainView.addSubview(backgroundLabel)
        
        /**
         * Search container
         */
        
        //Creating search container
        searchContainer = UIView(frame:CGRect(x: 0, y: 0, width: mainView.bounds.width - 20, height: 230))
        //Centering searchContainer into mainView
        searchContainer.center = CGPoint(x: mainView.bounds.width/2, y: mainView.bounds.height/2)
        //Adding searchContainer into mainView
        mainView.addSubview(searchContainer)
        
        /**
         * Title label
         */
        
        //Creating title label
        titleLabel = UnderlinedLabel(frame:CGRect(x: 0, y: 10, width: searchContainer.bounds.width, height: 50))
        //Centering the text into titleLabel
        titleLabel.textAlignment = .center
        //Setting lines number of the title
        titleLabel.numberOfLines = 2
        //Setting text font to titleLabel
        titleLabel.font = UIFont(name: "Helvetica-bold", size: 20)
        //Setting text color to titleLabel
        titleLabel.textColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding titleLabel into searchContainer
        //searchContainer.addSubview(titleLabel)
        
        /**
         * Search input
         */
        
        //Create search label
        let searchLabel = UIView(frame:CGRect(x: 0, y: 0, width: searchContainer.bounds.width - 30, height: 50))
        //Adding background color
        searchLabel.backgroundColor = UIColor.white
        //Centering searchLabel into searchContainer
        searchLabel.center = CGPoint(x: searchContainer.bounds.width/2, y: 50)
        //Creating search text view
        searchText = TextField(frame:CGRect(x: 0, y: 0, width: searchLabel.bounds.width, height: searchLabel.bounds.height))
        //Adding searchText into searchLabel
        searchLabel.addSubview(searchText)
        //Adding searchLabel into searchContainer
        searchContainer.addSubview(searchLabel)
        
        /**
         * Buttons container
         */
        
        //Creating buttons container
        buttonsContainer = UIView(frame:CGRect(x: 0, y: searchLabel.frame.maxY, width: searchContainer.bounds.width, height: 100))
        //Adding buttonsContainer into searchContainer
        searchContainer.addSubview(buttonsContainer)
        
        /**
         * Cancel button
         */
        
        //Creating cancel button
        cancelButton = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        //Centering cancelButton into buttonsContainer
        cancelButton.center = CGPoint(x: buttonsContainer.bounds.width/2 - 35, y: buttonsContainer.bounds.height/2)
        //Setting title to cancelButton
        cancelButton.setImage(#imageLiteral(resourceName: "ic_close"), for: .normal)
        //Adding cancelButton into buttonsContainer
        buttonsContainer.addSubview(cancelButton)
        
        /**
         * Accept button
         */
        
        //Creating accept button
        acceptButton = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
        //Centering acceptButton into buttonsContainer
        acceptButton.center = CGPoint(x: buttonsContainer.bounds.width/2 + 70, y: buttonsContainer.bounds.height/2)
        //Setting title to acceptButton
        acceptButton.setImage(#imageLiteral(resourceName: "ic_search"), for: .normal)
        //Adding acceptButton into buttonsContainer
        buttonsContainer.addSubview(acceptButton)
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
