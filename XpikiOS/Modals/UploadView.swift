//
//  UploadView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/24/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class UploadView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Main view variable
    var mainView: UIView!
    
    //Upload container variable
    var uploadContainer: UIView!
    
    //Camera button container variable
    var cameraContainer: UIView!
    
    //Color button container variable
    var colorContainer: UIView!
    
    //Emoji button container variable
    var emojiContainer: UIView!
    
    //Gallery button container variable
    var galleryContainer: UIView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SearchView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Setting the background color to the main view
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        /**
         * Upload container
         */
        
        //Creating upload container
        uploadContainer = UIView(frame:CGRect(x: 0, y: 0, width: mainView.bounds.width - (mainView.bounds.width/4), height: mainView.bounds.width - (mainView.bounds.width/4)))
        //Centering uploadContainer into mainView
        uploadContainer.center = CGPoint(x: mainView.bounds.width/2, y: mainView.bounds.height/2)
        //Adding uploadContainer into mainView
        uploadContainer.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding uploadContainer into mainView
        mainView.addSubview(uploadContainer)
        
        //Camera container variable
        cameraContainer = UIView(frame: CGRect(x: 0, y: 0, width: uploadContainer.bounds.width/2, height: uploadContainer.bounds.height/2))
        //Adding cameraContainer to uploadContainer
        uploadContainer.addSubview(cameraContainer)
        //Creating the camera icon logo UIImageView
        let cameraLogo = UIImageView(frame:CGRect(x: 0, y: 0, width: 120, height: 120))
        //Centering cameraLogo in cameraContainer
        cameraLogo.center = CGPoint(x: cameraContainer.bounds.width - 50, y: cameraContainer.bounds.height - 50)
        //Adding logo to cameraLogo
        cameraLogo.image = UIImage(named: "ic_camera")
        //Adding cameraLogo to cameraContainer
        cameraContainer.addSubview(cameraLogo)
        
        //Gallery container variable
        galleryContainer = UIView(frame: CGRect(x: cameraContainer.frame.width, y: 0, width: uploadContainer.bounds.width/2, height: uploadContainer.bounds.height/2))
        //Adding alpha to galleryContainer
        galleryContainer.backgroundColor = UIColor.white.withAlphaComponent(0.1)
        //Adding galleryContainer to uploadContainer
        uploadContainer.addSubview(galleryContainer)
        //Creating the gallery icon logo UIImageView
        let galleryLogo = UIImageView(frame:CGRect(x: 0, y: 0, width: 120, height: 120))
        //Centering galleryLogo in galleryContainer
        galleryLogo.center = CGPoint(x: galleryContainer.bounds.width - 50, y: galleryContainer.bounds.height - 50)
        //Adding logo to galleryLogo
        galleryLogo.image = UIImage(named: "ic_gallery")
        //Adding galleryLogo to galleryContainer
        galleryContainer.addSubview(galleryLogo)
        
        //Color container variable
        colorContainer = UIView(frame: CGRect(x: 0, y: cameraContainer.frame.height, width: uploadContainer.bounds.width/2, height: uploadContainer.bounds.height/2))
        //Adding alpha to colorContainer
        colorContainer.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        //Adding colorContainer to uploadContainer
        uploadContainer.addSubview(colorContainer)
        //Creating the color icon logo UIImageView
        let colorLogo = UIImageView(frame:CGRect(x: 0, y: 0, width: 120, height: 120))
        //Centering colorLogo in colorContainer
        colorLogo.center = CGPoint(x: colorContainer.bounds.width - 50, y: colorContainer.bounds.height - 50)
        //Adding logo to colorLogo
        colorLogo.image = UIImage(named: "ic_solid_background")
        //Adding colorLogo to colorContainer
        colorContainer.addSubview(colorLogo)
        
        //Emoji container variable
        emojiContainer = UIView(frame: CGRect(x: colorContainer.frame.width, y: galleryContainer.frame.height, width: uploadContainer.bounds.width/2, height: uploadContainer.bounds.height/2))
        //Adding alpha to emojiContainer
        emojiContainer.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        //Adding emojiContainer to uploadContainer
        uploadContainer.addSubview(emojiContainer)
        //Creating the emoji icon logo UIImageView
        let emojiLogo = UIImageView(frame:CGRect(x: 0, y: 0, width: 120, height: 120))
        //Centering emojiLogo in emojiContainer
        emojiLogo.center = CGPoint(x: emojiContainer.bounds.width - 50, y: emojiContainer.bounds.height - 50)
        //Adding logo to emojiLogo
        emojiLogo.image = UIImage(named: "ic_emoji")
        //Adding emojiLogo to emojiContainer
        emojiContainer.addSubview(emojiLogo)
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

/*//
//  UploadView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/24/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class UploadView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Main view variable
    var mainView: UIView!
    
    //Upload container variable
    var uploadContainer: UIView!
    
    //Camera button container variable
    var cameraContainer: UIView!
    
    //Color button container variable
    var colorContainer: UIView!
    
    //Emoji button container variable
    var emojiContainer: UIView!
    
    //Gallery button container variable
    var galleryContainer: UIView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SearchView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Setting the background color to the main view
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        /**
         * Upload container
         */
        
        //Creating upload container
        uploadContainer = UIView(frame:CGRect(x: 0, y: 0, width: mainView.bounds.width - 20, height: 200))
        //Centering uploadContainer into mainView
        uploadContainer.center = CGPoint(x: mainView.bounds.width/2, y: mainView.bounds.height/2)
        //Adding uploadContainer into mainView
        uploadContainer.backgroundColor = UIColor.white
        //Adding corner radius to the uploadContainer
        uploadContainer.layer.cornerRadius = 40
        //Adding border width
        uploadContainer.layer.borderWidth = 4
        //Adding border color
        uploadContainer.layer.borderColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0).cgColor
        //Adding uploadContainer into mainView
        mainView.addSubview(uploadContainer)
        
        /**
         * Title label
         */
        
        //Creating title label
        let titleLabel = UILabel(frame:CGRect(x: 0, y: 10, width: uploadContainer.bounds.width, height: 50))
        //Centering the text into titleLabel
        titleLabel.textAlignment = .center
        //Setting text font to titleLabel
        titleLabel.font = UIFont(name: "Helvetica-bold", size: 20)
        //Setting text color to titleLabel
        titleLabel.textColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Setting text to titleLabel
        titleLabel.text = NSLocalizedString("Complete_Action", comment: "")
        //Adding titleLabel into uploadContainer
        uploadContainer.addSubview(titleLabel)
        
        /**
         * Buttons container
         */
        
        //Creating buttons container
        let buttonsContainer = UIView(frame:CGRect(x: 0, y: titleLabel.frame.maxY, width: uploadContainer.bounds.width, height: uploadContainer.bounds.height - titleLabel.frame.height))
        //Adding buttonsContainer into uploadContainer
        uploadContainer.addSubview(buttonsContainer)
        
        cameraContainer = UIView(frame: CGRect(x: 0, y: 0, width: buttonsContainer.bounds.width/2, height: buttonsContainer.bounds.height/2))
        buttonsContainer.addSubview(cameraContainer)
        
        /**
         * Camera button
         */
        
        //Creating camera button
        let cameraButton = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        //Centering cameraButton into buttonsContainer
        cameraButton.center = CGPoint(x: buttonsContainer.bounds.width/4, y: 20)
        //Setting icon to cameraButton
        cameraButton.image = UIImage(named: "ic_camera")
        //Adding cameraButton into cameraContainer
        cameraContainer.addSubview(cameraButton)
        
        /**
         * Camera label
         */
        
        //Creating camera label
        let cameraLabel = UILabel(frame:CGRect(x: 0, y: 0, width: cameraContainer.bounds.width/2, height: 45))
        //Centering cameraLabel into buttonsContainer
        cameraLabel.center = CGPoint(x: cameraContainer.bounds.width/2, y: cameraButton.frame.maxY + 10)
        //Centering the text into cameraLabel
        cameraLabel.textAlignment = .center
        //Setting text font to cameraLabel
        cameraLabel.font = UIFont(name: "Helvetica-bold", size: 16)
        //Setting text to cameraLabel
        cameraLabel.text = NSLocalizedString("Camera", comment: "")
        //Adding cameraLabel into cameraContainer
        cameraContainer.addSubview(cameraLabel)
        
        galleryContainer = UIView(frame: CGRect(x: cameraContainer.frame.width, y: 0, width: buttonsContainer.bounds.width/2, height: buttonsContainer.bounds.height/2))
        buttonsContainer.addSubview(galleryContainer)
        
        /**
         * Gallery button
         */
        
        //Creating gallery button
        let galleryButton = UIImageView(frame: CGRect(x: 0, y: 0, width: 35, height: 30))
        //Centering galleryButton into galleryContainer
        galleryButton.center = CGPoint(x: galleryContainer.bounds.width/2, y: 20)
        //Setting icon to galleryButton
        galleryButton.image = UIImage(named: "ic_gallery")
        //Adding galleryButton into galleryContainer
        galleryContainer.addSubview(galleryButton)
        
        /**
         * Gallery label
         */
        
        //Creating gallery label
        let galleryLabel = UILabel(frame:CGRect(x: 0, y: 0, width: galleryContainer.bounds.width/2, height: 45))
        //Centering galleryLabel into buttonsContainer
        galleryLabel.center = CGPoint(x: galleryContainer.bounds.width/2, y: galleryButton.frame.maxY + 10)
        //Centering the text into galleryLabel
        galleryLabel.textAlignment = .center
        //Setting text font to galleryLabel
        galleryLabel.font = UIFont(name: "Helvetica-bold", size: 16)
        //Setting text to galleryLabel
        galleryLabel.text = NSLocalizedString("Gallery", comment: "")
        //Adding galleryLabel into galleryContainer
        galleryContainer.addSubview(galleryLabel)
        
        colorContainer = UIView(frame: CGRect(x: 0, y: cameraContainer.frame.height, width: buttonsContainer.bounds.width/2, height: buttonsContainer.bounds.height/2))
        buttonsContainer.addSubview(colorContainer)
        
        /**
         * Color button
         */
        
        //Creating color button
        let colorButton = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        //Centering colorButton into buttonsContainer
        colorButton.center = CGPoint(x: colorContainer.bounds.width/2, y: 10)
        //Setting icon to colorButton
        colorButton.image = UIImage(named: "ic_solid_background")
        //Adding colorButton into buttonsContainer
        colorContainer.addSubview(colorButton)
        
        /**
         * Color label
         */
        
        //Creating color label
        let colorLabel = UILabel(frame:CGRect(x: 0, y: 0, width: colorContainer.bounds.width/2, height: 45))
        //Centering colorLabel into buttonsContainer
        colorLabel.center = CGPoint(x: colorContainer.bounds.width/2, y: colorButton.frame.maxY + 10)
        //Centering the text into colorLabel
        colorLabel.textAlignment = .center
        //Setting text font to colorLabel
        colorLabel.font = UIFont(name: "Helvetica-bold", size: 16)
        //Setting text to colorLabel
        colorLabel.text = NSLocalizedString("Color", comment: "")
        //Adding colorLabel into colorContainer
        colorContainer.addSubview(colorLabel)
        
        emojiContainer = UIView(frame: CGRect(x: colorContainer.frame.width, y: galleryContainer.frame.height, width: buttonsContainer.bounds.width/2, height: buttonsContainer.bounds.height/2))
        buttonsContainer.addSubview(emojiContainer)
        
        /**
         * Emoji button
         */
        
        //Creating emoji button
        let emojiButton = UIImageView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        //Centering emojiButton into buttonsContainer
        emojiButton.center = CGPoint(x: emojiContainer.bounds.width/2, y: 10)
        //Setting icon to emojiButton
        emojiButton.image = UIImage(named: "ic_emoji")
        //Adding emojiButton into emojiContainer
        emojiContainer.addSubview(emojiButton)
        
        /**
         * Emoji label
         */
        
        //Creating color label
        let emojiLabel = UILabel(frame:CGRect(x: 0, y: 0, width: emojiContainer.bounds.width/2, height: 45))
        //Centering emojiLabel into buttonsContainer
        emojiLabel.center = CGPoint(x: emojiContainer.bounds.width/2, y: emojiButton.frame.maxY + 10)
        //Centering the text into emojiLabel
        emojiLabel.textAlignment = .center
        //Setting text font to emojiLabel
        emojiLabel.font = UIFont(name: "Helvetica-bold", size: 16)
        //Setting text to emojiLabel
        emojiLabel.text = NSLocalizedString("Emoji", comment: "")
        //Adding emojiLabel into buttonsContainer
        emojiContainer.addSubview(emojiLabel)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}*/
