//
//  NodesModal.swift
//  XpikiOS
//
//  Created by Xpik Developer on 7/15/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class NodesModal: UIView {
    
    /**
     * Class Attributes
     */
    
    //Main view variable
    var mainView: UIView!
    
    //Modal view title label variable
    var titleLabel: UILabel!
    
    var currentNode: Int!
    
    let items = ["ic_plus","ic_minus","ic_arrow_down","ic_denied","ic_cancel","ic_equal","none"]
    
    override init(frame: CGRect) {

        //Calling the default initializer for the SearchView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Setting the background color to the main view
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        /**
         * Search container
         */
        
        //Creating search container
        let searchContainer = UIView(frame:CGRect(x: 0, y: 0, width: mainView.bounds.width - 20, height: 180))
        //Centering searchContainer into mainView
        searchContainer.center = CGPoint(x: mainView.bounds.width/2, y: mainView.bounds.height/2)
        //Adding searchContainer into mainView
        searchContainer.backgroundColor = UIColor.white
        //Adding corner radius to the searchContainer
        searchContainer.layer.cornerRadius = 40
        //Adding border width
        searchContainer.layer.borderWidth = 4
        //Adding border color
        searchContainer.layer.borderColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0).cgColor
        //Adding searchContainer into mainView
        mainView.addSubview(searchContainer)
        
        /**
         * Title label
         */
        
        //Creating title label
        titleLabel = UILabel(frame:CGRect(x: 0, y: 10, width: searchContainer.bounds.width, height: 50))
        //Centering the text into titleLabel
        titleLabel.textAlignment = .center
        //Setting lines number of the title
        titleLabel.numberOfLines = 2
        //Setting text font to titleLabel
        titleLabel.font = UIFont(name: "Helvetica-bold", size: 20)
        //Setting text to title
        titleLabel.text = NSLocalizedString("Select_Node", comment: "")
        //Setting text color to titleLabel
        titleLabel.textColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding titleLabel into searchContainer
        searchContainer.addSubview(titleLabel)
        
        //Creating colors container
        let colorsContainer = UIView(frame:CGRect(x: searchContainer.bounds.width/4, y: 0, width: 170, height: 110))
        colorsContainer.center = CGPoint(x: searchContainer.bounds.width/2, y: searchContainer.bounds.height/2 + 20)
        searchContainer.addSubview(colorsContainer)
        
        var row = 0
        
        var auxRow = 0
        
        var positionColor = 0
        
        for _ in 0...1 {
            
            var column = 0
            
            for _ in 0...2 {
                
                let colorButton = UIButton(frame:CGRect(x: column, y: row, width: 50, height: 50))
                colorButton.setImage(UIImage(named: items[positionColor] ), for: .normal)
                colorButton.imageView?.layer.cornerRadius = colorButton.frame.width/2
                colorButton.imageView?.clipsToBounds = true
                //colorButton.imageView?.layer.borderWidth = 1.0
                colorButton.tag = positionColor
                colorButton.addTarget(self, action: #selector(selectBackground), for: .touchUpInside)
                colorsContainer.addSubview(colorButton)
                
                column = column + Int(colorButton.frame.width) + 10
                
                auxRow = Int(colorButton.frame.height)
                
                positionColor = positionColor + 1
            }
            
            row = row + auxRow + 10
        }
        
    }
    
    func selectBackground (sender: UIButton){
        
        if(Shared.shared.currentController == Shared.shared.homeController){
            Shared.shared.homeController.nodeSelected(currentNode: currentNode, nodeType: sender.tag)
        }else{
            Shared.shared.libraryController.nodeSelected(currentNode: currentNode, nodeType: sender.tag)
        }
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
