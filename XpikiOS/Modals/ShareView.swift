//
//  ShareView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 4/18/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class ShareView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Main view variable
    var mainView: UIView!
    
    //Share container variable
    var shareContainer: UIView!
    
    //Download button variable
    var downloadButton: UIButton!
    
    //Send button variable
    var sendButton: UIButton!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SearchView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Setting the background color to the main view
        mainView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        /**
         * Share container
         */
        
        //Creating share container
        shareContainer = UIView(frame:CGRect(x: 0, y: 0, width: mainView.bounds.width - 20, height: 120))
        //Centering shareContainer into mainView
        shareContainer.center = CGPoint(x: mainView.bounds.width/2, y: mainView.bounds.height/2)
        //Adding shareContainer into mainView
        shareContainer.backgroundColor = UIColor.white
        //Adding corner radius to the shareContainer
        shareContainer.layer.cornerRadius = 40
        //Adding border width
        shareContainer.layer.borderWidth = 4
        //Adding border color
        shareContainer.layer.borderColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0).cgColor
        //Adding shareContainer into mainView
        mainView.addSubview(shareContainer)
        
        /**
         * download button
         */
        
        //Creating download button
        downloadButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 55))
        //Centering downloadButton into shareContainer
        downloadButton.center = CGPoint(x: shareContainer.bounds.width/4, y: shareContainer.bounds.height/2 - 10)
        //Setting icon to downloadButton
        downloadButton.setImage(#imageLiteral(resourceName: "ic_download"), for: .normal)
        //Adding downloadButton into shareContainer
        shareContainer.addSubview(downloadButton)
        
        /**
         * download label
         */
        
        //Creating download label
        let downloadLabel = UILabel(frame:CGRect(x: 0, y: 0, width: shareContainer.bounds.width/2, height: 45))
        //Centering downloadLabel into shareContainer
        downloadLabel.center = CGPoint(x: shareContainer.bounds.width/4, y: downloadButton.frame.maxY + 10)
        //Centering the text into downloadLabel
        downloadLabel.textAlignment = .center
        //Setting text font to downloadLabel
        downloadLabel.font = UIFont(name: "Helvetica-bold", size: 16)
        //Setting text to downloadLabel
        downloadLabel.text = NSLocalizedString("Download", comment: "")
        //Adding downloadLabel into shareContainer
        shareContainer.addSubview(downloadLabel)
        
        /**
         * Send button
         */
        
        //Creating send button
        sendButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 55))
        //Centering sendButton into shareContainer
        sendButton.center = CGPoint(x: shareContainer.bounds.width/2 + shareContainer.bounds.width/4, y: shareContainer.bounds.height/2 - 10)
        //Setting icon to sendButton
        sendButton.setImage(#imageLiteral(resourceName: "ic_send"), for: .normal)
        //Adding sendButton into shareContainer
        shareContainer.addSubview(sendButton)
        
        /**
         * Share label
         */
        
        //Creating share label
        let shareLabel = UILabel(frame:CGRect(x: 0, y: 0, width: shareContainer.bounds.width/2, height: 45))
        //Centering shareLabel into shareContainer
        shareLabel.center = CGPoint(x: shareContainer.bounds.width/2 + shareContainer.bounds.width/4, y: sendButton.frame.maxY + 10)
        //Centering the text into shareLabel
        shareLabel.textAlignment = .center
        //Setting text font to shareLabel
        shareLabel.font = UIFont(name: "Helvetica-bold", size: 16)
        //Setting text to shareLabel
        shareLabel.text = NSLocalizedString("Share", comment: "")
        //Adding shareLabel into shareContainer
        shareContainer.addSubview(shareLabel)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
