//
//  PreferencesView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/16/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class PreferencesView: UIView {
    
    /**
     * Class Attributes
     */
    
    var titleLabel: UIView!
    
    //Back button variable
    var backButton: UIButton!
    
    //Content view variable
    var contentContainer: UIView!
    
    //Safe Search Switch button variable
    //var safeSearchSwitch: UISwitch!
    
    //Disable audio Switch button variable
    //var disableAudioSwitch: UISwitch!
    
    //Disable shake Switch button variable
    var disableShakeSwitch: UISwitch!
    
    //Disable empty Switch button variable
    //var emptySwitch: UISwitch!
    
    //Carousel pages container variable
    var carouselPagesContainer: UIView!
    
    //Languages container variable
    var languagesContainer: UIView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Creating a mainCanvas object
        let secondaryCanvas = SecondaryCanvas(frame: CGRect.zero)
        //Adding mainView from secondaryCanvas to principal view
        addSubview(secondaryCanvas.mainView)
        
        /**
         * Back button
         */
        
        //Creating backButton
        backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering backButton button into leftButtonContainer
        backButton.center = CGPoint(x: secondaryCanvas.leftButtonContainer.bounds.width/2, y: secondaryCanvas.leftButtonContainer.bounds.height - 20)
        //Adding back image
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        //Adding backButton to leftButtonContainer
        secondaryCanvas.leftButtonContainer.addSubview(backButton)
        
        //Getting content container variable from secondary canvas
        contentContainer = secondaryCanvas.contentContainer
        
        /**
         * Settings title
         */
        
        //Creating the title label
        titleLabel = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 100))
        //Title label background color
        titleLabel.backgroundColor = UIColor.white
        //Setting border bottom to title label
        titleLabel.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating the preferences icon UIImageView
        let prefLogo = UIImageView(frame:CGRect(x: 0, y: 0, width: 70, height: 70))
        //Centering prefLogo in emojiContainer
        prefLogo.center = CGPoint(x: titleLabel.center.x, y: titleLabel.center.y)
        //Adding logo to prefLogo
        prefLogo.image = UIImage(named: "settings_logo")
        //Adding prefLogo to titleLabel
        titleLabel.addSubview(prefLogo)
        //Adding titleLabel to contentContainer
        contentContainer.addSubview(titleLabel)
        
        /*/**
         * Safe search container
         */
        
        //Creating the safe search container
        let safeSearchContainer = UIView(frame: CGRect(x: 0, y: titleLabel.frame.maxY, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/7 - contentContainer.bounds.height/21))
        //Setting border bottom to safe search container
        safeSearchContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating safe search switch button
        safeSearchSwitch = UISwitch()
        //Setting size to safe search switch button
        safeSearchSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        //Centering safe search switch button into safe search container
        safeSearchSwitch.center = CGPoint (x: safeSearchContainer.bounds.width - 50, y: safeSearchContainer.bounds.height/2)
        //Setting on (true) the safe search switch button
        safeSearchSwitch.isOn = true
        safeSearchSwitch.setOn(true, animated: false)
        //Setting tint color when switch is on
        safeSearchSwitch.onTintColor = UIColor.orange
        //Setting tint color to safe search switch
        safeSearchSwitch.tintColor = UIColor.orange
        //Adding safe search switch button into safe search container
        safeSearchContainer.addSubview(safeSearchSwitch)
        //Creating UIlabel for safe search
        let safeSearchLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - safeSearchSwitch.bounds.width, height: safeSearchContainer.bounds.height))
        //Setting text to safeSearchLabel
        safeSearchLabel.text = NSLocalizedString("Safe_Search", comment: "")
        //Adding safeSearchLabel into safeSearchContainer
        safeSearchContainer.addSubview(safeSearchLabel)
        //Adding safe search container to contentContainer
        contentContainer.addSubview(safeSearchContainer)
        
        /**
         * Disable audio container
         */
        
        //Creating the disable audio container
        let disableAudioContainer = UIView(frame: CGRect(x: 0, y: safeSearchContainer.frame.maxY, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/7 - contentContainer.bounds.height/21))
        //Setting border bottom to safe search container
        disableAudioContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating disable audio switch button
        disableAudioSwitch = UISwitch()
        //Setting size to disable audio switch button
        disableAudioSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        //Centering disable audio switch button into disable audio container
        disableAudioSwitch.center = CGPoint (x: disableAudioContainer.bounds.width - 50, y: disableAudioContainer.bounds.height/2)
        //Setting on (true) the disable audio switch button
        disableAudioSwitch.isOn = true
        disableAudioSwitch.setOn(true, animated: false)
        //Setting tint color when switch is on
        disableAudioSwitch.onTintColor = UIColor.orange
        //Setting tint color to disable audio switch
        disableAudioSwitch.tintColor = UIColor.orange
        //Adding disable audio switch button into disable audio container
        disableAudioContainer.addSubview(disableAudioSwitch)
        //Creating UIlabel for disable audio
        let disableAudioLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: disableAudioContainer.bounds.height))
        //Setting text to disableAudioLabel
        disableAudioLabel.text = NSLocalizedString("Disable_Audio", comment: "")
        //Adding disableAudioLabel into disableAudioContainer
        disableAudioContainer.addSubview(disableAudioLabel)
        //Adding disable audio container to contentContainer
        contentContainer.addSubview(disableAudioContainer)*/
        
        /**
         * Disable shake container
         */
        
        //Creating the disable shake container
        let disableShakeContainer = UIView(frame: CGRect(x: 0, y: titleLabel.frame.maxY, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/7 - contentContainer.bounds.height/21))
        //Setting border bottom to disable shake container
        disableShakeContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating disable shake switch button
        disableShakeSwitch = UISwitch()
        //Setting size to disable shake switch button
        disableShakeSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        //Centering disable shake switch button into safe search container
        disableShakeSwitch.center = CGPoint (x: disableShakeContainer.bounds.width - 50, y: disableShakeContainer.bounds.height/2)
        //Setting on (true) the disable shake switch button
        disableShakeSwitch.isOn = true
        disableShakeSwitch.setOn(true, animated: false)
        //Setting tint color when switch is on
        disableShakeSwitch.onTintColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Setting tint color to disable shake switch
        disableShakeSwitch.tintColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding disable shake switch button into disable shake container
        disableShakeContainer.addSubview(disableShakeSwitch)
        //Creating UIlabel for disable shake
        let disableShakeLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: disableShakeContainer.bounds.height))
        //Setting text to disableShakeLabel
        disableShakeLabel.text = NSLocalizedString("Disable_Shake", comment: "")
        //Adding disableShakeLabel into disableShakeContainer
        disableShakeContainer.addSubview(disableShakeLabel)
        //Adding disable shake container to contentContainer
        contentContainer.addSubview(disableShakeContainer)
        
        /**
         * Carousel pages container
         */
        
        //Creating the carousel pages container
        carouselPagesContainer = UIView(frame: CGRect(x: 0, y: disableShakeContainer.frame.maxY, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/7 - 2.5))
        //Setting border bottom to carousel pages container
        carouselPagesContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating UIlabel for carousel pages
        let carouselPagesLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: carouselPagesContainer.bounds.height/2))
        //Setting text to carouselPagesLabel
        carouselPagesLabel.text = NSLocalizedString("Carousel_Pages", comment: "")
        //Adding carouselPagesLabel into carouselPagesContainer
        carouselPagesContainer.addSubview(carouselPagesLabel)
        //Creating UIlabel for carousel sub page
        let carouselPagesSubLabel = UILabel(frame: CGRect(x: 15, y: carouselPagesLabel.frame.maxY/2 + 10, width: contentContainer.bounds.width - 15, height: carouselPagesContainer.bounds.height/2))
        //Setting text to carouselPagesLabel
        carouselPagesSubLabel.text = NSLocalizedString("Search_Number", comment: "")
        //Setting font to text
        carouselPagesSubLabel.font = UIFont(name: "Helvetica", size: 16)
        //Setting color to text
        carouselPagesSubLabel.textColor = UIColor.gray
        //Adding carouselPagesSubLabel into carouselPagesContainer
        carouselPagesContainer.addSubview(carouselPagesSubLabel)
        
        //Adding carousel pages container to contentContainer
        contentContainer.addSubview(carouselPagesContainer)
        
        /*/**
         * Empty home container
         */
        
        //Creating the empty home container
        let emptyHomeContainer = UIView(frame: CGRect(x: 0, y: carouselPagesContainer.frame.maxY, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/7 - 2.5))
        //Setting border bottom to empty home container
        emptyHomeContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating UIlabel for empty home
        let emptyHomeLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: emptyHomeContainer.bounds.height/2))
        //Setting text to emptyHomeLabel
        emptyHomeLabel.text = NSLocalizedString("Empty_Home", comment: "")
        //Adding emptyHomeLabel into emptyHomeContainer
        emptyHomeContainer.addSubview(emptyHomeLabel)
        //Creating empty switch button
        emptySwitch = UISwitch()
        //Setting size to empty switch switch button
        emptySwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        //Centering empty switch button into safe search container
        emptySwitch.center = CGPoint (x: emptyHomeContainer.bounds.width - 50, y: 20)
        //Setting on (true) the empty switch button
        emptySwitch.isOn = true
        emptySwitch.setOn(true, animated: false)
        //Setting tint color when switch is on
        emptySwitch.onTintColor = UIColor.orange
        //Setting tint color to empty switch
        emptySwitch.tintColor = UIColor.orange
        //Adding disable empty switch button into empty container
        emptyHomeContainer.addSubview(emptySwitch)
        //Creating UIlabel for empty sub label
        let emptyHomeSubLabel = UILabel(frame: CGRect(x: 15, y: emptyHomeLabel.frame.maxY/2 + 10, width: contentContainer.bounds.width - 15, height: carouselPagesContainer.bounds.height/2))
        //Setting text to emptyHomeSubLabel
        emptyHomeSubLabel.text = NSLocalizedString("Clean_Home", comment: "")
        //Setting font to text
        emptyHomeSubLabel.font = UIFont(name: "Helvetica", size: 16)
        //Setting color to text
        emptyHomeSubLabel.textColor = UIColor.gray
        //Adding emptyHomeSubLabel into emptyHomeContainer
        emptyHomeContainer.addSubview(emptyHomeSubLabel)
        //Adding empty home container to contentContainer
        contentContainer.addSubview(emptyHomeContainer)*/
        
        /**
         * Languages container
         */
        
        //Creating the languages container
        languagesContainer = UIView(frame: CGRect(x: 0, y: carouselPagesContainer.frame.maxY, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/7 - 2.5))
        //Setting border bottom to languages container
        languagesContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating UIlabel for languages
        let languagesLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: languagesContainer.bounds.height/2))
        //Setting text to languagesLabel
        languagesLabel.text = NSLocalizedString("Languages", comment: "")
        //Adding disableShakeLabel into languagesContainer
        languagesContainer.addSubview(languagesLabel)
        //Creating UIlabel for languages sub label
        let languagesSubLabel = UILabel(frame: CGRect(x: 15, y: languagesLabel.frame.maxY/2 + 10, width: contentContainer.bounds.width - 15, height: carouselPagesContainer.bounds.height/2))
        
        switch UserDefaults.standard.string(forKey: "currentLanguageCode") {
            case "default"?:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("Default", comment: "")
            case "en-US"?:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("English", comment: "")
            case "es"?:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("Spanish", comment: "")
            case "it"?:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("Italian", comment: "")
            case "ru"?:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("Russian", comment: "")
            case "de"?:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("German", comment: "")
            case "fr"?:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("French", comment: "")
            case "pt-PT"?:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("Portuguese", comment: "")
            case "zh-Hant"?:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("Traditional_Chinese", comment: "")
            case "zh-Hans"?:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("Simplified_Chinese", comment: "")
            default:
                //Setting text to languagesSubLabel
                languagesSubLabel.text = NSLocalizedString("Default", comment: "")
        }
        
        //Setting color to text
        languagesSubLabel.textColor = UIColor.gray
        //Adding languagesSubLabel into languagesContainer
        languagesContainer.addSubview(languagesSubLabel)
        //Adding languages container to contentContainer
        contentContainer.addSubview(languagesContainer)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
