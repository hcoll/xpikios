//
//  PreferencesController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/17/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class PreferencesController: UIViewController {
    
    /**
     * Class Attributes
     */
    
    //Preferences view variable
    var preferencesView: PreferencesView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    /**
     * Notifies the view controller that its view is about to be added to a view hierarchy
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Creating a preferencesView instance
        preferencesView = PreferencesView(frame: CGRect.zero)
        
        //Getting backButton from preferencesView and add target
        preferencesView.backButton.addTarget(self, action: #selector(backToHomePage), for: .touchUpInside)
        
        //Getting safe search switch from preferencesView and add target
        //preferencesView.safeSearchSwitch.addTarget(self, action: #selector(safeSearchSwitchChange), for: .valueChanged)
        
        //Getting disable audio switch from preferencesView and add target
        //preferencesView.disableAudioSwitch.addTarget(self, action: #selector(disableAudioSwitchChange), for: .valueChanged)
        
        //Getting disable shake switch from preferencesView and add target
        preferencesView.disableShakeSwitch.addTarget(self, action: #selector(disableShakeSwitchChange), for: .valueChanged)
        
        //Getting empty switch from preferencesView and add target
        //preferencesView.emptySwitch.addTarget(self, action: #selector(emptyHomeSwitchChange), for: .valueChanged)
        
        /*//Setting Safe/Unsafe Search
        if let safeSearch = UserDefaults.standard.string(forKey: "safeSearch"){
            
            if(safeSearch == "true"){
                preferencesView.safeSearchSwitch.setOn(true, animated: false)
            }else{
                preferencesView.safeSearchSwitch.setOn(false, animated: false)
            }
            
        }else{
            preferencesView.safeSearchSwitch.setOn(false, animated: false)
        }
        
        //Setting Enable/Disable Audio
        if let enableAudio = UserDefaults.standard.string(forKey: "enableAudio"){
            
            if(enableAudio == "true"){
                preferencesView.disableAudioSwitch.setOn(true, animated: false)
            }else{
                preferencesView.disableAudioSwitch.setOn(false, animated: false)
            }
            
        }else{
            preferencesView.disableAudioSwitch.setOn(false, animated: false)
        }*/
        
        //Setting Enable/Disable shake
        if let enableShake = UserDefaults.standard.string(forKey: "enableShake"){
            
            if(enableShake == "true"){
                preferencesView.disableShakeSwitch.setOn(true, animated: false)
            }else{
                preferencesView.disableShakeSwitch.setOn(false, animated: false)
            }
           
        }else{
            preferencesView.disableShakeSwitch.setOn(false, animated: false)
        }
        
        /*//Setting Empty/Fill home
        if let emptyHome = UserDefaults.standard.string(forKey: "emptyHome"){
            
            if(emptyHome == "true"){
                preferencesView.emptySwitch.setOn(true, animated: false)
            }else{
                preferencesView.emptySwitch.setOn(false, animated: false)
            }
            
        }else{
            preferencesView.emptySwitch.setOn(false, animated: false)
        }*/
        
        //Creating carouselPagesTapGesture and add target
        let carouselPagesTapGesture = UITapGestureRecognizer(target: self, action: #selector(setCarouselPages))
        //Setting to carouselPagesContainer isUserInteractionEnabled true
        preferencesView.carouselPagesContainer.isUserInteractionEnabled = true
        //Adding to carouselPagesContainer gesture
        preferencesView.carouselPagesContainer.addGestureRecognizer(carouselPagesTapGesture)
        
        //Creating languagesTapGesture and add target
        let languagesTapGesture = UITapGestureRecognizer(target: self, action: #selector(setLanguage))
        //Setting to languagesContainer isUserInteractionEnabled true
        preferencesView.languagesContainer.isUserInteractionEnabled = true
        //Adding to languagesContainer gesture
        preferencesView.languagesContainer.addGestureRecognizer(languagesTapGesture)
        
        //Setting preferencesView to app view
        self.view = preferencesView
    }
    
    /**
     * Sent to the view controller when the app receives a memory warning
     */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /**
     * Back to home page: pop current controller
     */
    
    func backToHomePage() {
        let auxHomeCont = Shared.shared.homeController
        
        Shared.shared.homeController = HomePageController()
        
        auxHomeCont?.navigationController?.pushViewController(Shared.shared.homeController, animated: false)
    }
    
    func emptyHomeSwitchChange(sender:UISwitch!){
        
        if (sender.isOn == true){
            UserDefaults.standard.set("true", forKey: "emptyHome")
        }
        else{
            UserDefaults.standard.set("false", forKey: "emptyHome")
        }
    }
    
    func safeSearchSwitchChange(sender:UISwitch!){
        
        if (sender.isOn == true){
            UserDefaults.standard.set("true", forKey: "safeSearch")
        }
        else{
            UserDefaults.standard.set("false", forKey: "safeSearch")
        }
    }
    
    func disableAudioSwitchChange(sender:UISwitch!){
        
        if (sender.isOn == true){
            UserDefaults.standard.set("true", forKey: "enableAudio")
        }
        else{
            UserDefaults.standard.set("false", forKey: "enableAudio")
        }
    }
    
    func disableShakeSwitchChange(sender:UISwitch!){
        
        if (sender.isOn == true){
            UserDefaults.standard.set("true", forKey: "enableShake")
        }
        else{
            UserDefaults.standard.set("false", forKey: "enableShake")
        }
    }
    
    func setCarouselPages() {
        
        //Creating pagesPickerController object
        let pagesPickerController = PagesPickerController()
        //Setting modalPresentationStyle option
        pagesPickerController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //Open date picker modal
        self.navigationController?.present(pagesPickerController, animated: true, completion: nil)
        
    }
    
    func setLanguage() {
        
        Shared.shared.languageTableType = true
        //Creating pagesPickerController object
        let pagesPickerController = LanguagesTableViewController()
        //Setting modalPresentationStyle option
        pagesPickerController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //Open date picker modal
        self.navigationController?.present(pagesPickerController, animated: true, completion: nil)
    }
}
