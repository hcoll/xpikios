//
//  PreviewController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 2/15/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class PreviewController: UIViewController {
    
    /**
     * Class Attributes
     */
    
    //Preview variable
    var preview: Preview!
    
    let containerView = UIView()
    
    //Share view variable
    //var shareView: ShareView!
    
    //Node button variable
    var nodeButton: UIButton!
    
    var buttonNode: CircleMenu!
    
    let items = ["ic_plus","ic_minus","ic_arrow_down","ic_denied","ic_cancel","ic_equal","none"]
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    /**
     * Notifies the view controller that its view is about to be added to a view hierarchy
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Creating a preview instance
        preview = Preview(frame: CGRect.zero)
        
        /*//Initializing share view
        shareView = ShareView(frame: CGRect.zero)
        
        //Creating shareTapGesture and add target
        let shareTapGesture = UITapGestureRecognizer(target: self, action: #selector(closeShareModal))
        //Setting to shareView isUserInteractionEnabled true
        shareView.mainView.isUserInteractionEnabled = true
        //Adding to shareView gesture
        shareView.mainView.addGestureRecognizer(shareTapGesture)*/
        
        //Getting backButton from preview and add target
        preview.backButton.addTarget(self, action: #selector(backToHomePage), for: .touchUpInside)
        
        //Getting shareButton from shareView and add target
        preview.shareButton.addTarget(self, action: #selector(shareXpik), for: .touchUpInside)
        
        //Getting downloadButton from shareView and add target
        preview.downloadButton.addTarget(self, action: #selector(saveXpik), for: .touchUpInside)
        
        setupScrollView()
        setupImages()
        
        //Setting preview to app view
        self.view = preview
    }
    
    /**
     * Sent to the view controller when the app receives a memory warning
     */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupScrollView(){
        
        let height = CGFloat(220 * Shared.shared.currentCarousel.count)
        let width = preview.contentContainer.bounds.width
        
        preview.contentContainer.contentSize = CGSize(width: width, height: height)
        
        //Delegating the contentContainer from preview
        preview.contentContainer.delegate = self
        
        containerView.frame = CGRect(x: 0, y: 0, width: width, height: height)
        containerView.backgroundColor = UIColor.white
        
        let minZoomScale = min(UIScreen.main.bounds.width/width, UIScreen.main.bounds.height/height)
        preview.contentContainer.minimumZoomScale = minZoomScale
        preview.contentContainer.maximumZoomScale = 1.0
        
        preview.contentContainer.setZoomScale(preview.contentContainer.minimumZoomScale, animated: false)
        self.containerView.center.x =  preview.contentContainer.center.x
        
        self.containerView.isUserInteractionEnabled = true
        
        // Gesture Recogniser (Double Tap)
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        self.containerView.addGestureRecognizer(tap)
        
        //Getting downloadButton from shareView and add target
        //shareView.downloadButton.addTarget(self, action: #selector(saveXpik), for: .touchUpInside)
        
        //Getting sendButton from shareView and add target
        //shareView.sendButton.addTarget(self, action: #selector(shareXpik), for: .touchUpInside)
    }
    
    /**
     * Double tap method: Double tap to zoom-in/zoom-out
     */
    
    func doubleTapped() {
        
        if(preview.contentContainer.zoomScale > preview.contentContainer.minimumZoomScale){
            //Zoom-out
            preview.contentContainer.setZoomScale(preview.contentContainer.minimumZoomScale, animated: true)
            self.containerView.center.x =  preview.contentContainer.center.x
        }
        else{
            //Zoom-in
            preview.contentContainer.setZoomScale(preview.contentContainer.maximumZoomScale, animated: true)
            self.containerView.center.x =  preview.contentContainer.center.x
        }

    }
    
    func setupImages(){
        var yPosition = 0
        
        //Getting thumbnails from each carousel
        for i in 0..<Shared.shared.currentCarousel.count{
            
            //Getting carousel array
            let carousel = Shared.shared.currentCarousel[i]
            
            //Creating alamo fire source array
            var alamofireSource = [AlamofireSource]()
            
            //Setting image url into alamofireSource
            alamofireSource.append(AlamofireSource(urlString: carousel.thumbnail[carousel.position])!)
            
            let imageView = ImageSlideshow(frame: CGRect(x: 0, y: yPosition, width: Int(containerView.bounds.width), height: 220))
            //Setting images to image slide show
            imageView.setImageInputs(alamofireSource)
            imageView.contentMode = .scaleAspectFit
            //ImageView border
            imageView.addBottomBorderWithColor(color: UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0), width: 2.5)
            
            containerView.addSubview(imageView)
            
            if(i > 0){
                
                //Creating nodeButton
                nodeButton = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
                //Centering nodeButton into containerView
                nodeButton.center = CGPoint(x: imageView.bounds.width/2, y: (CGFloat(yPosition)))
                //Adding back image
                nodeButton.setImage(UIImage(named: items[Shared.shared.currentCarousel[i].type]), for: .normal)
                //Adding nodeButton to containerView
                containerView.addSubview(nodeButton)
            }
            
            yPosition += 220
            
        }
        preview.contentContainer.addSubview(containerView)
    }
    
    /**
     * Back to home page: pop current controller
     */
    
    func backToHomePage() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * Get xpik method: take screenshot to get xpik
     */
    
    func getXpik() -> UIImage{
        
            preview.contentContainer.setZoomScale(preview.contentContainer.maximumZoomScale, animated: false)
            self.containerView.center.x =  preview.contentContainer.center.x
        
            var image = UIImage();
            UIGraphicsBeginImageContextWithOptions(preview.contentContainer.contentSize, false, UIScreen.main.scale)
            
            // save initial values
            let savedContentOffset = preview.contentContainer.contentOffset;
            let savedFrame = preview.contentContainer.frame;
            let savedBackgroundColor = preview.contentContainer.backgroundColor
            
            // reset offset to top left point
            preview.contentContainer.contentOffset = CGPoint(x: 0, y: 0);
            // set frame to content size
            preview.contentContainer.frame = CGRect(x: 0, y: 0, width: preview.contentContainer.contentSize.width, height: preview.contentContainer.contentSize.height);
            // remove background
            preview.contentContainer.backgroundColor = UIColor.clear
            
            // make temp view with scroll view content size
            // a workaround for issue when image on ipad was drawn incorrectly
            let tempView = UIView(frame: CGRect(x: 0, y: 0, width: preview.contentContainer.contentSize.width, height: preview.contentContainer.contentSize.height));
            
            // save superview
            let tempSuperView = preview.contentContainer.superview
            // remove scrollView from old superview
            preview.contentContainer.removeFromSuperview()
            // and add to tempView
            tempView.addSubview(preview.contentContainer)
            
            // render view
            // drawViewHierarchyInRect not working correctly
            tempView.layer.render(in: UIGraphicsGetCurrentContext()!)
            // and get image
            image = UIGraphicsGetImageFromCurrentImageContext()!;
            
            // and return everything back
            tempView.subviews[0].removeFromSuperview()
            tempSuperView?.addSubview(preview.contentContainer)
            tempSuperView?.addSubview(preview.navigationBar)
            tempSuperView?.addSubview(preview.bottomContainer)
        
            // restore saved settings
            preview.contentContainer.contentOffset = savedContentOffset;
            preview.contentContainer.frame = savedFrame;
            preview.contentContainer.backgroundColor = savedBackgroundColor
            
            UIGraphicsEndImageContext();
        
            return image
    }
    
    /*/**
     * Open share modal method: Open share modal to download or send xpik
     */
    
    func openShareModal() {
        self.view.addSubview(shareView.mainView)
    }
    
    /**
     * Close share modal method: Close share modal to download or send xpik
     */
    
    func closeShareModal() {
        shareView.mainView.removeFromSuperview()
    }*/
    
    /**
     * Share xpik method: Share the current xpik
     */
    
    func shareXpik() {
        
        //closeShareModal()
        
        // Set up activity view controller
        let activityViewController = UIActivityViewController(activityItems: [getXpik()], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // Exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // Present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    /**
     * Save xpik method: Save the current xpik into gallery
     */
    
    func saveXpik() {
        //closeShareModal()
        UIImageWriteToSavedPhotosAlbum(getXpik(), self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: NSLocalizedString("Saved", comment: ""), message: NSLocalizedString("Save_Gallery", comment: ""), preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default))
            present(ac, animated: true)
        }
    }
}

extension PreviewController: UIScrollViewDelegate{
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return containerView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        
        //Get the maxZoomScale of the scrollView
        
        let maxZoomScale = scrollView.maximumZoomScale
        
        //Center the container view
        
        self.containerView.center.x = self.preview.contentContainer.center.x
        
        //Disable scroll when maxZoomScale == scale
        
        if maxZoomScale == scale{

            scrollView.isScrollEnabled = true
        }else{

            scrollView.isScrollEnabled = false
        }
    }
    
}
