//
//  Preview.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 2/15/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class Preview: UIView {
    
    /**
     * Class Attributes
     */
    
    //Back button variable
    var backButton: UIButton!
    
    //Hide node button variable
    var hideNodeButton: UIButton!
    
    //Content view variable
    var contentContainer: UIScrollView!
    
    //Download button variable
    var downloadButton: UIButton!
    
    //Share button variable
    var shareButton: UIButton!
    
    //Save button variable
    var saveButton: UIButton!
    
    //Send button variable
    var sendButton: UIButton!
    
    //Navigation bar variable
    var navigationBar: UIView!
    
    //Bottom container variable
    var bottomContainer: UIView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Setting the background color to the main view
        mainView.backgroundColor = UIColor.white
        //Adding mainView to view
        addSubview(mainView)
        
        //Creating logoImageView
        let speechBackground = UIImageView(frame: CGRect(x: 0, y: 0, width: mainView.frame.width, height: mainView.frame.height))
        speechBackground.image = UIImage(named:"speech_background")
        speechBackground.contentMode = .scaleAspectFill
        mainView.addSubview(speechBackground)
        
        //Creating status bar
        let statusBar = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30))
        //Setting the background color to the status bar
        statusBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding statusBar to mainView
        mainView.addSubview(statusBar)
        
        /**
         * Content container
         */
        
        //Creating the content container
        contentContainer = UIScrollView(frame: CGRect(x: 0, y: statusBar.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Adding content container to mainView
        mainView.addSubview(contentContainer)
        
        /**
         * Navigation bar
         */
        
        //Creating navigation bar
        navigationBar = UIView(frame:CGRect(x: 0, y: statusBar.frame.maxY, width: UIScreen.main.bounds.width, height: 65))
        //Adding navigationBar to mainView
        mainView.addSubview(navigationBar)
        
        /**
         * Back button
         */
        
        //Creating back button container
        let backButtonContainer = UIView(frame:CGRect(x: 0, y: 0, width: 70, height: 70))
        //Creating backButton
        backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        //Centering backButton button into backButtonContainer
        backButton.center = CGPoint(x: backButtonContainer.bounds.width/2, y: statusBar.bounds.height + 5)
        //Adding back image
        backButton.setImage(UIImage(named: "ic_back_preview"), for: .normal)
        //Adding backButton to backButtonContainer
        backButtonContainer.addSubview(backButton)
        //Adding backButtonContainer to navigationBar
        navigationBar.addSubview(backButtonContainer)
        
        /**
         * Right button container
         */
        
        //Creating right button container
        let rightButtonContainer = UIView(frame:CGRect(x: navigationBar.bounds.width - 70, y: 0, width: 70, height: 70))
        //Creating hideNodeButton
        hideNodeButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 55))
        //Centering hideNodeButton button into rightButtonContainer
        hideNodeButton.center = CGPoint(x: rightButtonContainer.bounds.width/2, y: statusBar.bounds.height + 5)
        //Adding back image
        hideNodeButton.setImage(UIImage(named: "ic_hide"), for: .normal)
        //Adding hideNodeButton to rightButtonContainer
        rightButtonContainer.addSubview(hideNodeButton)
        //Adding rightButtonContainer to navigationBar
        //navigationBar.addSubview(rightButtonContainer)
        
        /**
         * Bottom container
         */
        
        //Creating the bottom container
        bottomContainer = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 60, width: UIScreen.main.bounds.width, height: 60))
        //Adding bottom container to mainView
        mainView.addSubview(bottomContainer)
        
        /**
         * Download button
         */
        
        //Creating right button container
        let downloadContainer = UIView(frame:CGRect(x: 0, y: 0, width: 70, height: 70))
        //Creating downloadButton
        downloadButton = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        //Centering downloadButton button into downloadContainer
        downloadButton.center = CGPoint(x: downloadContainer.bounds.width/2, y: 20)
        //Adding back image
        downloadButton.setImage(#imageLiteral(resourceName: "ic_download"), for: .normal)
        //Adding shareButton to downloadContainer
        downloadContainer.addSubview(downloadButton)
        //Adding downloadContainer to bottomContainer
        bottomContainer.addSubview(downloadContainer)
        
        /**
         * Share button
         */
        
        //Creating right button container
        let shareContainer = UIView(frame:CGRect(x: bottomContainer.bounds.width - 70, y: 0, width: 70, height: 70))
        //Creating shareButton
        shareButton = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        //Centering shareButton button into shareContainer
        shareButton.center = CGPoint(x: shareContainer.bounds.width/2, y: 20)
        //Adding back image
        shareButton.setImage(#imageLiteral(resourceName: "ic_send"), for: .normal)
        //Adding shareButton to shareContainer
        shareContainer.addSubview(shareButton)
        //Adding shareContainer to bottomContainer
        bottomContainer.addSubview(shareContainer)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
