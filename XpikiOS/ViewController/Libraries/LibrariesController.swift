//
//  LibrariesController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/5/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class LibrariesController: GridController {
    
    /**
     * Class Attributes
     */
    
    //Categories view variable
    var categoriesView: DataCollectionView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        gridCollection.reloadData()
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    /**
     * Notifies the view controller that its view is about to be added to a view hierarchy
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Shared.shared.currentXpikDefault = Shared.shared.dataCollectionArray
        
        //Creating a categoriesView instance
        categoriesView = DataCollectionView(frame: CGRect.zero)
        
        //Getting backButton from categoriesView and add target
        categoriesView.backButton.addTarget(self, action: #selector(backToHomePage), for: .touchUpInside)
        
        //Getting searchButton from categoriesView and add target
        categoriesView.searchButton.addTarget(self, action: #selector(headerSearchView), for: .touchUpInside)
        
        //Setting width and height to gridCollection
        gridCollection.frame = CGRect(x: 0, y: 0, width: categoriesView.contentContainer.bounds.width, height: categoriesView.contentContainer.bounds.height)
        //Setting data source to gridCollection
        gridCollection.dataSource = self
        //Delegating the gridCollection
        gridCollection.delegate = self
        //Adding gridCollection into contentContainer
        categoriesView.contentContainer.addSubview(gridCollection)
        
        //Setting categoriesView to app view
        self.view = categoriesView
    }
    
    /**
     * Sent to the view controller when the app receives a memory warning
     */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /**
     * Back to home page: pop current controller
     */
    
    func backToHomePage() {
        Shared.shared.currentCarousel = Shared.shared.currentHomeCarousel
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //Setting the favorite index carousels selected
        Shared.shared.collectionDataIndex = indexPath.row
        
        Shared.shared.libraryController = LibrariesViewController()
        
        //Initializing FavoritesEditorController
        self.navigationController?.pushViewController(Shared.shared.libraryController, animated: false)
    }
}
