//
//  LibrariesViewController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/6/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class LibrariesViewController: SpeechController {
 
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Shared.shared.currentController = Shared.shared.libraryController
        
        Shared.shared.currentCarousel = Shared.shared.currentXpikDefault[Shared.shared.collectionDataIndex].carousels
        
        speechView.categoriesButton.isEnabled = false
        
        /**
         * Back button
         */
        
        //Creating backButton
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Setting width to left container
        speechView.mainCanvas.leftButtonContainer.frame = CGRect(x: 0, y: 0, width: 80, height: speechView.mainCanvas.leftButtonContainer.frame.height)
        //Centering back button into leftButtonContainer
        backButton.center = CGPoint(x: speechView.mainCanvas.leftButtonContainer.bounds.width/2, y: speechView.mainCanvas.leftButtonContainer.bounds.height - 20)
        //Adding menu icon to backButton
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        //Adding tint color to backButton
        backButton.tintColor = UIColor.white
        //Adding backButton to leftButtonContainer
        speechView.mainCanvas.leftButtonContainer.addSubview(backButton)
        
        //Getting menuButton from libraryView and add target
        backButton.addTarget(self, action: #selector(backToHomePage), for: .touchUpInside)
    }
    
    /**
     * Back to home page: pop current controller
     */
    
    func backToHomePage() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
