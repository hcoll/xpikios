//
//  LibrariesController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/22/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class CategoriesController: CategoriesCollection {
    
    /**
     * Class Attributes
     */
    
    //Categories view variable
    var categoriesView: CategoriesView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    /**
     * Notifies the view controller that its view is about to be added to a view hierarchy
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Shared.shared.inHomePage = false
        
        //Creating a categoriesView instance
        categoriesView = CategoriesView(frame: CGRect.zero)
        
        //Getting backButton from librariesView and add target
        categoriesView.backButton.addTarget(self, action: #selector(backToHomePage), for: .touchUpInside)
        
        //Setting width and height to tableView
        tableView.frame = CGRect(x: 0, y: 0, width: categoriesView.contentContainer.bounds.width, height: categoriesView.contentContainer.bounds.height)
        //Setting data source to tableView
        tableView.dataSource = self
        //Delegating the tableView
        tableView.delegate = self
        //Adding tableView into contentContainer
        categoriesView.contentContainer.addSubview(tableView)
        
        //Setting categoriesView to app view
        self.view = categoriesView
        
    }
    
    /**
     * Sent to the view controller when the app receives a memory warning
     */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /**
     * Back to home page: pop current controller
     */
    
    func backToHomePage() {
        
        Shared.shared.inHomePage = true
        
        let auxHomeCont = Shared.shared.homeController
        
        Shared.shared.homeController = HomePageController()
        
        auxHomeCont?.navigationController?.pushViewController(Shared.shared.homeController, animated: false)
    }
}
