//
//  LibrariesCollection.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/2/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class CategoriesCollection: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView = UITableView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        //Setting none the separator style
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
    }
    
    /**
     * Setting number of sections (categories)
     */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Shared.shared.categories.count
    }
    
    /**
     * Return the number of rows in the section
     */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Shared.shared.categories[section].subcategories.count
    }
    
    /**
     * Adding height to header to each section
     */
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    
    /**
     * Adding header to each section
     */
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let sectionView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 45))
        sectionView.backgroundColor = UIColor.gray
        
        let sectionName = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.frame.size.width - 15, height: 40))
        sectionName.center = CGPoint(x: sectionView.bounds.width/2, y: sectionView.bounds.height/2)
        sectionName.text = Shared.shared.categories[section].name
        sectionName.textColor = UIColor.white
        sectionName.font = UIFont.systemFont(ofSize: 18)
        sectionName.textAlignment = .left
        
        sectionView.addSubview(sectionName)
        return sectionView
    }
    
    /**
     * Setting values to cell
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "Cell")
        
        cell.textLabel?.text = Shared.shared.categories[indexPath.section].subcategories[indexPath.row].name
    
        return cell
    }
    
    /**
     * Tells the delegate that the specified row is now selected
     */
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        //Libraries shared variable
        var libraries: [DataCollection] = []
        
        for library in Shared.shared.categories[indexPath.section].subcategories[indexPath.row].libraries {
            libraries.append(DataCollection(library.title,
                                       library.carousel))
        }
        Shared.shared.dataCollectionArray = libraries
        
        self.navigationController?.pushViewController(LibrariesController(), animated: false)
        
        //Change the selected background view of the cell.
        tableView.deselectRow(at: indexPath, animated: true)

    }
}
