//
//  LibrariesView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/16/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class CategoriesView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Back button variable
    var backButton: UIButton!
    
    //Content view variable
    var contentContainer: UIView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Creating a mainCanvas object
        let secondaryCanvas = SecondaryCanvas(frame: CGRect.zero)
        //Adding mainView from secondaryCanvas to principal view
        addSubview(secondaryCanvas.mainView)
        
        /**
         * Back button
         */
        
        //Creating backButton
        backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering backButton button into leftButtonContainer
        backButton.center = CGPoint(x: secondaryCanvas.leftButtonContainer.bounds.width/2, y: secondaryCanvas.leftButtonContainer.bounds.height - 20)
        //Adding back image
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        //Adding backButton to leftButtonContainer
        secondaryCanvas.leftButtonContainer.addSubview(backButton)
        
        //Getting content container variable from secondary canvas
        contentContainer = secondaryCanvas.contentContainer
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
