//
//  FilterView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/1/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class FilterView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Back button variable
    var backButton: UIButton!
    
    //Save button variable
    var saveButton: UIButton!
    
    //Content view variable
    var contentContainer: UIView!
    
    //Image View variable
    var imageView: UIImageView!
    
    //Bottom container variable
    var bottomContainer: UIView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Creating a main view with all the elements involved
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Adding background to mainView
        mainView.backgroundColor = UIColor.white
        
        //Creating status bar
        let statusBar = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 22))
        //Setting the background color to the status bar
        statusBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding statusBar to mainView
        mainView.addSubview(statusBar)
        
        //Creating navigation bar
        let navigationBar = UIView(frame:CGRect(x: 0, y: statusBar.frame.maxY, width: UIScreen.main.bounds.width, height: 65))
        //Setting the background color to the navigation bar
        navigationBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding navigationBar to mainView
        mainView.addSubview(navigationBar)
        
        /**
         * Back button
         */
        
        //Creating back button container
        let backButtonContainer = UIView(frame:CGRect(x: 0, y: 0, width: 80, height: navigationBar.bounds.height))
        //Creating backButton
        backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering backButton button into backButtonContainer
        backButton.center = CGPoint(x: backButtonContainer.bounds.width/2, y: backButtonContainer.bounds.height - 20)
        //Adding back image
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        //Adding backButton to backButtonContainer
        backButtonContainer.addSubview(backButton)
        //Adding backButtonContainer to navigationBar
        navigationBar.addSubview(backButtonContainer)
        
        /**
         * Save button
         */
        
        //Creating save button container
        let saveButtonContainer = UIView(frame:CGRect(x: mainView.bounds.width - (mainView.bounds.width/6), y: 0, width: mainView.bounds.width/6, height: navigationBar.bounds.height))
        //Creating saveButton
        saveButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        //Centering saveButton button into saveButtonContainer
        saveButton.center = CGPoint(x: saveButtonContainer.bounds.width/2, y: saveButtonContainer.bounds.height/2)
        //Adding save image
        saveButton.setImage(UIImage(named: "ic_check"), for: .normal)
        //Adding saveButton to saveButtonContainer
        saveButtonContainer.addSubview(saveButton)
        //Adding saveButtonContainer to navigationBar
        navigationBar.addSubview(saveButtonContainer)
        
        /**
         * Xpik logo
         */
        
        //Creating title container
        let titleContainer = UIView(frame:CGRect(x: backButtonContainer.frame.maxX, y: 0, width: mainView.bounds.width - (backButtonContainer.bounds.width + saveButtonContainer.bounds.width), height: navigationBar.bounds.height))
        //Creating logoImageView
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 45))
        //Centering logoImageView button into titleContainer
        logoImageView.center = CGPoint(x: titleContainer.bounds.width/2, y: titleContainer.bounds.height/2)
        //Adding xpikLogoTextWhite to logoImageView
        logoImageView.image = UIImage(named:"ic_logo_text_xpik")
        //Adding the logoImageView into the titleContainer
        titleContainer.addSubview(logoImageView)
        //Adding titleContainer to navigationBar
        navigationBar.addSubview(titleContainer)
        
        /**
         * Bottom container
         */
        
        //Creating the bottom container
        bottomContainer = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 100, width: UIScreen.main.bounds.width, height: 100))
        //Setting background color to bottomContainer
        bottomContainer.backgroundColor = UIColor.black
        //Adding bottom container to mainView view
        mainView.addSubview(bottomContainer)
        
        /**
         * Content container
         */
        
        //Creating the content container
        let contentContainer = UIView(frame: CGRect(x: 0, y: navigationBar.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (navigationBar.bounds.height + statusBar.bounds.height + bottomContainer.bounds.height)))
        //Adding content container to mainView
        mainView.addSubview(contentContainer)
        
        //Creating imageView
        imageView = UIImageView(frame:CGRect(x: 0, y: 0, width: contentContainer.bounds.width, height: contentContainer.bounds.height))
        imageView.contentMode = .scaleAspectFit
        contentContainer.addSubview(imageView)
        
        addSubview(mainView)
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
