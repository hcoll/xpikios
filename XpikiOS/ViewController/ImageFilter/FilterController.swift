//
//  FilterController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 2/28/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

public protocol FilterControllerDelegate {
    func shViewControllerImageDidFilter(image: UIImage)
    func shViewControllerDidCancel()
}

import Foundation
import UIKit

@objcMembers
class FilterController: FilterCollection {
    
    /**
     * Class Attributes
     */
    
    public var delegate: FilterControllerDelegate?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    /**
     * Notifies the view controller that its view is about to be added to a view hierarchy
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Creating a favoritesView instance
        filterView = FilterView(frame: CGRect.zero)
        
        //Adding target to cancelButton
        filterView.backButton.addTarget(self, action: #selector(cancelEdit), for: .touchUpInside)
        
        //Adding target to saveButton
        filterView.saveButton.addTarget(self, action: #selector(saveImage), for: .touchUpInside)
        
        //Setting image to imageView
        filterView.imageView.image = image
        
        //Setting width and height to filterCollection
        filterCollection.frame = CGRect(x: 0, y: 0, width: filterView.bottomContainer.bounds.width, height: filterView.bottomContainer.bounds.height)
        //Setting data source to filterCollection
        filterCollection.dataSource = self
        //Delegating the filterCollection
        filterCollection.delegate = self
        //Adding filterCollection into bottomContainer
        filterView.bottomContainer.addSubview(filterCollection)
        
        //Setting favoritesView to app view
        self.view = filterView
    }
    
    /**
     * Sent to the view controller when the app receives a memory warning
     */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cancelEdit() {
        if let delegate = self.delegate {
            delegate.shViewControllerDidCancel()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func saveImage() {
        if let delegate = self.delegate {
            delegate.shViewControllerImageDidFilter(image: (filterView.imageView?.image)!)
        }
        dismiss(animated: true, completion: nil)
    }
    
}
