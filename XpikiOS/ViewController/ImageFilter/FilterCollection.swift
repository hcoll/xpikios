//
//  FilterCollection.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/1/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class FilterCollection: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //Filter collection variable
    var filterCollection: UICollectionView!
    
    //Filter view variable
    var filterView: FilterView!
    
    //Image variable
    var image: UIImage?
    
    let context = CIContext(options: nil)
    
    let filterNameList = [
        "No Filter",
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectMono",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer",
        "CILinearToSRGBToneCurve",
        "CISRGBToneCurveToLinear"
    ]
    
    let filterDisplayNameList = [
        "Normal",
        "Chrome",
        "Fade",
        "Instant",
        "Mono",
        "Noir",
        "Process",
        "Tonal",
        "Transfer",
        "Tone",
        "Linear"
    ]
    
    public init(image: UIImage) {
        super.init(nibName: nil, bundle: nil)
        self.image = image
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Cell layout configuration
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 2, left: 5, bottom: 5, right: 5)
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 80, height: 100)
        
        //Filter collection configuration
        filterCollection = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        filterCollection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "CellIdentifier")
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterNameList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellIdentifier", for: indexPath)
        
        //Creating Filter Image View
        let filterImageView = UIImageView(frame:CGRect(x: 0, y: 0, width: cell.frame.size.width, height: 70))
        var filteredImage = UIImage(named:"filter_image")
        if indexPath.row != 0 {
            filteredImage = createFilteredImage(filterName: filterNameList[indexPath.row], image: UIImage(named:"filter_image")!)
        }
        filterImageView.image = filteredImage
        filterImageView.contentMode = .scaleAspectFit
        cell.addSubview(filterImageView)
        
        let textLabel = UILabel(frame:CGRect(x: 0, y: filterImageView.frame.maxY, width: cell.frame.size.width, height: 30))
        textLabel.backgroundColor = UIColor.black
        textLabel.textColor = UIColor.white
        textLabel.textAlignment = .center
        let textForLabel = filterDisplayNameList[indexPath.row]
        textLabel.text = textForLabel
        
        cell.addSubview(textLabel)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row != 0 {
            applyFilter(filterIndex: indexPath)
        } else {
            filterView.imageView?.image = image
        }
    }
    
    func applyFilter(filterIndex: IndexPath) {
        let filterName = filterNameList[filterIndex.row]
        if let image = self.image {
            let filteredImage = createFilteredImage(filterName: filterName, image: image)
            filterView.imageView?.image = filteredImage
        }
    }
    
    func createFilteredImage(filterName: String, image: UIImage) -> UIImage {
        // 1 - create source image
        let sourceImage = CIImage(image: image)
        
        // 2 - create filter using name
        let filter = CIFilter(name: filterName)
        filter?.setDefaults()
        
        // 3 - set source image
        filter?.setValue(sourceImage, forKey: kCIInputImageKey)
        
        // 4 - output filtered image as cgImage with dimension.
        let outputCGImage = context.createCGImage((filter?.outputImage!)!, from: (filter?.outputImage!.extent)!)
        
        // 5 - convert filtered CGImage to UIImage
        let filteredImage = UIImage(cgImage: outputCGImage!)
        
        return filteredImage
    }
}
