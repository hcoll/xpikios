//
//  HomePageController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/21/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class HomePageController: SpeechController, CircleMenuDelegate {
    
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Shared.shared.currentController = Shared.shared.homeController
        
        /**
         * Menu button
         */
        
        //Creating menuButton
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 20))
        //Centering menuButton button into leftButtonContainer
        menuButton.center = CGPoint(x: speechView.mainCanvas.leftButtonContainer.bounds.width/2, y: speechView.mainCanvas.leftButtonContainer.bounds.height/2)
        //Adding menu icon to menuButton
        menuButton.setImage(UIImage(named: "ic_menu"), for: .normal)
        //Adding tint color to menuButton
        menuButton.tintColor = UIColor.white
        //Adding menuButton to leftButtonContainer
        speechView.mainCanvas.leftButtonContainer.addSubview(menuButton)
        
        //Getting menuButton from HomePageView and add target
        menuButton.addTarget(self, action: #selector(openMenu), for: .touchUpInside)
        
        //Creating side menu object
        sideMenuView = SideMenuView(frame: CGRect.zero)
        
        //Creating sideMainViewTapGesture and add target
        let sideMainViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(openMenu))
        //Setting to sideMainView isUserInteractionEnabled true
        sideMenuView.blackView.isUserInteractionEnabled = true
        //Adding to sideMainView gesture
        sideMenuView.blackView.addGestureRecognizer(sideMainViewTapGesture)
        
        //Adding black view to principal view
        self.view.addSubview(sideMenuView.blackView)
        
        //Adding side menu to principal view
        self.view.addSubview(sideMenuView.sideView)
        
    }
    
    /**
     * Go to categories method: Go to categories (libraries) section
     */
    
    func goToCategories(){
        Shared.shared.currentHomeCarousel = Shared.shared.currentCarousel
        self.navigationController?.pushViewController(CategoriesController(), animated: false)
    }
    
    /**
     * Open menu method: Open side menu
     */
    
    func openMenu(){
        
        if(self.sideMenuView.sideView.frame.origin.x != 0.0){
            
            //Setting transparent background to black view
            self.sideMenuView.blackView.backgroundColor = UIColor.black.withAlphaComponent(0)
            
            UIView.animate(withDuration: 0.35, animations: { () -> Void in
                self.sideMenuView.sideView.frame = CGRect(x: 0, y: 22, width: self.sideMenuView.sideView.bounds.width, height: self.sideMenuView.sideView.bounds.height)
                self.sideMenuView.blackView.frame = CGRect(x: 0, y: 22, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
                
            },completion: { finished in })
            
        }else{
            
            //Setting clear background to black view
            self.sideMenuView.blackView.backgroundColor = UIColor.clear
            
            UIView.animate(withDuration: 0.35, animations: { () -> Void in
                self.sideMenuView.sideView.frame = CGRect(x: -self.view.frame.width, y: 22, width:  self.sideMenuView.sideView.bounds.width, height:  self.sideMenuView.sideView.bounds.height)
                self.sideMenuView.blackView.frame = CGRect(x: -self.view.frame.width, y: 22, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            },completion: { finished in })
        }
    }
    
    /**
     * Change view method: Navigate to another view
     */
    
    func changeView(menu: Int) {
        
        if menu == 0 || menu == 1{
            
            openMenu()
            
        }
        else if menu == 2 {
            self.navigationController?.pushViewController(CategoriesController(), animated: false)
            openMenu()
            
        }else if menu == 3 {
            
            self.navigationController?.pushViewController(PreferencesController(), animated: false)
            openMenu()
            
        }else if menu == 4 {
            
            self.navigationController?.pushViewController(HelpController(), animated: false)
            openMenu()
            
        }
    }
}
