//
//  EditorView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/16/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class EditorView: UIView {
    
    /**
     * Class Attributes
     */
    
    //MainCanvas variable
    var mainCanvas: MainCanvas!
    
    //Back button variable
    var backButton: UIButton!
    
    //Info button variable
    var infoButton: UIButton!
    
    //Content view variable
    var contentContainer: UIView!
    
    //Crop button variable
    var cropButton: UIButton!
    
    //Filter button variable
    var filterButton: UIButton!
    
    //Font button variable
    var fontButton: UIButton!
    
    //Share button variable
    var shareButton: UIButton!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Creating a mainCanvas object
        mainCanvas = MainCanvas(frame: CGRect.zero)
        //Adding mainView from mainCanvas to principal view
        addSubview(mainCanvas.mainView)

        
        /**
         * Back button
         */
        
        //Creating backButton
        backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        mainCanvas.leftButtonContainer.frame = CGRect(x: 0, y: 0, width: 80, height: mainCanvas.leftButtonContainer.frame.height)
        //Centering backButton button into leftButtonContainer
        backButton.center = CGPoint(x: mainCanvas.leftButtonContainer.bounds.width/2, y: mainCanvas.leftButtonContainer.bounds.height - 20)
        //Adding back image
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        //Adding backButton to leftButtonContainer
        mainCanvas.leftButtonContainer.addSubview(backButton)
        
        /**
         * Info button
         */
        
        //Creating infoButton
        infoButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering infoButton button into rightButtonContainer
        infoButton.center = CGPoint(x: mainCanvas.rightButtonContainer.bounds.width/2, y: mainCanvas.rightButtonContainer.bounds.height - 20)
        //Adding menu icon to infoButton
        infoButton.setImage(UIImage(named: "ic_info"), for: .normal)
        //Adding tint color to infoButton
        infoButton.tintColor = UIColor.white
        //Adding infoButton to rightButtonContainer
        mainCanvas.rightButtonContainer.addSubview(infoButton)
        
        //Getting content container variable from main canvas
        contentContainer = mainCanvas.contentContainer
        //Adding background color to contentContainer
        contentContainer.backgroundColor = UIColor.white
        
        /**
         * Crop button
         */
        
        //Creating the crop container
        let cropContainer = UIView(frame: CGRect(x: 0, y: 0, width: mainCanvas.bottomContainer.bounds.width/4, height: mainCanvas.bottomContainer.bounds.height))
        //Creating crop button
        cropButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering cropButton button into cropContainer
        cropButton.center = CGPoint(x: cropContainer.bounds.width/2, y: mainCanvas.bottomContainer.bounds.height - 30)
        //Setting icon to cropButton
        cropButton.setImage(#imageLiteral(resourceName: "ic_crop"), for: .normal)
        //Adding cropButton to cropContainer
        cropContainer.addSubview(cropButton)
        //Adding cropContainer to bottomContainer
        mainCanvas.bottomContainer.addSubview(cropContainer)
        
        /**
         * Filter button
         */
        
        //Creating the filter container
        let filterContainer = UIView(frame: CGRect(x: cropContainer.frame.maxX, y: 0, width: mainCanvas.bottomContainer.bounds.width/4, height: mainCanvas.bottomContainer.bounds.height))
        //Creating filter button
        filterButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering filter button into filterContainer
        filterButton.center = CGPoint(x: filterContainer.bounds.width/2, y: mainCanvas.bottomContainer.bounds.height - 30)
        //Setting icon to filterButton
        filterButton.setImage(#imageLiteral(resourceName: "ic_filter"), for: .normal)
        //Adding filterButton to filterContainer
        filterContainer.addSubview(filterButton)
        //Adding filterContainer to bottomContainer
        mainCanvas.bottomContainer.addSubview(filterContainer)
        
        /**
         * Font button
         */
        
        //Creating the font container
        let fontContainer = UIView(frame: CGRect(x: filterContainer.frame.maxX, y: 0, width: mainCanvas.bottomContainer.bounds.width/4, height: mainCanvas.bottomContainer.bounds.height))
        //Creating font button
        fontButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering font button into fontContainer
        fontButton.center = CGPoint(x: fontContainer.bounds.width/2, y: mainCanvas.bottomContainer.bounds.height - 30)
        //Setting icon to fontButton
        fontButton.setImage(#imageLiteral(resourceName: "ic_font"), for: .normal)
        //Adding fontButton to fontContainer
        fontContainer.addSubview(fontButton)
        //Adding fontContainer to bottomContainer
        mainCanvas.bottomContainer.addSubview(fontContainer)
        
        /**
         * Share button
         */
        
        //Creating the share container
        let shareContainer = UIView(frame: CGRect(x: fontContainer.frame.maxX, y: 0, width: mainCanvas.bottomContainer.bounds.width/4, height: mainCanvas.bottomContainer.bounds.height))
        //Creating share button
        shareButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering share button into shareContainer
        shareButton.center = CGPoint(x: shareContainer.bounds.width/2, y: mainCanvas.bottomContainer.bounds.height - 30)
        //Setting icon to shareButton
        shareButton.setImage(#imageLiteral(resourceName: "ic_share_white"), for: .normal)
        //Adding shareButton to shareContainer
        shareContainer.addSubview(shareButton)
        //Adding shareContainer to bottomContainer
        mainCanvas.bottomContainer.addSubview(shareContainer)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
