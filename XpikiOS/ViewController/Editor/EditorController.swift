//
//  EditorController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 12/29/17.
//  Copyright © 2017 HacheColl. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import FirebaseStorage

@objcMembers
class EditorController: UIViewController, PhotoEditorDelegate, CropViewControllerDelegate, FilterControllerDelegate {
   
    let stickers = ["ic_check_sticker","ic_check_2_sticker","ic_denied_sticker","ic_denied_2_sticker","ic_cancel_sticker","ic_cancel_2_sticker"]
    
    //Editor view variable
    var editorView: EditorView!
    
    var cropController: CropViewController!
    
    var currentImageSlideShow: ImageSlideshow!
    
    var slideshow: ImageSlideshow = {
        let slideshow = ImageSlideshow()
        slideshow.zoomEnabled = true
        slideshow.singleTapEnabled = true
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFit
        slideshow.pageControlPosition = PageControlPosition.insideScrollView
        slideshow.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        return slideshow
    }()
    
    /// Closure called on page selection
    var pageSelected: ((_ page: Int) -> Void)?
    
    /// Index of initial image
    var initialPage: Int = 0
    
    /// Input sources to
    var inputs: [InputSource]?
    
    /// Background color
    var backgroundColor = UIColor.white
    
    /// Enables/disable zoom
    var zoomEnabled = true {
        didSet {
            slideshow.zoomEnabled = zoomEnabled
        }
    }
    
    /// Enables/disable single tap
    var singleTapEnable = true {
        didSet {
            slideshow.singleTapEnabled = singleTapEnable
        }
    }
    
    //Current position
    var currentPosition: Int!
    
    //Carousel selected
    var currentCarousel: Carousel!
    
    //Xpik table view variable
    var xpikTable: UITableView!
    
    //Original content container width
    var containerWidth: CGFloat!
    
    //Original content container height
    var containerHeight: CGFloat!
    
    //Original content container y position
    var containerY: CGFloat!
    
    //Landscape width variable
    var landscapeWidth: CGFloat!
    
    //Landscape height variable
    var landscapeHeight: CGFloat!
    
    fileprivate var isInit = true
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        if UIDevice.current.orientation.isLandscape {
            editorView.contentContainer.frame = CGRect(x: 0.0, y: 0.0, width: landscapeWidth, height: landscapeHeight)
        } else {
            editorView.contentContainer.frame = CGRect(x: 0.0, y: containerY, width: containerWidth, height: containerHeight)
        }
    }
    
    /**
     * Notifies the view controller that its view is about to be added to a view hierarchy
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isInit {
            isInit = false
            slideshow.setCurrentPage(initialPage, animated: false)
        }
        
        // Hide the navigation bar on this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Landscape width variable
        landscapeWidth = self.view.frame.height
        
        //Setting landscape height variable
        landscapeHeight = self.view.frame.width
        
        //Initializing home page view
        editorView = EditorView(frame: CGRect.zero)
        
        //Getting backButton from EditorView and add target
        editorView.backButton.addTarget(self, action: #selector(backToHomePage), for: .touchUpInside)
        
        //Getting cropButton from EditorView and add target
        editorView.cropButton.addTarget(self, action: #selector(crop), for: .touchUpInside)
        
        //Getting filterButton from EditorView and add target
        editorView.filterButton.addTarget(self, action: #selector(filter), for: .touchUpInside)
        
        //Getting fontButton from EditorView and add target
        editorView.fontButton.addTarget(self, action: #selector(edit), for: .touchUpInside)
        
        //Getting shareButton from EditorView and add target
        editorView.shareButton.addTarget(self, action: #selector(shareImage), for: .touchUpInside)
        
        //Getting infoButton from EditorView and add target
        editorView.infoButton.addTarget(self, action: #selector(getImageInfo), for: .touchUpInside)
        
        //Setting editorView to app view
        self.view = editorView
        
        //Setting images to slideshow
        if let inputs = inputs {
            slideshow.setImageInputs(inputs)
            for i in 0..<inputs.count {
                slideshow.slideshowItems[i].imageView.image = currentImageSlideShow.slideshowItems[i].imageView.image
            }
        }
        
        //Vertical scroll disable
        if #available(iOS 11.0, *) {
            slideshow.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
            automaticallyAdjustsScrollViewInsets = false
        }
        
        //Setting width/height to slideshow
        slideshow.frame = CGRect(x: 0, y: 0, width: editorView.contentContainer.bounds.width, height: editorView.contentContainer.bounds.height)
        
        //Getting content container width
        containerWidth = editorView.contentContainer.frame.width
        
        //Getting content container height
        containerHeight = editorView.contentContainer.frame.height
        
        //Getting content container y position
        containerY = editorView.contentContainer.frame.origin.y
        
        //Adding slideshow to contentContainer
        editorView.contentContainer.addSubview(slideshow)
    }
    
    /**
     * Hide bars method: Hide bars after single tap
     */
    
    func hideBars() {
        if(self.view.frame.width != landscapeWidth){
            
            UIView.animate(withDuration: 0.3, delay: 0, options: [], animations: {
                
                if(self.slideshow.singleTapEnabled){
                    self.editorView.mainCanvas.navigationBar.frame.origin.y = -(2*self.editorView.mainCanvas.navigationBar.frame.origin.y + self.editorView.mainCanvas.statusBar.frame.height)
                    self.editorView.mainCanvas.bottomContainer.frame.origin.y = 2*self.editorView.mainCanvas.bottomContainer.frame.origin.y
                }else{
                    self.editorView.mainCanvas.navigationBar.frame.origin.y = self.editorView.mainCanvas.statusBar.frame.height
                    self.editorView.mainCanvas.bottomContainer.frame.origin.y = self.view.frame.height - 60
                }
                
            }, completion: { _ in
                //self.yourView.hidden = true // Here you hide it when animation done
            })
            
            slideshow.singleTapEnabled = !slideshow.singleTapEnabled
        }
    }
    
    /**
     * Updating image of the carousel
     */
    
    func setToCarousel(image: UIImage){
        
        let indexPath = IndexPath(row: self.currentPosition, section: 0)
        let cell = xpikTable.cellForRow(at: indexPath) as! CarouselView
        cell.imageSlideShow.setCurrentPage(slideshow.currentPage, animated: false)
        cell.imageSlideShow.currentSlideshowItem?.imageView.image = image
    }
    
    /**
     * Updating image with crop
     */
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        controller.dismiss(animated: true, completion: nil)
        
        uploadToFirebase(newImage: image)
        slideshow.currentSlideshowItem?.imageView.image = image
        setToCarousel(image: image)
    }
    
    /**
     * Cancel crop
     */
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    /**
     * Updating image with editor features
     */
    
    func doneEditing(image: UIImage) {
        
        uploadToFirebase(newImage: image)
        slideshow.currentSlideshowItem?.imageView.image = image
        setToCarousel(image: image)
    }
    
    /**
     * Cancel editor
     */
    
    func canceledEditing() {
        print("Canceled")
    }
    
    /**
     * Updating image with filter
     */
    
    func shViewControllerImageDidFilter(image: UIImage) {
        
        uploadToFirebase(newImage: image)
        slideshow.currentSlideshowItem?.imageView.image = image
        setToCarousel(image: image)
    }
    
    /**
     * Cancel filter method
     */
    
    func shViewControllerDidCancel() {
    }
    
    /**
     * Back to homepage
     */
    
    func backToHomePage() {
        // if pageSelected closure set, send call it with current page
        if let pageSelected = pageSelected {
            pageSelected(slideshow.currentPage)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     * Crop method: Crop the current image
     */
    
    func crop(){
        
        let controller = CropViewController()
        controller.delegate = self
        controller.image = slideshow.currentSlideshowItem?.imageView.image
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: true, completion: nil)
        
    }
    
    /**
     * Filter method: Add filter to the current image
     */
    
    func filter(){
        
        let filterController = FilterController(image: (slideshow.currentSlideshowItem?.imageView.image)!)
        filterController.delegate = self
        let navController = UINavigationController(rootViewController: filterController)
        present(navController, animated: true, completion: nil)
    }
    
    /**
     * Edit method: Open the image editor view
     */
    
    func edit() {
        
        let photoEditor = PhotoEditorViewController(nibName:"PhotoEditorViewController",bundle: Bundle(for: PhotoEditorViewController.self))
        
        photoEditor.photoEditorDelegate = self

       photoEditor.image = slideshow.currentSlideshowItem?.imageView.image

        //Colors for drawing and Text, If not set default values will be used
        //        photoEditor.colors = [.red,.blue,.green]
        
        //Stickers that the user will choose from to add on the image
        for sticker in stickers {
            photoEditor.stickers.append(UIImage(named: sticker )!)
        }
        
        
        //To hide controls - array of enum control
        photoEditor.hiddenControls = [.crop]
        
        present(photoEditor, animated: true, completion: nil)
    }
    
    /**
     * Upload to Firebase method: Upload the image edited to firebase storage
     */
    
    func uploadToFirebase(newImage: UIImage) {
        
        //Setting the firebase bucket route
        let route = "images/"+"IMG_"+String(Date().millisecondsSince1970)+".jpg"
        
        //Getting the firebase bucket reference
        let imageRef = Storage.storage().reference().child(route)
        
        //Upload image to firebase method
        FirebaseService.uploadImage(newImage, at: imageRef) { (downloadURL) in
            
            //Getting the download url of the firebase bucket
            guard let downloadURL = downloadURL else {
                return
            }
            
            //Getting the url in string format
            let urlString = downloadURL.absoluteString
            
            Shared.shared.currentCarousel[self.currentPosition].thumbnail[self.slideshow.currentPage] = urlString
            
        }
    }
    
    /**
     * Share image method:Share the current image
     */
    
    func shareImage() {
        
        // Set up activity view controller
        let activityViewController = UIActivityViewController(activityItems: [slideshow.currentSlideshowItem?.imageView.image as! UIImage], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // Exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // Present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func getImageInfo() {
    
        UIApplication.shared.open(NSURL(string: currentCarousel.webpage[slideshow.currentPage])! as URL, options: [:], completionHandler: nil)
    }
    
    func resizeImage(image: UIImage) -> UIImage {
        
        UIGraphicsBeginImageContext(CGSize(width: 20, height: 30))
        image.draw(in: CGRect(x: 0, y: 0, width: 20, height: 30))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension Notification.Name {
    static let peru = Notification.Name("peru")
}
