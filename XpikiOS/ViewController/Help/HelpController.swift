//
//  HelpController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 5/22/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

@objcMembers
class HelpController: UIViewController, MFMailComposeViewControllerDelegate {
    
    /**
     * Class Attributes
     */
    
    //Help view variable
    var helpView: HelpView!
    
    //Slideshow variable
    var tutorialContainer: UIView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    /**
     * Notifies the view controller that its view is about to be added to a view hierarchy
     */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Creating a helpView instance
        helpView = HelpView(frame: CGRect.zero)
        
        //Getting backButton from helpView and add target
        helpView.backButton.addTarget(self, action: #selector(backToHomePage), for: .touchUpInside)
        
        //Creating tutorialTapGesture and add target
        let tutorialTapGesture = UITapGestureRecognizer(target: self, action: #selector(tutorial))
        //Setting to tutorialContainer isUserInteractionEnabled true
        helpView.tutorialContainer.isUserInteractionEnabled = true
        //Adding to tutorialContainer gesture
        helpView.tutorialContainer.addGestureRecognizer(tutorialTapGesture)
        
        //Creating youtubeTapGesture and add target
        let youtubeTapGesture = UITapGestureRecognizer(target: self, action: #selector(youtube))
        //Setting to youtubeContainer isUserInteractionEnabled true
        helpView.youtubeContainer.isUserInteractionEnabled = true
        //Adding to youtubeContainer gesture
        helpView.youtubeContainer.addGestureRecognizer(youtubeTapGesture)
        
        //Creating contactTapGesture and add target
        let contactTapGesture = UITapGestureRecognizer(target: self, action: #selector(contact))
        //Setting to contactContainer isUserInteractionEnabled true
        helpView.contactContainer.isUserInteractionEnabled = true
        //Adding to contactContainer gesture
        helpView.contactContainer.addGestureRecognizer(contactTapGesture)
        
        //Creating webTapGesture and add target
        let webTapGesture = UITapGestureRecognizer(target: self, action: #selector(website))
        //Setting to webContainer isUserInteractionEnabled true
        helpView.webContainer.isUserInteractionEnabled = true
        //Adding to webContainer gesture
        helpView.webContainer.addGestureRecognizer(webTapGesture)
        
        //Creating privacyTapGesture and add target
        let privacyTapGesture = UITapGestureRecognizer(target: self, action: #selector(privacy))
        //Setting to privacyContainer isUserInteractionEnabled true
        helpView.privacyContainer.isUserInteractionEnabled = true
        //Adding to privacyContainer gesture
        helpView.privacyContainer.addGestureRecognizer(privacyTapGesture)
        
        //Setting preferencesView to app view
        self.view = helpView
    }
    
    /**
     * Back to home page: pop current controller
     */
    
    func backToHomePage() {
        let auxHomeCont = Shared.shared.homeController
        
        Shared.shared.homeController = HomePageController()
        
        auxHomeCont?.navigationController?.pushViewController(Shared.shared.homeController, animated: false)
    }
    
    /**
     * Tutorial method: open the tutorial slideshow
     */
    
    func tutorial() {
        
        tutorialContainer = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        tutorialContainer.backgroundColor = UIColor.black
        self.view.addSubview(tutorialContainer)
        
        let slideShow = ImageSlideshow(frame: CGRect(x: 0, y: 22, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 22))
        slideShow.contentScaleMode = UIViewContentMode.scaleAspectFit
        //Vertical scroll disable
        if #available(iOS 11.0, *) {
            slideShow.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
            automaticallyAdjustsScrollViewInsets = false
        }
        
        slideShow.setImageInputs([
        ImageSource(image: UIImage(named: "tutorial_1_"+isoLanguageCode(languageCode: Shared.shared.currentLanguageCode))!),
        ImageSource(image: UIImage(named: "tutorial_2_"+isoLanguageCode(languageCode: Shared.shared.currentLanguageCode))!),
        ImageSource(image: UIImage(named: "tutorial_3_"+isoLanguageCode(languageCode: Shared.shared.currentLanguageCode))!),
        ImageSource(image: UIImage(named: "tutorial_4_"+isoLanguageCode(languageCode: Shared.shared.currentLanguageCode))!)
        ])

        tutorialContainer.addSubview(slideShow)
        
        /**
         * Back button
         */
        
        //Creating back button container
        let backButtonContainer = UIView(frame:CGRect(x: UIScreen.main.bounds.width - 70, y: 22, width: 70, height: 70))
        //Creating backButton
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 55))
        //Centering backButton button into backButtonContainer
        backButton.center = CGPoint(x: backButtonContainer.bounds.width/2, y: backButtonContainer.bounds.height/2)
        //Adding back image
        backButton.setImage(UIImage(named: "ic_back_tutorial"), for: .normal)
        //Adding target to backButton
        backButton.addTarget(self, action: #selector(closeTutorial), for: .touchUpInside)
        //Adding backButton to backButtonContainer
        backButtonContainer.addSubview(backButton)
        //Adding backButtonContainer to view
        tutorialContainer.addSubview(backButtonContainer)
        //Set hidden the back button
        backButtonContainer.isHidden = true
        
        //Changing image slideshow position of the current row
        slideShow.currentPageChanged = { page in
            if page == 3{
                backButtonContainer.isHidden = false
            }else{
                backButtonContainer.isHidden = true
            }
        }
        
    }
    
    /**
     * Close tutorial method: Close the tutorial and back to help section
     */
    
    func closeTutorial(){
        
        self.tutorialContainer.removeFromSuperview()
    }
    
    /**
     * Youtube method: link to youtube xpik channel
     */
    
    func youtube() {
         UIApplication.shared.open(NSURL(string: "https://www.youtube.com/channel/UCHQ-TqdelQ2gOS7f6AKrmSg")! as URL, options: [:], completionHandler: nil)
    }
    
    /**
     * Contact method: Send email to support
     */
    
    func contact() {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    /**
     * Email configuration method: Configure the email compose
     */
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["support@xpik.io"])
        mailComposerVC.setSubject("Support")
        mailComposerVC.setMessageBody("Your message here", isHTML: false)
        
        return mailComposerVC
    }
    
    /**
     * Send email error method: Show an alert with the error
     */
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: NSLocalizedString("Send_Email_Title", comment: ""), message: NSLocalizedString("Send_Email_Msg", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("Ok", comment: ""))
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    /**
     * Website method: link to xpik website
     */
    
    func website() {
        UIApplication.shared.open(NSURL(string: "https://xpik.io/")! as URL, options: [:], completionHandler: nil)
    }
    
    /**
     * Privacy method: link to xpik privacy and policy document
     */
    
    func privacy() {
        UIApplication.shared.open(NSURL(string: "https://xpik.io/privacypolicy.htm")! as URL, options: [:], completionHandler: nil)
    }
}
