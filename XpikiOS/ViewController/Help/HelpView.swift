//
//  HelpView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 6/21/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class HelpView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Title label variable
    var titleLabel: UIView!
    
    //Back button variable
    var backButton: UIButton!
    
    //Content view variable
    var contentContainer: UIView!
    
    //Tutorial container variable
    var tutorialContainer: UIView!
    
    //Youtube container variable
    var youtubeContainer: UIView!
    
    //Contact container variable
    var contactContainer: UIView!
    
    //Web container variable
    var webContainer: UIView!
    
    //Privacy container variable
    var privacyContainer: UIView!
    
    //Version container variable
    var versionContainer: UIView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Creating a mainCanvas object
        let secondaryCanvas = SecondaryCanvas(frame: CGRect.zero)
        //Adding mainView from secondaryCanvas to principal view
        addSubview(secondaryCanvas.mainView)
        
        /**
         * Back button
         */
        
        //Creating backButton
        backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering backButton button into leftButtonContainer
        backButton.center = CGPoint(x: secondaryCanvas.leftButtonContainer.bounds.width/2, y: secondaryCanvas.leftButtonContainer.bounds.height - 20)
        //Adding back image
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        //Adding backButton to leftButtonContainer
        secondaryCanvas.leftButtonContainer.addSubview(backButton)
        
        //Getting content container variable from secondary canvas
        contentContainer = secondaryCanvas.contentContainer
        
        /**
         * Help title
         */
        
        //Creating the title label
        titleLabel = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 100))
        //Title label background color
        titleLabel.backgroundColor = UIColor.white
        //Setting border bottom to title label
        titleLabel.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating the help icon UIImageView
        let helpLogo = UIImageView(frame:CGRect(x: 0, y: 0, width: 70, height: 70))
        //Centering helpLogo in emojiContainer
        helpLogo.center = CGPoint(x: titleLabel.center.x, y: titleLabel.center.y)
        //Adding logo to helpLogo
        helpLogo.image = UIImage(named: "help_logo")
        //Adding helpLogo to titleLabel
        titleLabel.addSubview(helpLogo)
        //Adding titleLabel to contentContainer
        contentContainer.addSubview(titleLabel)
        
        /**
         * Tutorial container
         */
        
        //Creating the tutorial container
        tutorialContainer = UIView(frame: CGRect(x: 0, y: titleLabel.frame.maxY + 5, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/8 - 2.5))
        //Setting border bottom to tutorial container
        tutorialContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating UIlabel for tutorial
        let tutorialLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: tutorialContainer.bounds.height/2))
        //Setting text to tutorialLabel
        tutorialLabel.text = NSLocalizedString("Instructions", comment: "")
        //Adding tutorialLabel into tutorialContainer
        tutorialContainer.addSubview(tutorialLabel)
        //Creating UIlabel for tutorial
        let tutorialSubLabel = UILabel(frame: CGRect(x: 15, y: tutorialLabel.frame.maxY/2 + 10, width: contentContainer.bounds.width - 15, height: tutorialContainer.bounds.height/2))
        //Setting text to tutorialSubLabel
        tutorialSubLabel.text = NSLocalizedString("Inst_Description", comment: "")
        //Setting font to text
        tutorialSubLabel.font = UIFont(name: "Helvetica", size: 12)
        //Setting color to text
        tutorialSubLabel.textColor = UIColor.gray
        //Adding tutorialSubLabel into tutorialContainer
        tutorialContainer.addSubview(tutorialSubLabel)
        //Adding tutorial container to contentContainer
        contentContainer.addSubview(tutorialContainer)
        
        /**
         * Youtube container
         */
        
        //Creating the youtube container
        youtubeContainer = UIView(frame: CGRect(x: 0, y: tutorialContainer.frame.maxY + 10, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/8 - 2.5))
        //Setting border bottom to youtube container
        youtubeContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating UIlabel for youtube
        let youtubeLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: youtubeContainer.bounds.height/2))
        //Setting text to youtube Label
        youtubeLabel.text = NSLocalizedString("Youtube_Channel", comment: "")
        //Adding youtubeLabel into youtubeContainer
        youtubeContainer.addSubview(youtubeLabel)
        //Creating UIlabel for youtube
        let youtubeSubLabel = UILabel(frame: CGRect(x: 15, y: youtubeLabel.frame.maxY/2 + 10, width: contentContainer.bounds.width - 15, height: youtubeContainer.bounds.height/2))
        //Setting text to youtubeSubLabel
        youtubeSubLabel.text = NSLocalizedString("Youtube_Description", comment: "")
        //Setting font to text
        youtubeSubLabel.font = UIFont(name: "Helvetica", size: 12)
        //Setting color to text
        youtubeSubLabel.textColor = UIColor.gray
        //Adding youtubeSubLabel into youtubeContainer
        youtubeContainer.addSubview(youtubeSubLabel)
        //Adding youtube container to contentContainer
        contentContainer.addSubview(youtubeContainer)
        
        /**
         * Contact container
         */
        
        //Creating the contact container
        contactContainer = UIView(frame: CGRect(x: 0, y: youtubeContainer.frame.maxY + 10, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/8 - 2.5))
        //Setting border bottom to contact container
        contactContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating UIlabel for contact
        let contactLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: contactContainer.bounds.height/2))
        //Setting text to contact Label
        contactLabel.text = NSLocalizedString("Contact_us", comment: "")
        //Adding contactLabel into contactContainer
        contactContainer.addSubview(contactLabel)
        //Creating UIlabel for contact
        let contactSubLabel = UILabel(frame: CGRect(x: 15, y: contactLabel.frame.maxY/2 + 10, width: contentContainer.bounds.width - 15, height: contactContainer.bounds.height/2))
        //Setting text to contactSubLabel
        contactSubLabel.text = NSLocalizedString("Contact_Description", comment: "")
        //Setting font to text
        contactSubLabel.font = UIFont(name: "Helvetica", size: 12)
        //Setting color to text
        contactSubLabel.textColor = UIColor.gray
        //Adding contactSubLabel into contactContainer
        contactContainer.addSubview(contactSubLabel)
        //Adding contact container to contentContainer
        contentContainer.addSubview(contactContainer)
        
        /**
         * Web container
         */
        
        //Creating the web container
        webContainer = UIView(frame: CGRect(x: 0, y: contactContainer.frame.maxY + 10, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/8 - 2.5))
        //Setting border bottom to web container
        webContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating UIlabel for web
        let webLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: webContainer.bounds.height/2))
        //Setting text to web Label
        webLabel.text = NSLocalizedString("Web_Page", comment: "")
        //Adding webLabel into webContainer
        webContainer.addSubview(webLabel)
        //Creating UIlabel for web
        let webSubLabel = UILabel(frame: CGRect(x: 15, y: webLabel.frame.maxY/2 + 10, width: contentContainer.bounds.width - 15, height: webContainer.bounds.height/2))
        //Setting text to webSubLabel
        webSubLabel.text = NSLocalizedString("Web_Description", comment: "")
        //Setting font to text
        webSubLabel.font = UIFont(name: "Helvetica", size: 12)
        //Setting color to text
        webSubLabel.textColor = UIColor.gray
        //Adding webSubLabel into webContainer
        webContainer.addSubview(webSubLabel)
        //Adding web container to contentContainer
        contentContainer.addSubview(webContainer)
        
        /**
         * Privacy container
         */
        
        //Creating the privacy container
        privacyContainer = UIView(frame: CGRect(x: 0, y: webContainer.frame.maxY + 10, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/8 - 2.5))
        //Setting border bottom to privacy container
        privacyContainer.addBottomBorderWithColor(color: UIColor.gray.withAlphaComponent(0.5), width: 1)
        //Creating UIlabel for privacy
        let privacyLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: privacyContainer.bounds.height/2))
        //Setting text to privacy Label
        privacyLabel.text = NSLocalizedString("Privacy_Policy", comment: "")
        //Adding privacyLabel into privacyContainer
        privacyContainer.addSubview(privacyLabel)
        //Creating UIlabel for privacy
        let privacySubLabel = UILabel(frame: CGRect(x: 15, y: privacyLabel.frame.maxY/2 + 10, width: contentContainer.bounds.width - 15, height: privacyContainer.bounds.height/2))
        //Setting text to privacySubLabel
        privacySubLabel.text = NSLocalizedString("Privacy_Description", comment: "")
        //Setting font to text
        privacySubLabel.font = UIFont(name: "Helvetica", size: 12)
        //Setting color to text
        privacySubLabel.textColor = UIColor.gray
        //Adding privacySubLabel into privacyContainer
        privacyContainer.addSubview(privacySubLabel)
        //Adding privacy container to contentContainer
        contentContainer.addSubview(privacyContainer)
        
        /**
         * Version container
         */
        
        //Creating the version container
        versionContainer = UIView(frame: CGRect(x: 0, y: privacyContainer.frame.maxY + 10, width: UIScreen.main.bounds.width, height: contentContainer.bounds.height/8 - 2.5))
        //Creating UIlabel for version
        let versionLabel = UILabel(frame: CGRect(x: 15, y: 0, width: contentContainer.bounds.width - 15, height: versionContainer.bounds.height/2))
        //Setting text to version Label
        versionLabel.text = NSLocalizedString("Version", comment: "")
        //Adding versionLabel into versionContainer
        versionContainer.addSubview(versionLabel)
        //Creating UIlabel for version
        let versionSubLabel = UILabel(frame: CGRect(x: 15, y: versionLabel.frame.maxY/2 + 10, width: contentContainer.bounds.width - 15, height: versionContainer.bounds.height/2))
        //Setting text to versionSubLabel
        versionSubLabel.text = NSLocalizedString("Version_Description", comment: "")
        //Setting font to text
        versionSubLabel.font = UIFont(name: "Helvetica", size: 12)
        //Setting color to text
        versionSubLabel.textColor = UIColor.gray
        //Adding versionSubLabel into versionContainer
        versionContainer.addSubview(versionSubLabel)
        //Adding version container to contentContainer
        contentContainer.addSubview(versionContainer)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
