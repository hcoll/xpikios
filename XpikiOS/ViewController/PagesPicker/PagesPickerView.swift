//
//  PreferencesPickerView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 2/8/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome_swift

class PagesPickerView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Accept button variable
    var acceptButton: UIButton!
    
    //Cancel button variable
    var cancelButton: UIButton!
    
    //Page picker variable
    var pagePicker: UIPickerView!
    
    /**
     * This method set an initial value for each stored property on that
     * instance and performing any other setup or initialization that is required before
     * the new instance is ready for use.
     */
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Setting transparent background to principal view
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Creating a main view with all the elements involved
        let mainView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 50, height: 280))
        //Centering mainView into principal view
        mainView.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        //Setting the background color (gradient) to the main view
        mainView.backgroundColor = UIColor.white
        
        /**
         * Title UILabel
         */
        
        //Creating title UILabel
        let titleLabel = UILabel(frame: CGRect(x: 20, y: 20, width: mainView.bounds.width - 20, height: 30))
        //Aligning text into titleLabel
        titleLabel.textAlignment = .left
        //Setting font to text
        titleLabel.font = UIFont(name: "Helvetica-bold", size: 20)
        //Setting color to text
        titleLabel.textColor = UIColor.black
        //Setting text
        titleLabel.text = NSLocalizedString("Carousel_Pages", comment: "")
        //Adding titleLabel to mainView
        mainView.addSubview(titleLabel)
        
        /**
         * Carousel page container
         */
        
        //Creating carouselPageContainer
        let carouselPageContainer = UIView(frame: CGRect(x: 0, y: titleLabel.frame.maxY + 10, width: mainView.frame.width, height: 150))
        //Setting background color to carousel page container
        carouselPageContainer.backgroundColor = UIColor.white
        //Adding carouselPageContainer to mainView
        mainView.addSubview(carouselPageContainer)
        
        /**
         * Page container
         */
        
        //Creating page container
        let pageContainer = UIView(frame: CGRect(x: 0, y: 0, width: carouselPageContainer.frame.width/3, height: carouselPageContainer.bounds.height))
        //Centering page container into carousel page container
        pageContainer.center = CGPoint(x: carouselPageContainer.bounds.width/2, y: carouselPageContainer.bounds.height/2)
        //Creating page picker view
        pagePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: pageContainer.frame.width, height: pageContainer.frame.height))
        //Adding page picker view to pageContainer
        pageContainer.addSubview(pagePicker)
        //Adding pageContainer to carouselPageContainer
        carouselPageContainer.addSubview(pageContainer)
        
        /**
         * Bottom Buttons container
         */
        
        //Creating a container for bottom buttons
        let bottomLabel = UIView(frame: CGRect(x: 0, y: carouselPageContainer.frame.maxY + 10, width: mainView.frame.width, height: 50))
        //Adding monthContainer to mainView
        mainView.addSubview(bottomLabel)
        
        /**
         * Accept button
         */
        
        //Creating accept button
        acceptButton = UIButton(frame: CGRect(x: bottomLabel.bounds.width - 100, y: 0, width: 100, height: 50))
        //Setting title to acceptButton
        acceptButton.setTitle(NSLocalizedString("Ok", comment: ""), for: .normal)
        //Setting text color
        acceptButton.setTitleColor(UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0), for: .normal)
        //Adding acceptButton to bottomLabel
        bottomLabel.addSubview(acceptButton)
        
        /**
         * Cancel button
         */
        
        //Creating cancel button
        cancelButton = UIButton(frame: CGRect(x: bottomLabel.bounds.width - 100 - acceptButton.bounds.width, y: 0, width: 100, height: 50))
        //Setting title to cancelButton
        cancelButton.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
        //Setting text color
        cancelButton.setTitleColor(UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0), for: .normal)
        //Adding cancelButton to bottomLabel
        bottomLabel.addSubview(cancelButton)
        
        //Adding mainView into principal view
        addSubview(mainView)
        
    }
    
    /**
     * This tell the compiler that code using the marked object or function should not compile
     */
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
