//
//  PagesPickerController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 2/8/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

@objcMembers
class PagesPickerController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    /**
     * Class Attributes
     */
    
    //Page picker view variable
    var pagePickerView: PagesPickerView!
    
    //Current page selected variable
    var currentPage: String!
    
    //Picker data (pages)
    var pickerPage = ["1","2","3","4","5","6","7"]
    
    /**
     * Called after the controller's view is loaded into memory
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Creating a pagePickerView instance
        pagePickerView = PagesPickerView(frame: CGRect.zero)
        
        //Setting pagePickerView to app view
        self.view = pagePickerView
        
        //Adding delegation to pagePicker
        pagePickerView.pagePicker.delegate = self
        pagePickerView.pagePicker.dataSource = self
        
        //Setting current page in the pagePicker
        if let usercurrentPage = UserDefaults.standard.string(forKey: "carouselPages"){
            currentPage = usercurrentPage
        }else{
            currentPage = "7"
        }
        pagePickerView.pagePicker.selectRow(pickerPage.index(of: currentPage)!, inComponent:0, animated:true)
        
        //Adding target to accept button
        pagePickerView.acceptButton.addTarget(self, action: #selector(acceptDate), for: .touchUpInside)
        
        //Adding target to cancel button
        pagePickerView.cancelButton.addTarget(self, action: #selector(cancelDate), for: .touchUpInside)
    }
    
    /**
     * Accept date method: return the date selected
     */
    
    func acceptDate() {
        
        UserDefaults.standard.set(currentPage, forKey: "carouselPages")
        let auxHomeCont = Shared.shared.homeController
        
        Shared.shared.homeController = HomePageController()
        
        auxHomeCont?.navigationController?.pushViewController(Shared.shared.homeController, animated: false)
        
        //Dismiss current view controller
        cancelDate()
    }
    
    /**
     * Cancel date method: return to sign up view
     */
    
    func cancelDate() {
        
        //Back to home page controller
        navigationController?.popViewController(animated: false)
        
        //Dismiss current view controller (PagePickerController)
        dismiss(animated: true, completion: nil)
    }
    
    /**
     * The number of columns of data
     */
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    /**
     * The number of rows of data
     */
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return pickerPage.count
    }
    
    /**
     * The data to return for the row and component (column) that's being passed in
     */
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        var rowContent: String!
        
       rowContent = pickerPage[row]
        
        return rowContent
    }
    
    /**
     * Return the value for specific row
     */
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch row {
        case 0:
            currentPage = "1"
        case 1:
            currentPage = "2"
        case 2:
            currentPage = "3"
        case 3:
            currentPage = "4"
        case 4:
            currentPage = "5"
        case 5:
            currentPage = "6"
        case 6:
            currentPage = "7"
        default:
            currentPage = "1"
        }
        
    }
}
