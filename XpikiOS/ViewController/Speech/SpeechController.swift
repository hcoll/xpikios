//
//  SpeechController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 6/19/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit
import Speech
import Alamofire
import ImageSlideshow
import Firebase
import FirebaseStorage
import Photos
import BSImagePicker
import Intents
import MaterialActivityIndicator

@objcMembers
class SpeechController: CarouselController, SFSpeechRecognizerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    /**
     * Class Attributes
     */
    
    //Search view variable
    var searchView: ModalView!
    
    //Color modal variable
    var colorModal: ColorModal!
    
    //Microphone modal view variable
    var microphoneView: MicrophoneModal!
    
    //Setting carousel pages
    var carouselPages: String!
    
    //Language table controller variable
    var languageTableController: LanguagesTableViewController!
    
    //Spinner view variable
    var spinnerView: UIView!
    
    //Upload view variable
    var uploadView: UploadView!
    
    //Keyboard notification variable
    var keyboardNotification: KeyboardNotification!
    
    //Side menu view variable
    var sideMenuView: SideMenuView!
    
    //Image picker from Gallery/Camera object
    var imagePicker = UIImagePickerController()
    
    //Assets selected from gallery
    var SelectedAssets: [PHAsset]!
    
    //Search view variable
    var deleteFavoriteView: DeleteFavoriteView!
    
    //Speech recognizer variables
    var audioPlayer:AVAudioPlayer!
    
    //Disable Speech Button variable
    var disableSpeechButton: Bool!
    
    //Emoji keyboard variable
    var emojiKeyboard: KeyboardView!
    
    //Emoji keyboard view variable
    var keyboardView: UIView!
    
    //Emojis array
    var emojis: [EmojisDetails] = []

    let audioEngine:AVAudioEngine? = AVAudioEngine()
    
    var speechRecognizer: SFSpeechRecognizer!
    
    var request: SFSpeechAudioBufferRecognitionRequest?
    
    var recognitionTask: SFSpeechRecognitionTask?
    
    var query: String?
    
    /**
     * Lock Orientation
     */
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        
        // Or to rotate and lock
        
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
        
    }
    
    /**
     * Notifies the view controller that its view is about to be added to a view hierarchy
     */
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        //Hide the navigation bar on this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        speechView.mainCanvas.statusBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: speechView.mainCanvas.statusBar.frame.height)
        
        speechView.mainCanvas.navigationBar.frame = CGRect(x: 0, y: speechView.mainCanvas.statusBar.frame.maxY, width: UIScreen.main.bounds.width, height: speechView.mainCanvas.navigationBar.frame.height)
        
        speechView.mainCanvas.leftButtonContainer.frame = CGRect(x: 0, y: 0, width: speechView.mainCanvas.navigationBar.bounds.width/6, height: speechView.mainCanvas.navigationBar.bounds.height)
        
        speechView.mainCanvas.rightButtonContainer.frame = CGRect(x: speechView.mainCanvas.navigationBar.bounds.width - (speechView.mainCanvas.navigationBar.bounds.width/6), y: 0, width: speechView.mainCanvas.navigationBar.bounds.width/6, height: speechView.mainCanvas.navigationBar.bounds.height)

        speechView.mainCanvas.rightLeftButtonContainer.frame = CGRect(x: speechView.mainCanvas.navigationBar.bounds.width - (speechView.mainCanvas.navigationBar.bounds.width/3), y: 0, width: speechView.mainCanvas.navigationBar.bounds.width/6, height: speechView.mainCanvas.navigationBar.bounds.height)
        
        speechView.mainCanvas.titleContainer.center.x = UIScreen.main.bounds.width/2
        
        speechView.mainCanvas.bottomContainer.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 70, width: UIScreen.main.bounds.width, height: 70)
        
        speechView.mainCanvas.bottomContainer.addTopBorderWithColor(color: UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0), width: 2.5)
        
        speechView.keyboardContainer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width/6, height: speechView.mainCanvas.bottomContainer.bounds.height)
        
        speechView.keyboardButton.center = CGPoint(x: speechView.keyboardContainer.bounds.width/2, y: speechView.keyboardContainer.bounds.height/2)
        
        speechView.categoriesContainer.frame = CGRect(x: speechView.keyboardContainer.frame.maxX, y: 0, width: UIScreen.main.bounds.width/6, height: speechView.mainCanvas.bottomContainer.bounds.height)
        
        speechView.categoriesButton.center = CGPoint(x: speechView.categoriesContainer.bounds.width/2, y: speechView.categoriesContainer.bounds.height/2)
        
        speechView.microphoneContainer.frame = CGRect(x: speechView.categoriesContainer.frame.maxX, y: 3, width: UIScreen.main.bounds.width - 4*(UIScreen.main.bounds.width/6), height: speechView.mainCanvas.bottomContainer.bounds.height)
        
        speechView.microphoneButton.center = CGPoint(x: speechView.microphoneContainer.bounds.width/2, y: speechView.microphoneContainer.bounds.height/2)
        
        speechView.cameraContainer.frame = CGRect(x: speechView.microphoneContainer.frame.maxX, y: 0, width: UIScreen.main.bounds.width/6, height: speechView.mainCanvas.bottomContainer.bounds.height)
        
        speechView.cameraButton.center = CGPoint(x: speechView.cameraContainer.bounds.width/2, y: speechView.cameraContainer.bounds.height/2)
        
        speechView.shareContainer.frame = CGRect(x: speechView.cameraContainer.frame.maxX, y: 0, width: UIScreen.main.bounds.width/6, height: speechView.mainCanvas.bottomContainer.bounds.height)
        
        speechView.shareButton.center = CGPoint(x: speechView.shareContainer.bounds.width/2, y: speechView.shareContainer.bounds.height/2)
        
        speechView.mainCanvas.contentContainer.frame = CGRect(x: 0, y: speechView.mainCanvas.navigationBar.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (speechView.mainCanvas.navigationBar.bounds.height + speechView.mainCanvas.statusBar.bounds.height + 70))
        
        speechView.speechBackground.frame = CGRect(x: 0, y: speechView.mainCanvas.navigationBar.frame.maxY, width: speechView.mainCanvas.contentContainer.frame.width, height: speechView.mainCanvas.contentContainer.frame.height)
        
        microphoneView.listeningIcon.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height - 100)
        
        searchView.mainView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        searchView.searchContainer.frame = CGRect(x: 0, y: 0, width: searchView.mainView.bounds.width - 20, height: 230)
        searchView.searchContainer.center = CGPoint(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        searchView.titleLabel.frame = CGRect(x: 0, y: 10, width: searchView.searchContainer.bounds.width, height: 50)
        searchView.searchText.center = CGPoint(x: searchView.searchContainer.bounds.width/2, y: searchView.titleLabel.bounds.height + 50)
        
        searchView.buttonsContainer.frame = CGRect(x: 0, y: searchView.searchText.frame.maxY, width: searchView.searchContainer.bounds.width, height: 100)
        
        searchView.cancelButton.frame = CGRect(x: 0, y: 0, width: searchView.buttonsContainer.bounds.width/2, height: 50)
        searchView.cancelButton.center = CGPoint(x: searchView.buttonsContainer.bounds.width/4, y: searchView.buttonsContainer.bounds.height/2)
        
        searchView.acceptButton.frame = CGRect(x: 0, y: 0, width: searchView.buttonsContainer.bounds.width/2, height: 50)
        searchView.acceptButton.center = CGPoint(x: searchView.buttonsContainer.bounds.width/2 + searchView.buttonsContainer.bounds.width/4, y: searchView.buttonsContainer.bounds.height/2)
        
        xpikTable.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (speechView.mainCanvas.bottomContainer.bounds.height + 80))
        
        xpikTable.reloadData()
        
        /*if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            
            speechView.mainCanvas.navigationBar.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: speechView.mainCanvas.navigationBar.frame.height)
            
            speechView.mainCanvas.contentContainer.frame = CGRect(x: 0, y: speechView.mainCanvas.navigationBar.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            speechView.mainCanvas.contentContainer.backgroundColor = UIColor.red
            
            //speechView.speechBackground.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
        } else {
            print("portrait")
            speechView.mainCanvas.navigationBar.frame = CGRect(x: 0, y: speechView.mainCanvas.statusBar.frame.maxY, width: UIScreen.main.bounds.width, height: speechView.mainCanvas.navigationBar.frame.height)
        }*/
    }

    
    /**
     * Called after the controller'€™s view is loaded into memory
     */
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        INPreferences.requestSiriAuthorization { status in
            if status == .authorized {
                print("Hey, Siri!")
            } else {
                print("Nay, Siri!")
                
                //Alert to tell the user that Siri is disable
                
                let alertController = UIAlertController(title: "Siri is disabled", message: "If you don't have Siri activated go to Settings/Siri & Search", preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
                   
            }
        }
        
        //Setting disableSpeechButton to false
        disableSpeechButton = false
        
        /**
         * Language button
         */
        
        //Initializing LanguagesTableViewController instance
        languageTableController = LanguagesTableViewController()
        
        //Creating languageButton
        let languageButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        //Centering languageButton into rightButtonContainer
        languageButton.center = CGPoint(x: speechView.mainCanvas.rightButtonContainer.bounds.width/2, y: speechView.mainCanvas.rightButtonContainer.bounds.height - 20)
        //Setting icon to languageButton
        languageButton.setImage(UIImage(named: changeSpeechLanguageImg(languageCode: Shared.shared.currentSpeechLanguageCode)), for: .normal)
        //Adding tint color to languageButton
        languageButton.tintColor = UIColor.white
        //Adding languageButton to rightButtonContainer
        speechView.mainCanvas.rightButtonContainer.addSubview(languageButton)
        //Getting languageButton from HomePageView and add target
        languageButton.addTarget(self, action: #selector(setSpeechLanguage), for: .touchUpInside)
        
        /**
         * Hide Nodes Button
         */
        
        //Centering hideNodesButton into rightLeftButtonContainer
        hideNodesButton.center = CGPoint(x: speechView.mainCanvas.rightLeftButtonContainer.bounds.width/2, y: speechView.mainCanvas.rightLeftButtonContainer.bounds.height - 20)
        //Adding menu icon to hideNodesButton
        hideNodesButton.setImage(UIImage(named: "ic_hide"), for: .normal)
        //Adding tint color to hideNodesButton
        hideNodesButton.tintColor = UIColor.white
        //Setting alpha to hideNodesButton
        hideNodesButton.alpha = 0.5
        //Adding hideNodesButton to rightLeftButtonContainer
        speechView.mainCanvas.rightLeftButtonContainer.addSubview(hideNodesButton)
        //Getting hideNodesButton from HomePageView and add target
        hideNodesButton.addTarget(self, action: #selector(hideNodes), for: .touchUpInside)
        
        //To get shake gesture
        self.becomeFirstResponder()

        
        //Setting carousel pages
        if let userCarouselPages = UserDefaults.standard.string(forKey: "carouselPages"){
            
            carouselPages = userCarouselPages
            
        }else{
            
            carouselPages = "7"
        }
        
        speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: Shared.shared.currentSpeechLanguageCode))
        
        //Initializing search view
        searchView = ModalView(frame: CGRect.zero)
        
        //Setting text to titleLabel to searchView
        searchView.titleLabel.text = NSLocalizedString("Image_Search", comment: "")
        
        //Placing the searchText text
        searchView.searchText.placeholder = NSLocalizedString("Search", comment: "")
        
        //Initializing color modal
        colorModal = ColorModal(frame: CGRect.zero)
        
        //Initializing upload view
        uploadView = UploadView(frame: CGRect.zero)
        
        //Setup emoji keyboard
        setupEmojiKeyboard()
        
        //Initializing microphone view
        microphoneView = MicrophoneModal(frame: CGRect.zero)
        
        //Creating keyboard notification instance
        keyboardNotification = KeyboardNotification(searchView.searchContainer)
        
        //Keyboard show observer
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        //Keyboard hide observer
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Setting width and height to tableView
        self.xpikTable.frame = CGRect(x: 0, y: 0, width: speechView.mainCanvas.contentContainer.bounds.width, height: speechView.mainCanvas.contentContainer.bounds.height)
        
        //Setting data source to xpikTable
        self.xpikTable.dataSource = self
        
        //Delegating the xpikTable
        self.xpikTable.delegate = self
        
        //Adding tableView into contentContainer
        speechView.mainCanvas.contentContainer.addSubview(self.xpikTable)
        
        //Getting categoriesButton from HomePageView and add target
        speechView.categoriesButton.addTarget(self, action: #selector(goCategories), for: .touchUpInside)
        
        //Getting keyboardButton from HomePageView and add target
        speechView.keyboardButton.addTarget(self, action: #selector(openModal), for: .touchUpInside)
       
        //Getting cameraButton from HomePageView and add target
        speechView.cameraButton.addTarget(self, action: #selector(uploadImage), for: .touchUpInside)
        
        //Getting shareButton from HomePageView and add target
        speechView.shareButton.addTarget(self, action: #selector(goPreview), for: .touchUpInside)
       
        //Getting acceptButton from SearchView and add target
        searchView.acceptButton.addTarget(self, action: #selector(searchWord), for: .touchUpInside)
        
        //Getting cancelButton from SearchView and add target
        searchView.cancelButton.addTarget(self, action: #selector(closeModal), for: .touchUpInside)
        
        //Creating cameraTapGesture and add target
        let cameraTapGesture = UITapGestureRecognizer(target: self, action: #selector(uploadFromCamera))
        
        //Setting to color modal isUserInteractionEnabled true
        uploadView.cameraContainer.isUserInteractionEnabled = true
        
        //Adding to uploadView gesture
        uploadView.cameraContainer.addGestureRecognizer(cameraTapGesture)
        
        //Creating galleryTapGesture and add target
        let galleryTapGesture = UITapGestureRecognizer(target: self, action: #selector(uploadFromGallery))
        
        //Setting to color modal isUserInteractionEnabled true
        uploadView.galleryContainer.isUserInteractionEnabled = true
        
        //Adding to uploadView gesture
        uploadView.galleryContainer.addGestureRecognizer(galleryTapGesture)
        
        //Creating colorTapGesture and add target
        let colorTapGesture = UITapGestureRecognizer(target: self, action: #selector(colorBackground))
        
        //Setting to color modal isUserInteractionEnabled true
        uploadView.colorContainer.isUserInteractionEnabled = true
        
        //Adding to uploadView gesture
        uploadView.colorContainer.addGestureRecognizer(colorTapGesture)
        
        //Creating emojiTapGesture and add target
        let emojiTapGesture = UITapGestureRecognizer(target: self, action: #selector(openKeyboard))
        
        //Setting to color modal isUserInteractionEnabled true
        uploadView.emojiContainer.isUserInteractionEnabled = true
        
        //Adding to uploadView gesture
        uploadView.emojiContainer.addGestureRecognizer(emojiTapGesture)
        
        //Creating colorBackTapGesture and add target
        let colorBackTapGesture = UITapGestureRecognizer(target: self, action: #selector(closeColorModal))
        
        //Setting to color modal isUserInteractionEnabled true
        colorModal.mainView.isUserInteractionEnabled = true
        
        //Adding to uploadView gesture
        colorModal.mainView.addGestureRecognizer(colorBackTapGesture)
        
        //Creating uploadImageTapGesture and add target
        let uploadImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(closeUploadImage))
        
        //Setting to uploadView isUserInteractionEnabled true
        uploadView.mainView.isUserInteractionEnabled = true
        
        //Adding to uploadView gesture
        uploadView.mainView.addGestureRecognizer(uploadImageTapGesture)
        
        //Speech button: Speech recognizer method
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(speechMethod))
        
        speechView.microphoneButton.addGestureRecognizer(longGesture)
        
        //Initializing delete favorite view
        deleteFavoriteView = DeleteFavoriteView(frame: CGRect.zero)
        
        //Getting acceptButton from delete favorite view and add target
        deleteFavoriteView.acceptButton.addTarget(self, action: #selector(deleteXpik), for: .touchUpInside)
        
        //Getting cancelButton from delete favorite view and add target
        deleteFavoriteView.cancelButton.addTarget(self, action: #selector(cancelXpik), for: .touchUpInside)
        
        //Adding home page view to principal view
        self.view = speechView.mainCanvas.mainView
        
    }
    
    /**
     * Add spinner view method: Add the spinner view
     */
    
    func addSpinnerView(){
        
        //Creating a spinner view with all the elements involved
        spinnerView = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        //Setting the background color to the spinner view
        spinnerView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(spinnerView)
        
        let indicator = MaterialActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        indicator.color = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        indicator.center = CGPoint(x: spinnerView.frame.width/2, y: spinnerView.frame.height/2)
        spinnerView.addSubview(indicator)
        indicator.startAnimating()
    }
    
    /**
     * Speech Method: Speech recognition
     */
    
    func speechMethod(sender: UITapGestureRecognizer) {
        
        if(Shared.shared.currentCarousel.count == 7){
            
            //Alert to tell the user that the list is full
            
            let alertController = UIAlertController(title: "", message: NSLocalizedString("List_full", comment: ""), preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            
            if presentedViewController == nil {
                
                self.present(alertController, animated: true, completion: nil)
                
            } else{
                
                self.dismiss(animated: false) { () -> Void in
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                
            }
            
        }else{
            
            //Event touch down
            
            if sender.state == .began {
                
                self.view.addSubview(microphoneView.mainView)
                
                self.speechView.microphoneButton.setImage(#imageLiteral(resourceName: "speech_button_2"), for: .normal)
                
                self.speechView.mainCanvas.bottomContainer.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
                
                if self.disableSpeechButton == false {
                    
                    checkPermission()
                    
                    playSound(fileString: "first")
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        
                        self.audioPlayer.stop()
                        
                        self.handleRecording()
                        
                    }
                    
                }
            }else if sender.state == .ended {
                
                self.speechView.microphoneButton.setImage(#imageLiteral(resourceName: "speech_button"), for: .normal)
                
                self.speechView.mainCanvas.bottomContainer.backgroundColor = UIColor.white
                
                self.disableSpeechButton = true
                
                self.microphoneView.mainView.removeFromSuperview()
                
                addSpinnerView()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // change 2 to desired number of seconds
                
                self.playSound(fileString: "second")
                
                self.speechRecognitionComplete(query: self.query)
                
                self.clearAudioEngine()
                
                self.disableSpeechButton = false
                
                }
                
            }
            
        }
    }
  
    /**
     * Play sound Method: Play sound before/after speech
     */
    
    func playSound(fileString:String){
        
        let audioFilePath = Bundle.main.path(forResource: fileString, ofType: "mp3")
        
        if audioFilePath != nil {
            
            let audioFileUrl = NSURL.fileURL(withPath: audioFilePath!)
            
            do{
                
                try audioPlayer = AVAudioPlayer(contentsOf: audioFileUrl)
                
                audioPlayer.play()
                
            }catch{
                
                print(error)
                
            }
            
        }
        
    }
    
    /**
     * Clear audio engine method: End audio request and recognition task
     */
    
    func clearAudioEngine(){
        
        self.request?.endAudio()
        
        self.audioEngine?.stop()
        
        if let node = self.audioEngine?.inputNode {
            
            node.removeTap(onBus: 0)
            
        }
        
        if recognitionTask != nil {
         
            recognitionTask?.cancel()
            
            recognitionTask = nil
            
        }
        
    }
    
    /**
     * Handle recording method: Speech method to record audio
     */
    
    func handleRecording(){
        
        self.query = ""
        
        request = SFSpeechAudioBufferRecognitionRequest()
        
        guard let request = request else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }
        
        guard let node = audioEngine?.inputNode else {return}
        
        let recordingFormat = node.outputFormat(forBus: 0)
        
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            
            self.request?.append(buffer)
            
        }
        
        audioEngine?.prepare()
        
        do {
            
            try audioEngine?.start()
            
        } catch {
            
            return print(error)
 
        }
        
        guard let myRecognizer = SFSpeechRecognizer() else {
            return
        }
        
        if !myRecognizer.isAvailable{
            
            return
            
        }
        
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { (result, error) in
            
            if result != nil {
                
                if let result = result{
                    
                    let textMatch = result.bestTranscription.formattedString
                    
                    self.query = textMatch
                    
                }
            }else if let error = error{
                
                print(error)
                
            }
        })
    }
    
    /**
     * Check permission method: Checking speech recognizer permission
     */
    
    func checkPermission(){
        speechView.microphoneButton.isEnabled = false
        speechRecognizer?.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { (authStatus) in
            var isButtonEnabled = false
            switch authStatus {
            case .authorized:
                isButtonEnabled = true
            case .denied:
                isButtonEnabled = false
                print("User denied access to speech recognition")
            case .restricted:
                isButtonEnabled = false
                print("Speech recognition restricted on this device")
            case .notDetermined:
                isButtonEnabled = false
                print("Speech recognition not yet authorized")
            }
            
            OperationQueue.main.addOperation() {
                self.speechView.microphoneButton.isEnabled = isButtonEnabled
            }
        }
    }
    
    /**
     * Speech recognition complete: Search image when stop listening
     */
    
    func speechRecognitionComplete(query: String?) {
        
        // Your code with delay
        if let query = query {
            self.search(word: query)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    /**
     * Search word method: Request image to bing typing the word
     */
    
    func searchWord() {
        
        if validate(textView: searchView.searchText) {
            
            addSpinnerView()
            
            //Calling search word method
            search(word: searchView.searchText.text)
            //Close modal
            closeModal()
        }
        
    }
    
    /**
     * Search method: Request image to bing
     */
    
    func search(word: String?) {
        
        //Setting speech language
        var currentSpeechLanguageCode: String!
        
        if let usercurrentSpeechLanguageCode = UserDefaults.standard.string(forKey: "currentSpeechLanguageCode"){
            
            currentSpeechLanguageCode = usercurrentSpeechLanguageCode
            
        }else{
            
            currentSpeechLanguageCode = Locale.current.languageCode!
            
        }
        
        if word != "" {
            
            let dateFormatter : DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = Date()
            let dateString = dateFormatter.string(from: date)
        
            Analytics.logEvent("search_word_with_user", parameters: [
                "word_name": word! as NSObject,
                "word_language": currentSpeechLanguageCode as NSObject,
                "time": dateString as NSObject
                ])
            
            guard let word = word?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
                return
            }
           
            let headers = [
                "Ocp-Apim-Subscription-Key": "b64bc21648bb4a3bb8a3dd61543d2831"
            ]
            
            
            Alamofire.request("https://api.cognitive.microsoft.com/bing/v5.0/images/search?q="+word+"&count="+carouselPages+"&safeSearch=STRICT",method: .get, encoding: JSONEncoding.default, headers : headers) .responseJSON
                { response in
                    
                    switch response.result {
                    case .success(let JSON):
                        
                        let valueObj = JSON as! NSDictionary
                        
                        //example if there is an id
                        let value = valueObj.object(forKey: "value")!
                        
                        let data = value as! NSArray
                        
                        if data != []{
                        
                            var links: [String] = []
                            
                            var thumbnails: [String] = []
                            
                            var webpages: [String] = []
                            
                            for i in 0..<(value as AnyObject).count{
                                
                                let val = data[i] as! NSDictionary
                                
                                let link = val.object(forKey: "contentUrl")!
                                
                                let thumbnail = val.object(forKey: "thumbnailUrl")
                                
                                let webpage = val.object(forKey: "hostPageUrl")
                                
                                links.append(link as! String)
                                
                                thumbnails.append(thumbnail as! String)
                                
                                webpages.append(webpage as! String)
                                
                            }
                            
                            self.spinnerView.removeFromSuperview()
                            
                            Shared.shared.currentCarousel.append(Carousel(word, webpages, links, links, thumbnails, 0, 2, 2))
                            
                            self.xpikTable.reloadData()
                            
                            //Scroll table view to bottom
                            self.scrollToBottom()
                            
                            
                        }else{
                            
                            //Appending the url into link array
                            let link: [String] = [Bundle.main.url(forResource: "linkroto", withExtension: "png")!.absoluteString]
                            
                            //Appending the webpage (none)
                            let webpage: [String] = ["none"]
                            
                            self.spinnerView.removeFromSuperview()
                            
                            //Creating and appending a new carousel
                            Shared.shared.currentCarousel.append(Carousel("no-data", webpage, link, link, link, 0, 2, 2))
                            
                            self.xpikTable.reloadData()
                            
                            //Scroll table view to bottom
                            self.scrollToBottom()
                        }
                        
                    case .failure(let error):
                        print("Request failed with error: \(error)")
                    }
                    
            }
        }else{
            
            self.spinnerView.removeFromSuperview()
        }
    }
    
    /**
     * Upload from camera: Take image with camera
     */
    
    func uploadFromCamera(){
        
        //Hide upload modal
        uploadView.mainView.removeFromSuperview()
        
        if(Shared.shared.currentCarousel.count == 7){
            
            //Alert to tell the user that the list is full
            
            let alertController = UIAlertController(title: "", message: NSLocalizedString("List_full", comment: ""), preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            if presentedViewController == nil {
                self.present(alertController, animated: true, completion: nil)
                
            } else{
                
                self.dismiss(animated: false) { () -> Void in
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
        }else{
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                
                //Creating the UIImagePickerController for the camera
                let camImagePicker = UIImagePickerController()
                
                //Delegating the camImagePicker
                camImagePicker.delegate = self
                
                //Setting the source type (camera) of camImagePicker
                camImagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                
                //Setting false the editing mode
                camImagePicker.allowsEditing = false
                
                //Initializing the camImagePicker UIImagePickerController
                self.present(camImagePicker, animated: true, completion: nil)
            }
        }
    }
    
    /**
     * Image Picker Controller method: Take picture from camera
     */
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if(picker.sourceType == UIImagePickerControllerSourceType.camera){
            
            //Getting actual image
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            //Saving the image into the gallery
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(cameraImageSavedAsynchronously(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    /**
     * Camera Image Saved Asynchronously method: Get image taked with the camera
     */
    
    func cameraImageSavedAsynchronously(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if error != nil {
            
            //We got back an error!
            print("error")
            
        } else {
            
            //Fetch the image, if the image is saved
            self.fetchLastImage()
        }
    }
    
    /**
     * Fetch last image method: Fetch the last image (taked with camera) path from gallery
     */
    
    func fetchLastImage() {
        
        //Creating SelectedAssets array to append the path of images
        SelectedAssets = [PHAsset]()
        
        //Creating the fetch options
        let fetchOptions = PHFetchOptions()
        //Setting the fetch options to creating date, to get the last image from gallery
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        //Setting fetch limit, to get only the last image from gallery
        fetchOptions.fetchLimit = 1
        
        //Getting the fetch result
        let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        
        if (fetchResult.firstObject != nil)
        {
            //Getting the path of image from gallery
            let lastImageAsset: PHAsset = fetchResult.firstObject!
            
            //Appending the PHAsset to SelectedAssets array
            self.SelectedAssets.append(lastImageAsset)
            
            //Converting the image to upload
            self.convertAssetToImages()
            
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    /**
     * Upload from gallery: Select image from gallery
     */
    
    func uploadFromGallery() {
        
        //Hide upload modal
        uploadView.mainView.removeFromSuperview()
        
        if(Shared.shared.currentCarousel.count == 7){
            
            //Alert to tell the user that the list is full
            
            let alertController = UIAlertController(title: "", message: NSLocalizedString("List_full", comment: ""), preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            if presentedViewController == nil {
                self.present(alertController, animated: true, completion: nil)
                
            } else{
                
                self.dismiss(animated: false) { () -> Void in
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
        }else{
            
            //Creating SelectedAssets array to append the path of images
            SelectedAssets = [PHAsset]()
            
            //Create BSImagePickerViewController an instance
            let vc = BSImagePickerViewController()
            
            //Setting max number of selections
            vc.maxNumberOfSelections = 7 - Shared.shared.currentCarousel.count
            
            /*let iconCamera = resizeImage(image: UIImage(named: "ic_camera")!, newWidth: 60)
             
             vc.takePhotoIcon = iconCamera
             
             vc.albumButton.tintColor = UIColor.green
             vc.cancelButton.tintColor = UIColor.red
             vc.doneButton.tintColor = UIColor.purple
             vc.selectionCharacter = "✓"
             vc.selectionFillColor = UIColor.gray
             vc.selectionStrokeColor = UIColor.yellow
             vc.selectionShadowColor = UIColor.red
             vc.backgroundColor = UIColor.orange
             
             vc.takePhotos = true*/
            
            //Display picture gallery
            self.bs_presentImagePickerController(vc, animated: true,
                                                 select: { (asset: PHAsset) -> Void in
            }, deselect: { (asset: PHAsset) -> Void in
                // User deselected an assets.
                
            }, cancel: { (assets: [PHAsset]) -> Void in
                // User cancelled. And this where the assets currently selected.
            }, finish: { (assets: [PHAsset]) -> Void in
                // User finished with these assets
                for i in 0..<assets.count
                {
                    
                    self.SelectedAssets.append(assets[i])
                    
                }
                
                //Converting the image to upload
                self.convertAssetToImages()
                
            }, completion: nil)
        }
    }
    
    /**
     * Convert asset to Images method: Compress the image selected and upload to firebase
     */
    
    func convertAssetToImages() -> Void {
        
        if SelectedAssets.count != 0{
            
            //Getting the PHAsset from SelectedAssets array
            for i in 0..<SelectedAssets.count{
                
                //Setting the PHAsset manager
                let manager = PHImageManager.default()
                
                //Setting the PHAsset options
                let option = PHImageRequestOptions()
                
                //Creating the thumbnail UIImage
                var thumbnail = UIImage()
                
                //Setting the option synchronous
                option.isSynchronous = true
                
                //Requesting the image from path
                manager.requestImage(for: SelectedAssets[i], targetSize: CGSize(width: 200, height: 200), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                    thumbnail = result!
                })
                
                //Compressing the image to be upload
                let data = UIImageJPEGRepresentation(thumbnail, 1)
                
                //Getting the compressed image
                let newImage = UIImage(data: data!)
                
                //Setting the firebase bucket route
                let route = "images/"+"IMG_"+String(Date().millisecondsSince1970)+".jpg"
                
                //Getting the firebase bucket reference
                let imageRef = Storage.storage().reference().child(route)
                
                //Upload image to firebase method
                FirebaseService.uploadImage(newImage!, at: imageRef) { (downloadURL) in
                    
                    //Getting the download url of the firebase bucket
                    guard let downloadURL = downloadURL else {
                        return
                    }
                    
                    //Getting the url in string format
                    let urlString = downloadURL.absoluteString
                    
                    //Appending the url into link array
                    let link: [String] = [urlString]
                    
                    //Appending the webpage (none)
                    let webpage: [String] = ["none"]
                    
                    //Creating and appending a new carousel
                    Shared.shared.currentCarousel.append(Carousel("selfphoto", webpage, link, link, link, 0, 2, 2))
                    
                    //Updating the xpikTable
                    self.xpikTable.reloadData()
                    
                    //Scrolling to bottom
                    self.scrollToBottom()
                    
                }
            }
        }
    }
    
    /**
     * Upload image method: Select and upload image from camera/gallery
     */
    
    func uploadImage() {
        
        self.view.addSubview(uploadView.mainView)
        
    }
    
    /**
     * Setup emoji keyboard: setup the emoji keyboard with size
     */
    
    func setupEmojiKeyboard() {
        
        for i in 1...65 {
            let icon = "emoji_" + String(i)
            emojis.append(EmojisDetails(uri: Bundle.main.url(forResource: icon, withExtension: "jpg")!.absoluteString, icon: icon + ".jpg"))
        }
        
        keyboardView = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        //Creating closeEmojiGesture and add target
        let closeEmojiGesture = UITapGestureRecognizer(target: self, action: #selector(openKeyboard))
        //Setting to keyboardView isUserInteractionEnabled true
        keyboardView.isUserInteractionEnabled = true
        //Adding to keyboardView gesture
        keyboardView.addGestureRecognizer(closeEmojiGesture)
        
        emojiKeyboard = KeyboardView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 200, width: UIScreen.main.bounds.width, height: 200))
        emojiKeyboard.borderWidth = 1.0
        emojiKeyboard.borderColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        
        emojiKeyboard.emojis = emojis
        emojiKeyboard.keyboardProtocol = self
    }
    
    /**
     * Open keyboard: Open emoji keyboard
     */
    
    func openKeyboard() {
        
        closeUploadImage()
        
        if(Shared.shared.currentCarousel.count == 7){
            
            //Alert to tell the user that the list is full
            
            let alertController = UIAlertController(title: "", message: NSLocalizedString("List_full", comment: ""), preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            if presentedViewController == nil {
                self.present(alertController, animated: true, completion: nil)
                
            } else{
                
                self.dismiss(animated: false) { () -> Void in
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
        }else{
            
            self.view.addSubview(keyboardView)
            self.view.addSubview(emojiKeyboard)
            
            if(self.keyboardView.frame.origin.y != UIScreen.main.bounds.height){
                
                UIView.animate(withDuration: 0.35, delay: 0, options: [.curveEaseIn],
                               animations: {
                                self.keyboardView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width:  self.keyboardView.bounds.width, height:  self.keyboardView.bounds.height)
                                self.emojiKeyboard.frame = CGRect(x: 0, y: UIScreen.main.bounds.height, width:  self.emojiKeyboard.bounds.width, height:  self.emojiKeyboard.bounds.height)
                }, completion: nil)
                
            }else{
                
                UIView.animate(withDuration: 0.35, delay: 0, options: [.curveEaseIn],
                               animations: {
                                self.keyboardView.frame = CGRect(x: 0, y: 0, width:  self.keyboardView.bounds.width, height:  self.keyboardView.bounds.height)
                                self.emojiKeyboard.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 200, width:  self.emojiKeyboard.bounds.width, height:  self.emojiKeyboard.bounds.height)
                }, completion: nil)
            }
        }
    }
    
    /**
     * Color background: Select background color
     */
    
    func colorBackground(){
        
        closeUploadImage()
        
        if(Shared.shared.currentCarousel.count == 7){
            
            //Alert to tell the user that the list is full
            
            let alertController = UIAlertController(title: "", message: NSLocalizedString("List_full", comment: ""), preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            if presentedViewController == nil {
                self.present(alertController, animated: true, completion: nil)
                
            } else{
                
                self.dismiss(animated: false) { () -> Void in
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
        }else{
            self.view.addSubview(colorModal.mainView)
        }
    }
    
    /**
     * Select node
     */
    
    func nodeSelected(currentNode: Int, nodeType: Int){
        
        nodesModal.mainView.removeFromSuperview()
        
        Shared.shared.currentCarousel[currentNode].type = nodeType
        Shared.shared.currentCarousel[currentNode].currentType = nodeType
        
        //Updating the xpikTable
        self.xpikTable.reloadData()
        
    }
    
    /**
     * Select background color (solid)
     */
    
    func selectBackground(backgroundColor: String) {
        
        //Close colorModal
        closeColorModal()
        
        //Appending the url into link array
        let link: [String] = [Bundle.main.url(forResource: backgroundColor, withExtension: "jpg")!.absoluteString]
        
        //Appending the webpage (none)
        let webpage: [String] = ["none"]
        
        //Creating and appending a new carousel
        Shared.shared.currentCarousel.append(Carousel("backgroundcolor", webpage, link, link, link, 0, 2, 2))
        
        //Updating the xpikTable
        self.xpikTable.reloadData()
        
        //Scroll table view to bottom
        self.scrollToBottom()
        
    }
    
    /**
     * Hide nodes method: Hide/show node buttons
     */
    
    func hideNodes() {
        
        Shared.shared.hiddenNodes = !Shared.shared.hiddenNodes
        
        if Shared.shared.hiddenNodes{
            
            for carousel in Shared.shared.currentCarousel.dropFirst(){
                
                carousel.type = 6
            }
        }else{
            
            for carousel in Shared.shared.currentCarousel.dropFirst(){
                
                carousel.type = carousel.currentType
            }
        }
        
        self.xpikTable.reloadData()
    }
    
    /**
     * Close color modal: Close the color background modal
     */
    
    func closeColorModal() {
        
        colorModal.mainView.removeFromSuperview()
    }
    
    /**
     * Close upload image: Close the upload image modal
     */
    
    func closeUploadImage() {
        
        uploadView.mainView.removeFromSuperview()
    }
    
    /**
     * Open modal: Type text to search image
     */
    
    func openModal() {
        
        if(Shared.shared.currentCarousel.count == 7){
            
            //Alert to tell the user that the list is full
            
            let alertController = UIAlertController(title: "", message: NSLocalizedString("List_full", comment: ""), preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            if presentedViewController == nil {
                self.present(alertController, animated: true, completion: nil)
            } else{
                self.dismiss(animated: false) { () -> Void in
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
        }else{
            
            self.view.addSubview(searchView.mainView)
            
            //Clean search text field
            searchView.searchText.text = ""
            //Open keyboard automatically
            searchView.searchText.becomeFirstResponder()
        }
    }
    
    /**
     * Close modal: Close the type text to search image modal
     */
    
    func closeModal() {
        
        searchView.mainView.removeFromSuperview()
    }
    
    /**
     * KeyboardWillShow: keyboard will show notification
     */
    
    func keyboardWillShow(notification: Notification) {
        
        keyboardNotification.keyboardFrameChangeNotification(notification: notification)
    }
    
    /**
     * KeyboardWillShow: keyboard will hide notification
     */
    
    func keyboardWillHide(notification: Notification) {
        
        keyboardNotification.keyboardFrameChangeNotification(notification: notification)
    }
    
    /**
     * Go categories
     */
    
    func goCategories() {
        self.navigationController?.pushViewController(CategoriesController(), animated: false)
    }
    
    /**
     * Go preview
     */
    
    func goPreview() {
        
        if Shared.shared.currentCarousel.isEmpty {
            
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            let alertController = UIAlertController(title: NSLocalizedString("Empty_Title", comment: ""), message: NSLocalizedString("Empty_Message", comment: ""), preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            self.navigationController?.pushViewController(PreviewController(), animated: false)
        }
        
    }
    
    /**
     * We are willing to become first responder to get shake motion
     */
    
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    /**
     * Enable detection of shake motion
     */
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        
        //Setting Enable/Disable shake
        if let enableShake = UserDefaults.standard.string(forKey: "enableShake"){
            
            if(enableShake == "false"){
                if Shared.shared.currentCarousel.isEmpty {
                    
                    //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
                    
                    let alertController = UIAlertController(title: NSLocalizedString("Empty_Title", comment: ""), message: NSLocalizedString("Empty_Message", comment: ""), preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                }else{
                    //enable
                    if motion == .motionShake {
                        
                        self.view.addSubview(deleteFavoriteView.mainView)
                    }
                }
            }
            
        }else{
            if Shared.shared.currentCarousel.isEmpty {
                
                //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
                
                let alertController = UIAlertController(title: NSLocalizedString("Empty_Title", comment: ""), message: NSLocalizedString("Empty_Message", comment: ""), preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            }else{
                //enable
                if motion == .motionShake {
                    
                    self.view.addSubview(deleteFavoriteView.mainView)
                }
            }
        }
    }
    
    /**
     * Close delete favorite modal
     */
    
    func cancelXpik() {
        
        deleteFavoriteView.mainView.removeFromSuperview()
    }
    
    /**
     * Delete favorite selected
     */
    
    func deleteXpik() {
        //Setting total images to 0
        totalImages = 0
        //Removing the element selected in the currentXpikDefault array
        Shared.shared.currentCarousel.removeAll()
        //Reloading xpik table view
        self.xpikTable.reloadData()
        //Closing delete favorite modal
        deleteFavoriteView.mainView.removeFromSuperview()
    }
    
    func changeSpeechLanguageImg(languageCode: String) -> String{
        
        switch languageCode {
        case "en":
            return "ic_england_selected"
        case "es":
            return "ic_spain_selected"
        case "it":
            return "ic_italy_selected"
        case "ru":
            return "ic_russia_selected"
        case "de":
            return "ic_germany_selected"
        case "fr":
            return "ic_france_selected"
        case "pt-PT":
            return "ic_portugal_selected"
        case "ar":
            return "ic_arabia_selected"
        case "ja":
            return "ic_japan_selected"
        case "hi":
            return "ic_india_selected"
        case "ko":
            return "ic_korea_selected"
        case "zh-Hant":
            return ""
        case "zh-Hans":
            return "ic_chiSm_selected"
        case "he":
            return "ic_hebrew_selected"
        case "tr":
            return "ic_turkey_selected"
        default:
            return "ic_england_selected"
        }
    }
    
    /**
     * Setting speech language
     */
    
    func setSpeechLanguage() {
        
        Shared.shared.languageTableType = false
        //Setting modalPresentationStyle option
        languageTableController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        //Open date picker modal
        self.navigationController?.present(languageTableController, animated: true, completion: nil)
    }
}

//Emoji keyboard protocol
extension SpeechController: KeyboardProtocol {
    
    func addImage(emojiUri: String) {
        
        //Appending the url into link array
        let link: [String] = [emojiUri]
        
        //Appending the webpage (none)
        let webpage: [String] = ["none"]
        
        //Creating and appending a new carousel
        Shared.shared.currentCarousel.append(Carousel("emoji", webpage, link, link, link, 0, 2, 2))
        
        //Updating the xpikTable
        self.xpikTable.reloadData()
        
        //Scroll table view to bottom
        self.scrollToBottom()
        
        openKeyboard()
    }
}
