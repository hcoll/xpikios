//
//  SpeechView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 3/23/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class SpeechView: UIView {
    
    /**
     * Class Attributes
     */
    
    //Main canvas variable
    var mainCanvas: MainCanvas!
    
    //Microphone button variable
    var microphoneButton: UIButton!
    
    //Keyboard container variable
    var keyboardContainer: UIView!
    
    //Categories container variable
    var categoriesContainer: UIView!
    
    //Camera container variable
    var cameraContainer: UIView!
    
    //Share container variable
    var shareContainer: UIView!
    
    //Microphone container variable
    var microphoneContainer: UIView!
    
    //Keyboard button variable
    var keyboardButton: UIButton!
    
    //Categories button variable
    var categoriesButton: UIButton!
    
    //Camera/Gallery upload button variable
    var cameraButton: UIButton!
    
    //Share button variable
    var shareButton: UIButton!
    
    //Speech Background variable
    var speechBackground: UIImageView!
    
    override init(frame: CGRect) {
        
        //Calling the default initializer for the SignInView class’s superclass
        super.init(frame: frame)
        
        //Creating a mainCanvas object
        mainCanvas = MainCanvas(frame: CGRect.zero)
        
        /**
         * Categories button
         */
        
        //Creating the categories container
        categoriesContainer = UIView(frame: CGRect(x: 0, y: 0, width: mainCanvas.bottomContainer.bounds.width/6, height: mainCanvas.bottomContainer.bounds.height))
        //Creating categories button
        categoriesButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering categoriesButton button into bottomContainer
        categoriesButton.center = CGPoint(x: categoriesContainer.bounds.width - 25, y: mainCanvas.bottomContainer.bounds.height - 25)
        //Setting icon to categoriesButton
        categoriesButton.setImage(#imageLiteral(resourceName: "ic_categories_white"), for: .normal)
        //Adding categoriesButton to categoriesContainer
        categoriesContainer.addSubview(categoriesButton)
        //Adding categoriesContainer to bottomContainer
        mainCanvas.bottomContainer.addSubview(categoriesContainer)
        
        /**
         * Keyboard button
         */
        
        //Creating the keyboard container
        keyboardContainer = UIView(frame: CGRect(x: categoriesContainer.frame.maxX, y: 0, width: mainCanvas.bottomContainer.bounds.width/6, height: mainCanvas.bottomContainer.bounds.height))
        //Creating keyboard button
        keyboardButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering keyboardButton button into keyboardContainer
        keyboardButton.center = CGPoint(x: keyboardContainer.bounds.width - 25, y: mainCanvas.bottomContainer.bounds.height - 25)
        //Setting icon to keyboardButton
        keyboardButton.setImage(#imageLiteral(resourceName: "ic_keyboard_white"), for: .normal)
        //Adding keyboardButton to keyboardContainer
        keyboardContainer.addSubview(keyboardButton)
        //Adding keyboardContainer to bottomContainer
        mainCanvas.bottomContainer.addSubview(keyboardContainer)
        
        /**
         * Microphone button
         */
        
        //Creating the microphone container
        microphoneContainer = UIView(frame: CGRect(x: keyboardContainer.frame.maxX, y: 3, width: mainCanvas.bottomContainer.bounds.width - 4*(mainCanvas.bottomContainer.bounds.width/6), height: mainCanvas.bottomContainer.bounds.height))
        //Creating microphone button
        microphoneButton = UIButton(frame: CGRect(x: 0, y: 0, width: 90, height: 90))
        //Centering microphoneButton button into microphoneContainer
        microphoneButton.center = CGPoint(x: microphoneContainer.bounds.width/2, y: microphoneContainer.bounds.height/2)
        //Setting icon to microphoneButton
        microphoneButton.setImage(#imageLiteral(resourceName: "speech_button"), for: .normal)
        //Adding microphoneButton to microphoneContainer
        microphoneContainer.addSubview(microphoneButton)
        //Adding microphoneContainer to bottomContainer
        mainCanvas.bottomContainer.addSubview(microphoneContainer)
        
        /**
         * Camera button
         */
        
        //Creating the camera container
        cameraContainer = UIView(frame: CGRect(x: microphoneContainer.frame.maxX, y: 0, width: mainCanvas.bottomContainer.bounds.width/6, height: mainCanvas.bottomContainer.bounds.height))
        //Creating font button
        cameraButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering cameraButton button into cameraContainer
        cameraButton.center = CGPoint(x: cameraContainer.bounds.width - 25, y: mainCanvas.bottomContainer.bounds.height - 25)
        //Setting icon to cameraButton
        cameraButton.setImage(#imageLiteral(resourceName: "ic_add_white"), for: .normal)
        //Adding cameraButton to cameraContainer
        cameraContainer.addSubview(cameraButton)
        //Adding cameraContainer to bottomContainer
        mainCanvas.bottomContainer.addSubview(cameraContainer)
        
        /**
         * Share button
         */
        
        //Creating the share container
        shareContainer = UIView(frame: CGRect(x: cameraContainer.frame.maxX, y: 0, width: mainCanvas.bottomContainer.bounds.width/6, height: mainCanvas.bottomContainer.bounds.height))
        //Creating share button
        shareButton = UIButton(frame: CGRect(x: 0, y: 0, width: 55, height: 55))
        //Centering shareButton button into shareContainer
        shareButton.center = CGPoint(x: shareContainer.bounds.width - 25, y: mainCanvas.bottomContainer.bounds.height - 25)
        //Setting icon to shareButton
        shareButton.setImage(#imageLiteral(resourceName: "ic_share_white"), for: .normal)
        //Adding shareButton to shareContainer
        shareContainer.addSubview(shareButton)
        //Adding shareContainer to bottomContainer
        mainCanvas.bottomContainer.addSubview(shareContainer)
        
        //Creating speechBackground
        speechBackground = UIImageView(frame: CGRect(x: 0, y: mainCanvas.navigationBar.frame.maxY, width: mainCanvas.contentContainer.frame.width, height: mainCanvas.contentContainer.frame.height))
        speechBackground.image = UIImage(named:"speech_background")
        speechBackground.contentMode = .scaleAspectFill
        mainCanvas.contentContainer.addSubview(speechBackground)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
