//
//  CarouselView.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/4/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit

class CarouselView: UITableViewCell {
    
    /**
     * Class Attributes
     */
    
    //TableViewController variable
    var carouselController: CarouselController?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder){
        fatalError("init(coder:) has not been implemented")
    }
    
    let imageSlideShow: ImageSlideshow = {
        
        let slideShow = ImageSlideshow(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 210))
        slideShow.contentScaleMode = UIViewContentMode.scaleAspectFit
        
        return slideShow
    }()
    
    let nodeButton: UIButton = {
        
        let nodeButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        
        return nodeButton
    }()
    
    let border: CALayer = {
        
        let border = CALayer()
        border.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0).cgColor
        
        return border
    }()
    
    func setup(row: Int, arrayCount: Int) {
        
        nodeButton.isHidden = row == 0 ? true : false
        
        border.isHidden = row == 0 ? true : false
    }
    
    func setupViews(){
        
        //Creating recognizer for each imageSlideShow
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        
        //Adding gesture to each imageSlideShow
        imageSlideShow.addGestureRecognizer(recognizer)
        
        //Adding imageSlideShow to cellView
        addSubview(imageSlideShow)
        
        //Cell border
        layer.addSublayer(border)
        
        //Adding nodeButton to cellView
        addSubview(nodeButton)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|",
                                                      options:NSLayoutFormatOptions(),
                                                      metrics: nil, views: ["v0": imageSlideShow]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v0]|",
                                                      options:NSLayoutFormatOptions(),
                                                      metrics: nil, views: ["v0": imageSlideShow]))
        
    }
    
    @objc func didTap() {
        
        if (Shared.shared.currentCarousel[imageSlideShow.tag].word != "no-data"){
            
            Shared.shared.editorController = EditorController()
            
            let fullscreen = Shared.shared.editorController
            
            fullscreen?.pageSelected = {(page: Int) in
                self.imageSlideShow.setCurrentPage(page, animated: false)
            }
            
            fullscreen?.initialPage = imageSlideShow.currentPage
            fullscreen?.inputs = imageSlideShow.images
            fullscreen?.currentImageSlideShow = imageSlideShow
            fullscreen?.currentPosition = imageSlideShow.tag
            fullscreen?.currentCarousel = Shared.shared.currentCarousel[imageSlideShow.tag]
            fullscreen?.xpikTable = carouselController?.xpikTable
            fullscreen?.transitioningDelegate = ZoomAnimatedTransitioningDelegate(slideshowView: imageSlideShow, slideshowController: fullscreen!)
            
            carouselController?.navigationController?.pushViewController(fullscreen!, animated: true)
            
        }
    }
}
