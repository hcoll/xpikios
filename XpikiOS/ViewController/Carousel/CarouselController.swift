//
//  PreferencesController.swift
//  XpikiOS
//
//  Created by Antonio Nardi on 1/17/18.
//  Copyright © 2018 HacheColl. All rights reserved.
//

import Foundation
import UIKit
import Reactions

@objcMembers
class CarouselController: UITableViewController, UITableViewDragDelegate, UITableViewDropDelegate {
    
    var xpikTable: UITableView!
    
    //Speech view variable
    var speechView: SpeechView!
    
    //NodesModal variable
    var nodesModal: NodesModal!
    
    //HideNodesButton variable
    var hideNodesButton: UIButton!
    
    //Total images variable (change event)
    var totalImages = 0 {
        
        didSet {
    
        if(totalImages > 1){
            hideNodesButton.alpha = 1
         }else{
            hideNodesButton.alpha = 0.5
         }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initializing speech view
        speechView = SpeechView(frame: CGRect.zero)
        
        //Initializing nodes modal view
        nodesModal = NodesModal(frame: CGRect.zero)
        
        //Creating hideNodesButton
        hideNodesButton = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        
        //Creating nodesTapGesture and add target
        let nodesTapGesture = UITapGestureRecognizer(target: self, action: #selector(closeNodesModal))
        
        //Setting to nodes modal isUserInteractionEnabled true
        nodesModal.mainView.isUserInteractionEnabled = true
        
        //Adding to nodesModal gesture
        nodesModal.mainView.addGestureRecognizer(nodesTapGesture)
        
        //Setting width and height to tableView
        xpikTable = UITableView()
        
        xpikTable.backgroundColor = UIColor.clear
        
        //Setting none the separator style
        self.xpikTable.separatorStyle = UITableViewCellSeparatorStyle.none
        
        //Hide vertical scroll line
        self.xpikTable.showsVerticalScrollIndicator = false
        
        if #available(iOS 11.0, *) {
            self.xpikTable.dragDelegate = self
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            self.xpikTable.dropDelegate = self
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 11.0, *) {
            self.xpikTable.dragInteractionEnabled = true
        } else {
            // Fallback on earlier versions
        }
        
        self.xpikTable.register(CarouselView.self, forCellReuseIdentifier: "carouselCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Shared.shared.currentCarousel.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //Creating custom cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "carouselCell", for: indexPath) as! CarouselView
        
        totalImages = Shared.shared.currentCarousel.count
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        //Delegating the custom cell
        cell.carouselController = self
        
        //Getting carousel array
        let carousel = Shared.shared.currentCarousel[indexPath.row]
        
        //Creating alamo fire source array
        var alamofireSource = [AlamofireSource]()
        
        //Getting thumbnails from each carousel
        for thumbnail in carousel.thumbnail{
            
            alamofireSource.append(AlamofireSource(urlString: thumbnail)!)
        }
        
        //Setting images to image slide show
        cell.imageSlideShow.setImageInputs(alamofireSource)
        
        //Changing image slideshow position of the current row
        cell.imageSlideShow.currentPageChanged = { page in
            carousel.position = page
        }
        
        //Setting the position to currentCarousel object
        cell.imageSlideShow.setCurrentPage(carousel.position, animated: false)
        
        //Setting imageSlideShow tag
        cell.imageSlideShow.tag = indexPath.row
        
        //Centering the imageSlideShow into the cell
        cell.imageSlideShow.center = CGPoint(x: cell.center.x, y: cell.frame.height/2 + 10)
        
        cell.border.frame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: 2.5)
        
        cell.nodeButton.tag = indexPath.row
        cell.nodeButton.center = CGPoint(x: UIScreen.main.bounds.width/2, y: 20)
        cell.nodeButton.setImage(UIImage(named: nodesModal.items[carousel.type]), for: .normal)
        cell.nodeButton.addTarget(self, action: #selector(selectNode), for: .touchUpInside)
        
        cell.setup(row: indexPath.row, arrayCount: Shared.shared.currentCarousel.count)
        
        /**
         * Copy button
         */
        
        //Creating copy button
        let copyButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        //Centering copyButton button into cell
        copyButton.center = CGPoint(x: 25, y: 50)
        //Setting icon to copyButton
        copyButton.setImage(#imageLiteral(resourceName: "ic_copy"), for: .normal)
        //Adding tag to copyButton
        copyButton.tag = indexPath.row
        //Adding copy row target to copyButton
        copyButton.addTarget(self, action: #selector(copyRow), for: .touchUpInside)
        //Adding copyButton to cell
        cell.addSubview(copyButton)
        
        /**
         * Trash button
         */
        
        //Creating trash button
        let trashButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        //Centering trashButton button into cell
        trashButton.center = CGPoint(x: cell.frame.width - 25, y: 50)
        //Setting icon to trashButton
        trashButton.setImage(#imageLiteral(resourceName: "ic_trash"), for: .normal)
        //Adding tag to trashButton
        trashButton.tag = indexPath.row
        //Adding delete row target to trashButton
        trashButton.addTarget(self, action: #selector(deleteRow), for: .touchUpInside)
        //Adding trashButton to cell
        cell.addSubview(trashButton)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 240
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        let item = Shared.shared.currentCarousel[indexPath.row]
        let itemProvider = NSItemProvider(object: item)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = item
        return [dragItem]
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
        
        var destinationIndexPath: IndexPath!
        
        if let indexPath = coordinator.destinationIndexPath
        {
            destinationIndexPath = indexPath
        }
        else
        {
            // Get last index path of collection view.
            let section = tableView.numberOfSections - 1
            let row = tableView.numberOfRows(inSection: section)
            destinationIndexPath = IndexPath(row: row, section: section)
        }
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UITableViewDropProposal
    {
        return UITableViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
    {
        let item = Shared.shared.currentCarousel[sourceIndexPath.row]
        Shared.shared.currentCarousel.remove(at: sourceIndexPath.row)
        Shared.shared.currentCarousel.insert(item, at: destinationIndexPath.row)
        
        DispatchQueue.main.async{
            //Now reload the tableView
            self.xpikTable.reloadData()
        }
    }
    
    /**
     * Select node method: Select node type
     */
    
    func selectNode(sender: UIButton) {
        
        self.view.addSubview(nodesModal.mainView)
        nodesModal.currentNode = sender.tag
    }
    
    /**
     * Close node modal method
     */
    
    func closeNodesModal(){
        nodesModal.mainView.removeFromSuperview()
    }
    
    /**
     * Delete row method: Delete current row
     */
    
    func deleteRow(sender: UIButton) {
        
        Shared.shared.currentCarousel.remove(at: sender.tag)
        let indexPath = IndexPath(item: sender.tag, section: 0)
        self.xpikTable.deleteRows(at: [indexPath], with: .fade)
        
        DispatchQueue.main.async{
            //Now reload the tableView
            self.xpikTable.reloadData()
        }
    }
    
    /**
     * Copy row method: Copy current row
     */
    
    func copyRow(sender: UIButton) {
        
        if(Shared.shared.currentCarousel.count == 7){
            
            //Alert to tell the user that the list is full
            
            let alertController = UIAlertController(title: "", message: NSLocalizedString("List_full", comment: ""), preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            if presentedViewController == nil {
                self.present(alertController, animated: true, completion: nil)
                
            } else{
                
                self.dismiss(animated: false) { () -> Void in
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            
        }else{
            
            let indexPath = IndexPath(item: sender.tag, section: 0)
            
            createCarousel(carousel: Shared.shared.currentCarousel[indexPath.row], position: indexPath.row + 1)
        }
    }
    
    /**
     * Create carousel method: add a new carousel to tableview
     */
    
    func createCarousel(carousel: Carousel, position: Int){
        
        var link: [String] = []
        
        var thumbnail: [String] = []
        
        var webpage: [String] = []
        
        for i in 0..<carousel.link.count{
            
            link.append(carousel.link[i])
            
            thumbnail.append(carousel.thumbnail[i])
            
            webpage.append(carousel.webpage[i])
        }
        
        //Inserting the new element into carousels array
        Shared.shared.currentCarousel.insert(Carousel(carousel.word, webpage, link, link, thumbnail, 0, carousel.type, carousel.currentType), at: position)
        
        DispatchQueue.main.async{
            //Now reload the tableView
            self.xpikTable.reloadData()
        }
    }
    
    /**
     * Scroll to bottom method: Scroll to the last row of the table view, after insert an element
     */
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: Shared.shared.currentCarousel.count-1, section: 0)
            self.xpikTable.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
}
