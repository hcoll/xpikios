//
//  ImageSlideshow.swift
//  ImageSlideshow
//
//  Created by Petr Zvoníček on 30.07.15.
//

//
//  ImageSlideshow.swift
//  ImageSlideshow
//
//  Created by Petr Zvoníček on 30.07.15.
//

import UIKit

/**
 Used to represent position of the Page Control
 - hidden: Page Control is hidden
 - insideScrollView: Page Control is inside image slideshow
 - underScrollView: Page Control is under image slideshow
 - custom: Custom vertical padding, relative to "insideScrollView" position
 */
public enum PageControlPosition {
    case hidden
    case insideScrollView
    case underScrollView
    case custom(padding: CGFloat)
    
    var bottomPadding: CGFloat {
        switch self {
        case .hidden, .insideScrollView:
            return 0.0
        case .underScrollView:
            return 30.0
        case .custom(let padding):
            return padding
        }
    }
}

/// Used to represent image preload strategy
///
/// - fixed: preload only fixed number of images before and after the current image
/// - all: preload all images in the slideshow
public enum ImagePreload {
    case all
}

/// Main view containing the Image Slideshow
@objcMembers
class ImageSlideshow: UIView {
    
    /// Scroll View to wrap the slideshow
    let scrollView = UIScrollView()
    
    /// Page Control shown in the slideshow
    let pageControl = UIPageControl()
    
    /// Activity indicator shown when loading image
    var activityIndicator: ActivityIndicatorFactory? {
        didSet {
            self.reloadScrollView()
        }
    }
    
    // MARK: - State properties
    
    /// Page control position
    var pageControlPosition = PageControlPosition.insideScrollView {
        didSet {
            setNeedsLayout()
        }
    }
    
    /// Current page
    fileprivate(set) var currentPage: Int = 0 {
        didSet {
            pageControl.currentPage = currentPage
            
            if oldValue != currentPage {
                currentPageChanged?(currentPage)
            }
        }
    }
    
    /// Called on each currentPage change
    var currentPageChanged: ((_ page: Int) -> ())?
    
    /// Called on scrollViewDidEndDecelerating
    var didEndDecelerating: (() -> ())?
    
    /// Currenlty displayed slideshow item
    var currentSlideshowItem: ImageSlideshowItem? {
        if slideshowItems.count > scrollViewPage {
            return slideshowItems[scrollViewPage]
        } else {
            return nil
        }
    }
    
    /// Current scroll view page. This may differ from `currentPage` as circular slider has two more dummy pages at indexes 0 and n-1 to provide fluent scrolling between first and last item.
    fileprivate(set) var scrollViewPage: Int = 0
    
    /// Input Sources loaded to slideshow
    fileprivate(set) var images = [InputSource]()
    
    /// Image Slideshow Items loaded to slideshow
    fileprivate(set) var slideshowItems = [ImageSlideshowItem]()
    
    /// Enables/disables user interactions
    var draggingEnabled = true {
        didSet {
            self.scrollView.isUserInteractionEnabled = draggingEnabled
        }
    }
    
    /// Enables/disables zoom
    var zoomEnabled = false {
        didSet {
            self.reloadScrollView()
        }
    }
    
    /// Enables/disables single tap
    var singleTapEnabled = false
    
    /// Image preload configuration, can be sed to .fixed to enable lazy load or .all
    var preload = ImagePreload.all
    
    /// Content mode of each image in the slideshow
    var contentScaleMode: UIViewContentMode = UIViewContentMode.scaleAspectFit {
        didSet {
            for view in slideshowItems {
                view.imageView.contentMode = contentScaleMode
            }
        }
    }
    
    //fileprivate var slideshowTimer: Timer?
    fileprivate var scrollViewImages = [InputSource]()
    
    /// Transitioning delegate to manage the transition to full screen controller
    fileprivate(set) var slideshowTransitioningDelegate: ZoomAnimatedTransitioningDelegate?
    
    // MARK: - Life cycle
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    fileprivate func initialize() {
        autoresizesSubviews = true
        clipsToBounds = true
        
        // scroll view configuration
        scrollView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height - 50.0)
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        scrollView.bounces = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.autoresizingMask = self.autoresizingMask
        addSubview(scrollView)
        
        layoutScrollView()
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // fixes the case when automaticallyAdjustsScrollViewInsets on parenting view controller is set to true
        scrollView.contentInset = UIEdgeInsets.zero
        
        layoutPageControl()
        layoutScrollView()
    }
    
    func layoutPageControl() {
        if case .hidden = self.pageControlPosition {
            pageControl.isHidden = true
        } else {
            pageControl.isHidden = self.images.count < 2
        }
        
        var pageControlBottomInset: CGFloat = 12.0
        if #available(iOS 11.0, *) {
            pageControlBottomInset += self.safeAreaInsets.bottom
        }
        
        pageControl.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: 10)
        pageControl.center = CGPoint(x: frame.size.width / 2, y: frame.size.height - pageControlBottomInset)
    }
    
    /// updates frame of the scroll view and its inner items
    func layoutScrollView() {
        let scrollViewBottomPadding: CGFloat = pageControlPosition.bottomPadding
        scrollView.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height - scrollViewBottomPadding)
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width * CGFloat(scrollViewImages.count), height: scrollView.frame.size.height)
        
        for (index, view) in self.slideshowItems.enumerated() {
            if !view.zoomInInitially {
                view.zoomOut()
            }
            view.frame = CGRect(x: scrollView.frame.size.width * CGFloat(index), y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
        }
        
        setCurrentPage(currentPage, animated: false)
    }
    
    /// reloads scroll view with latest slideshow items
    func reloadScrollView() {
        // remove previous slideshow items
        for view in self.slideshowItems {
            view.removeFromSuperview()
        }
        self.slideshowItems = []
        
        var i = 0
        for image in scrollViewImages {
            let item = ImageSlideshowItem(image: image, zoomEnabled: self.zoomEnabled, singleTapEnabled: self.singleTapEnabled, activityIndicator: self.activityIndicator?.create())
            item.imageView.contentMode = self.contentScaleMode
            slideshowItems.append(item)
            scrollView.addSubview(item)
            i += 1
        }
        
        loadImages(for: scrollViewPage)
    }
    
    private func loadImages(for scrollViewPage: Int) {
        let totalCount = slideshowItems.count
        
        for i in 0..<totalCount {
            let item = slideshowItems[i]
            switch self.preload {
            case .all:
                item.loadImage()
            }
        }
    }
    
    // MARK: - Image setting
    
    /**
     Set image inputs into the image slideshow
     - parameter inputs: Array of InputSource instances.
     */
    func setImageInputs(_ inputs: [InputSource]) {
        self.images = inputs
        self.pageControl.numberOfPages = inputs.count
            self.scrollViewImages = images
        
        reloadScrollView()
        layoutScrollView()
        layoutPageControl()
    }
    
    // MARK: paging methods
    
    /**
     Change the current page
     - parameter newPage: new page
     - parameter animated: true if animate the change
     */
    func setCurrentPage(_ newPage: Int, animated: Bool) {
        let pageOffset = newPage
        
        self.setScrollViewPage(pageOffset, animated: animated)
    }
    
    /**
     Change the scroll view page. This may differ from `setCurrentPage` as circular slider has two more dummy pages at indexes 0 and n-1 to provide fluent scrolling between first and last item.
     - parameter newScrollViewPage: new scroll view page
     - parameter animated: true if animate the change
     */
    func setScrollViewPage(_ newScrollViewPage: Int, animated: Bool) {
        if scrollViewPage < scrollViewImages.count {
            self.scrollView.scrollRectToVisible(CGRect(x: scrollView.frame.size.width * CGFloat(newScrollViewPage), y: 0, width: scrollView.frame.size.width, height: scrollView.frame.size.height), animated: animated)
            self.setCurrentPageForScrollViewPage(newScrollViewPage)
        }
    }
    
    @objc func slideshowTick(_ timer: Timer) {
        let page = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        let nextPage = page + 1
        
        self.setScrollViewPage(nextPage, animated: true)
    }
    
    fileprivate func setCurrentPageForScrollViewPage(_ page: Int) {
        if scrollViewPage != page {
            // current page has changed, zoom out this image
            if slideshowItems.count > scrollViewPage {
                slideshowItems[scrollViewPage].zoomOut()
            }
        }
        
        if page != scrollViewPage {
            loadImages(for: page)
        }
        scrollViewPage = page
        currentPage = page
    }
    
    @objc private func pageControlValueChanged() {
        self.setCurrentPage(pageControl.currentPage, animated: true)
    }
    
    fileprivate func setPrimaryVisiblePage() {
        let primaryVisiblePage = Int(scrollView.contentOffset.x + scrollView.frame.size.width / 2) / Int(scrollView.frame.size.width)
        setCurrentPageForScrollViewPage(primaryVisiblePage)
    }
}

extension ImageSlideshow: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        setPrimaryVisiblePage()
        didEndDecelerating?()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        setPrimaryVisiblePage()
    }
}
