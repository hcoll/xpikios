//
//  ViewController.swift
//  Photo Editor
//
//  Created by Mohamed Hamed on 4/23/17.
//  Copyright © 2017 Mohamed Hamed. All rights reserved.
//

import UIKit

public final class PhotoEditorViewController: UIViewController {
    
    //Bottom container variable
    var bottomContainer: UIView!
    
    //Done navigation bar variable
    var doneNavigationBar: UIView!
    
    //Done button variable
    var doneCButton: UIButton!
    
    /** holding the 2 imageViews original image and drawing & stickers */
    @IBOutlet weak var canvasView: UIView!
    //To hold the image
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    //To hold the drawings and stickers
    @IBOutlet weak var canvasImageView: UIImageView!
    
    @IBOutlet weak var topToolbar: UIView!
    @IBOutlet weak var bottomToolbar: UIView!
    
    @IBOutlet weak var clearAll: UILabel!
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var colorsCollectionView: UICollectionView!
    @IBOutlet weak var colorPickerView: UIView!
    @IBOutlet weak var colorPickerViewBottomConstraint: NSLayoutConstraint!
    
    //Controls
    @IBOutlet weak var cropButton: UIButton!
    @IBOutlet weak var stickerButton: UIButton!
    @IBOutlet weak var drawButton: UIButton!
    @IBOutlet weak var textButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    
    public var image: UIImage?
    /**
     Array of Stickers -UIImage- that the user will choose from
     */
    public var stickers : [UIImage] = []
    /**
     Array of Colors that will show while drawing or typing
     */
    public var colors  : [UIColor] = []
    
    public var photoEditorDelegate: PhotoEditorDelegate?
    var colorsCollectionViewDelegate: ColorsCollectionViewDelegate!
    
    // list of controls to be hidden
    public var hiddenControls : [control] = []
    
    var stickersVCIsVisible = false
    var drawColor: UIColor = UIColor.black
    var textColor: UIColor = UIColor.white
    var isDrawing: Bool = false
    var lastPoint: CGPoint!
    var swiped = false
    var lastPanPoint: CGPoint?
    var lastTextViewTransform: CGAffineTransform?
    var lastTextViewTransCenter: CGPoint?
    var lastTextViewFont:UIFont?
    var activeTextView: UITextView?
    var imageViewToPan: UIImageView?
    var isTyping: Bool = false
    
    
    var stickersViewController: StickersViewController!
    
    /**
     * Lock Orientation
     */
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        AppUtility.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
        
    }
    
    //Register Custom font before we load XIB
    public override func loadView() {
        registerFont()
        super.loadView()
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        //Hiding deafult toolbar
        hideToolbar(hide: true)
        
        //Setting image into imageView
        self.setImageView(image: image!)
        
        /**
         * Status Bar
         */
        
        //Creating status bar
        let statusBar = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 22))
        //Setting the background color to the status bar
        statusBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding statusBar to mainView
        self.view.addSubview(statusBar)
        
        /**
         * Navigation Bar
         */
        
        //Creating navigation bar
        let navigationBar = UIView(frame:CGRect(x: 0, y: statusBar.frame.maxY, width: UIScreen.main.bounds.width, height: 65))
        //Setting the background color to the navigation bar
        navigationBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding navigationBar to mainView
        self.view.addSubview(navigationBar)
        
        /**
         * Done navigation Bar
         */
        
        //Creating done navigation bar
        doneNavigationBar = UIView(frame:CGRect(x: 0, y: statusBar.frame.maxY, width: UIScreen.main.bounds.width, height: 65))
        //Setting the background color to the doneNavigationBar
        doneNavigationBar.backgroundColor = UIColor(red:1.00, green:0.40, blue:0.11, alpha:1.0)
        //Adding doneNavigationBar to mainView
        self.view.addSubview(doneNavigationBar)
        //Hiding done navigation bar
        doneNavigationBar.isHidden = true
        
        /**
         * Done button
         */
        
        //Creating done button container
        let doneButtonContainer = UIView(frame:CGRect(x: UIScreen.main.bounds.width - (UIScreen.main.bounds.width/6) - 35, y: 0, width: 80, height: doneNavigationBar.bounds.height))
        //Creating doneButton
        doneCButton = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 50))
        //Centering doneButton button into doneButtonContainer
        doneCButton.center = CGPoint(x: doneButtonContainer.bounds.width/2, y: doneButtonContainer.bounds.height/2)
        //Setting icon color
        doneCButton.setTitleColor(UIColor.white, for: .normal)
        //Setting icon size to doneButton
        doneCButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 16)
        //Setting icon to doneButton
        doneCButton.setTitle(NSLocalizedString("Done", comment: "")/*String.fontAwesomeIcon(name: .check)*/, for: .normal)
        //Adding target to doneButton
        doneCButton.addTarget(self, action: #selector(finishEditing), for: .touchUpInside)
        //Adding doneButton to doneButtonContainer
        doneButtonContainer.addSubview(doneCButton)
        //Adding doneButtonContainer to doneNavigationBar
        doneNavigationBar.addSubview(doneButtonContainer)
        
        /**
         * Cancel button
         */
        
        //Creating cancel button container
        let cancelButtonContainer = UIView(frame:CGRect(x: 0, y: 0, width: 80, height: navigationBar.bounds.height))
        //Creating cancelButton
        let cancelButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering cancelButton into cancelButtonContainer
        cancelButton.center = CGPoint(x: cancelButtonContainer.bounds.width/2, y: cancelButtonContainer.bounds.height - 20)
        //Adding cancel image
        cancelButton.setImage(UIImage(named: "ic_back"), for: .normal)
        //Adding target to cancelButton
        cancelButton.addTarget(self, action: #selector(cancelEdit), for: .touchUpInside)
        //Adding cancelButton to cancelButtonContainer
        cancelButtonContainer.addSubview(cancelButton)
        //Adding cancelButtonContainer to navigationBar
        navigationBar.addSubview(cancelButtonContainer)
        
        /**
         * Navigation bar buttons container
         */
        
        //Creating Navigation bar buttons container
        let navigationBarButtonsContainer = UIView(frame:CGRect(x: cancelButtonContainer.frame.maxX, y: 0, width: navigationBar.bounds.width - cancelButtonContainer.bounds.width, height: navigationBar.bounds.height))
        //Adding navigationBarButtonsContainer to navigationBar
        navigationBar.addSubview(navigationBarButtonsContainer)
        
        /**
         * Sticker button
         */
        
        //Creating sticker button container
        let stickerButtonContainer = UIView(frame:CGRect(x: 0, y: 0, width: 70, height: navigationBarButtonsContainer.bounds.height))
        //Creating stickerButton
        let stickerButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering stickerButton button into stickerButtonContainer
        stickerButton.center = CGPoint(x: stickerButtonContainer.bounds.width - 20, y: stickerButtonContainer.bounds.height - 20)
        //Adding sticker image
        stickerButton.setImage(UIImage(named: "ic_stickers"), for: .normal)
        //Adding target to stickerButton
        stickerButton.addTarget(self, action: #selector(openStickers), for: .touchUpInside)
        //Adding stickerButton to stickerButtonContainer
        stickerButtonContainer.addSubview(stickerButton)
        //Adding stickerButtonContainer to navigationBarButtonsContainer
        navigationBarButtonsContainer.addSubview(stickerButtonContainer)
        
        /**
         * Draw button
         */
        
        //Creating sticker button container
        let drawButtonContainer = UIView(frame:CGRect(x: stickerButtonContainer.frame.maxX, y: 0, width: 70, height: navigationBarButtonsContainer.bounds.height))
        //Creating drawButton
        let drawButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering drawButton button into drawButtonContainer
        drawButton.center = CGPoint(x: drawButtonContainer.bounds.width - 20, y: drawButtonContainer.bounds.height - 20)
        //Adding draw image
        drawButton.setImage(UIImage(named: "ic_paint"), for: .normal)
        //Adding target to drawButton
        drawButton.addTarget(self, action: #selector(draw), for: .touchUpInside)
        //Adding drawButton to drawButtonContainer
        drawButtonContainer.addSubview(drawButton)
        //Adding drawButtonContainer to navigationBarButtonsContainer
        navigationBarButtonsContainer.addSubview(drawButtonContainer)
        
        /**
         * Font button
         */
        
        //Creating font button container
        let fontButtonContainer = UIView(frame:CGRect(x: drawButtonContainer.frame.maxX, y: 0, width: 70, height: navigationBarButtonsContainer.bounds.height))
        //Creating fontButton
        let fontButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Centering fontButton button into fontButtonContainer
        fontButton.center = CGPoint(x: fontButtonContainer.bounds.width - 20, y: fontButtonContainer.bounds.height - 20)
        //Adding font image
        fontButton.setImage(UIImage(named: "ic_font_white"), for: .normal)
        //Adding target to fontButton
        fontButton.addTarget(self, action: #selector(addText), for: .touchUpInside)
        //Adding fontButton to fontButtonContainer
        fontButtonContainer.addSubview(fontButton)
        //Adding fontButtonContainer to navigationBarButtonsContainer
        navigationBarButtonsContainer.addSubview(fontButtonContainer)
        
        /**
         * Save button
         */
        
        //Creating save button container
        let saveButtonContainer = UIView(frame:CGRect(x: fontButtonContainer.frame.maxX, y: 0, width: 70, height: navigationBarButtonsContainer.bounds.height))
        //Creating saveButton
        let saveButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        //Centering saveButton button into saveButtonContainer
        saveButton.center = CGPoint(x: saveButtonContainer.bounds.width/2, y: saveButtonContainer.bounds.height/2)
        //Adding save image
        saveButton.setImage(UIImage(named: "ic_check"), for: .normal)
        //Adding target to saveButton
        saveButton.addTarget(self, action: #selector(saveImage), for: .touchUpInside)
        //Adding saveButton to saveButtonContainer
        saveButtonContainer.addSubview(saveButton)
        //Adding saveButtonContainer to navigationBarButtonsContainer
        navigationBarButtonsContainer.addSubview(saveButtonContainer)
        
        /**
         * Bottom container
         */
        
        //Creating the bottom container
        bottomContainer = UIView(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 60, width: UIScreen.main.bounds.width/4, height: 60))
        //Adding backgroudn color to bottomContainer
        bottomContainer.backgroundColor = UIColor.white
        //Adding bottom container to mainView
        self.view.addSubview(bottomContainer)
        
        /**
         * Clear button
         */
        
        //Creating the clear container
        let clearContainer = UIView(frame: CGRect(x: 5, y: 0, width: bottomContainer.bounds.width, height: bottomContainer.bounds.height/2))
        //Creating clear button
        let clearButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        //Centering clearButton button into clearContainer
        clearButton.center = CGPoint(x: clearContainer.bounds.width/2, y: 10)
        //Setting icon to clearButton
        clearButton.setImage(#imageLiteral(resourceName: "ic_clear"), for: .normal)
        //Adding target to clearButton
        clearButton.addTarget(self, action: #selector(clearFunction), for: .touchUpInside)
        //Adding clearButton to clearContainer
        clearContainer.addSubview(clearButton)
        //Adding clearContainer to bottomContainer
        bottomContainer.addSubview(clearContainer)
        
        //Creating UIlabel for clear
        let clearLabel = UILabel(frame: CGRect(x: 15, y: clearContainer.frame.maxY, width: bottomContainer.bounds.width, height: bottomContainer.bounds.height/2))
        //Setting text to clearLabel
        clearLabel.text = "Clear All"
        //Adding clearLabel into bottomContainer
        bottomContainer.addSubview(clearLabel)
        
        deleteView.layer.cornerRadius = deleteView.bounds.height / 2
        deleteView.layer.borderWidth = 2.0
        deleteView.layer.borderColor = UIColor.black.cgColor
        deleteView.clipsToBounds = true
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped(_:)))
        edgePan.edges = .bottom
        edgePan.delegate = self
        self.view.addGestureRecognizer(edgePan)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)),
                                               name: .UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)),
                                               name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillChangeFrame(_:)),
                                               name: .UIKeyboardWillChangeFrame, object: nil)
        
        
        configureCollectionView()
        stickersViewController = StickersViewController(nibName: "StickersViewController", bundle: Bundle(for: StickersViewController.self))
        hideControls()
        
        self.view.backgroundColor = UIColor.white
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    func configureCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 30, height: 30)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        colorsCollectionView.collectionViewLayout = layout
        colorsCollectionViewDelegate = ColorsCollectionViewDelegate()
        colorsCollectionViewDelegate.colorDelegate = self
        if !colors.isEmpty {
            colorsCollectionViewDelegate.colors = colors
        }
        colorsCollectionView.delegate = colorsCollectionViewDelegate
        colorsCollectionView.dataSource = colorsCollectionViewDelegate
        
        colorsCollectionView.register(
            UINib(nibName: "ColorCollectionViewCell", bundle: Bundle(for: ColorCollectionViewCell.self)),
            forCellWithReuseIdentifier: "ColorCollectionViewCell")
    }
    
    func setImageView(image: UIImage) {
        
        imageView.image = image
        let size = image.suitableSize(widthLimit: UIScreen.main.bounds.width)
        imageViewHeightConstraint.constant = (size?.height)!
        
    }
    
    func hideToolbar(hide: Bool) {
        topToolbar.isHidden = hide
        bottomToolbar.isHidden = hide
    }
    
    @objc func cancelEdit() {
        photoEditorDelegate?.canceledEditing()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func openStickers() {
        addStickersViewController()
    }
    
    @objc func draw() {
        
        doneNavigationBar.isHidden = false
        bottomContainer.isHidden = true
        isDrawing = true
        colorPickerView.isHidden = false
    }
    
    @objc func finishEditing() {
        view.endEditing(true)
        doneNavigationBar.isHidden = true
        bottomContainer.isHidden = false
        isDrawing = false
        colorPickerView.isHidden = true
    }
    
    @objc func saveImage() {
        let img = self.canvasView.toImage()
        photoEditorDelegate?.doneEditing(image: img)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func clearFunction() {
        //clear drawing
        canvasImageView.image = nil
        //clear stickers and textviews
        for subview in canvasImageView.subviews {
            subview.removeFromSuperview()
        }
    }
    
    @objc func addText() {
        
        doneNavigationBar.isHidden = false
        bottomContainer.isHidden = true
        isTyping = true
        let textView = UITextView(frame: CGRect(x: 0, y: canvasImageView.center.y,
                                                width: UIScreen.main.bounds.width, height: 30))
        
        textView.textAlignment = .center
        textView.font = UIFont(name: "Helvetica", size: 30)
        textView.textColor = textColor
        textView.layer.shadowColor = UIColor.black.cgColor
        textView.layer.shadowOffset = CGSize(width: 1.0, height: 0.0)
        textView.layer.shadowOpacity = 0.2
        textView.layer.shadowRadius = 1.0
        textView.layer.backgroundColor = UIColor.clear.cgColor
        textView.autocorrectionType = .no
        textView.isScrollEnabled = false
        textView.delegate = self
        self.canvasImageView.addSubview(textView)
        addGestures(view: textView)
        textView.becomeFirstResponder()
    }
}

extension PhotoEditorViewController: ColorDelegate {
    func didSelectColor(color: UIColor) {
        if isDrawing {
            self.drawColor = color
        } else if activeTextView != nil {
            activeTextView?.textColor = color
            textColor = color
        }
    }
}










