//
//  CropViewController.swift
//  CropViewController
//
//  Created by Guilherme Moura on 2/25/16.
//  Copyright © 2016 Reefactor, Inc. All rights reserved.
// Credit https://github.com/sprint84/PhotoCropEditor

import UIKit

public protocol CropViewControllerDelegate: class {
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect)
    func cropViewControllerDidCancel(_ controller: CropViewController)
}

open class CropViewController: UIViewController {
    open weak var delegate: CropViewControllerDelegate?
    
    var flipCover: UIView!
    
    var flipH = 0
    var flipV = 0
    
    var flipX = 1
    var flipY = 1
    
    open var image: UIImage? {
        didSet {
            cropView?.image = image
        }
    }
    open var keepAspectRatio = false {
        didSet {
            cropView?.keepAspectRatio = keepAspectRatio
        }
    }
    open var cropAspectRatio: CGFloat = 0.0 {
        didSet {
            cropView?.cropAspectRatio = cropAspectRatio
        }
    }
    open var cropRect = CGRect.zero {
        didSet {
            adjustCropRect()
        }
    }
    open var imageCropRect = CGRect.zero {
        didSet {
            cropView?.imageCropRect = imageCropRect
        }
    }
    open var toolbarHidden = false
    open var rotationEnabled = false {
        didSet {
            cropView?.rotationGestureRecognizer.isEnabled = rotationEnabled
        }
    }
    open var rotationTransform: CGAffineTransform {
        return cropView!.rotation
    }
    open var zoomedCropRect: CGRect {
        return cropView!.zoomedCropRect()
    }

    fileprivate var cropView: CropView?
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        initialize()
    }
    
    fileprivate func initialize() {
        rotationEnabled = true
    }
    
    open override func loadView() {
        let contentView = UIView()
        contentView.autoresizingMask = .flexibleWidth
        contentView.backgroundColor = UIColor.black
        view = contentView
        
        // Add CropView
        cropView = CropView(frame: contentView.bounds)
        contentView.addSubview(cropView!)
        
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        /*//Getting status bar
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        Setting status bar background color
        statusBar.backgroundColor = UIColor.black*/
        
        //Setting navigation bar background color
        navigationController?.navigationBar.barTintColor = UIColor.black
        
        /**
         * Back button
         */
        
        //Creating backButtonContainer
        let backButtonContainer = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Creating backButton
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        //Adding back image
        backButton.setImage(UIImage(named: "ic_back"), for: .normal)
        //Adding target to backButton
        backButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
        //Adding backButton to backButtonContainer
        backButtonContainer.addSubview(backButton)
        
        //Creating left bar button item and adding the backButtonContainer
        let leftBarButtonItem = UIBarButtonItem(customView: backButtonContainer)
        //Adding the leftBarButtonItem to the navigation bar
        navigationItem.leftBarButtonItem = leftBarButtonItem
        
        /**
         * Rotate button
         */
        
        //Creating rotate button
        let rotateButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        rotateButton.setImage(UIImage(named: "ic_rotate"), for: .normal)
        //Enabling user interaction
        rotateButton.isUserInteractionEnabled = true
        //Adding target to rotateButton
        rotateButton.addTarget(self, action: #selector(rotateImage), for: .touchUpInside)
        
        //Creating rotate button bar item and adding the rotate button
        let rotateBarButton = UIBarButtonItem(customView: rotateButton)
        
        /**
         * Flip flop button
         */
        
        //Creating flip button
        let flipButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        flipButton.setImage(UIImage(named: "ic_flip"), for: .normal)
        //Enabling user interaction
        flipButton.isUserInteractionEnabled = true
        //Adding target to flipButton
        flipButton.addTarget(self, action: #selector(flipFlop), for: .touchUpInside)
        
        //Creating rotate button bar item and adding the flip flop button
        let flipBarButton = UIBarButtonItem(customView: flipButton)
        
        /**
         * Crop button
         */
        
        //Creating crop button
        let cropButton = UIButton()
        //Setting crop button title
        cropButton.setTitle(NSLocalizedString("Crop", comment: ""), for: .normal)
        //Enabling user interaction
        cropButton.isUserInteractionEnabled = true
        //Adding target to cropButton
        cropButton.addTarget(self, action: #selector(done), for: .touchUpInside)
        
        //Creating right bar button item and adding the crop button
        let cropBarButton = UIBarButtonItem(customView: cropButton)
        
        //Adding the rightBarButtonItem to the navigation bar
        navigationItem.setRightBarButtonItems([cropBarButton,flipBarButton, rotateBarButton], animated: true)
        
        cropView?.image = image
        cropView?.rotationGestureRecognizer.isEnabled = rotationEnabled
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if cropAspectRatio != 0 {
            cropView?.cropAspectRatio = cropAspectRatio
        }
        
        if !cropRect.equalTo(CGRect.zero) {
            adjustCropRect()
        }
        
        if !imageCropRect.equalTo(CGRect.zero) {
            cropView?.imageCropRect = imageCropRect
        }
        
        cropView?.keepAspectRatio = keepAspectRatio
    }
    
    open func resetCropRect() {
        cropView?.resetCropRect()
    }
    
    open func resetCropRectAnimated(_ animated: Bool) {
        cropView?.resetCropRectAnimated(animated)
    }
    
    @objc func rotateImage() {
        
        cropView?.imageView?.transform = (cropView?.imageView?.transform.rotated(by: CGFloat(Double.pi/2)))!
        
    }
    
    @objc func flipFlop() {
        
        //Creating flip menu
        flipCover = UIView(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        
        //Creating flipTapGesture and add target
        let flipTapGesture = UITapGestureRecognizer(target: self, action: #selector(closeFlip))
        //Setting to flipCover isUserInteractionEnabled true
        flipCover.isUserInteractionEnabled = true
        //Adding to flipCover gesture
        flipCover.addGestureRecognizer(flipTapGesture)
        
        //Getting statusBar instance
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        
        //Creating flip menu
        let flipMenu = UIView(frame:CGRect(x: flipCover.frame.width - 200, y: statusBar.frame.maxY, width: 200, height: 80))
        //Adding background to flipMenu
        flipMenu.backgroundColor = UIColor(red:0.12, green:0.12, blue:0.12, alpha:1.0)
        //Adding flipMenu into flipCover
        flipCover.addSubview(flipMenu)
        
        //Creating flipHorizontalLabel UILabel
        let flipHorizontalLabel = CustomLabel(frame: CGRect(x: 0, y: 0, width: flipMenu.frame.width, height: flipMenu.frame.height/2))
        //Setting font to text
        flipHorizontalLabel.font = UIFont(name: "Helvetica-bold", size: 16)
        //Setting color to text
        flipHorizontalLabel.textColor = UIColor.white
        //Setting text to TextView
        flipHorizontalLabel.text = NSLocalizedString("Flip_H", comment: "")
        //Adding flipHorizontalLabel to flipMenu
        flipMenu.addSubview(flipHorizontalLabel)
        
        //Creating flipHTapGesture and add target
        let flipHTapGesture = UITapGestureRecognizer(target: self, action: #selector(flipHorizontal))
        //Setting to flipHorizontalLabel isUserInteractionEnabled true
        flipHorizontalLabel.isUserInteractionEnabled = true
        //Adding to flipHorizontalLabel gesture
        flipHorizontalLabel.addGestureRecognizer(flipHTapGesture)
        
        //Creating flipVerticalLabel UILabel
        let flipVerticalLabel = CustomLabel(frame: CGRect(x: 0, y: flipHorizontalLabel.frame.maxY, width: flipMenu.frame.width, height: flipMenu.frame.height/2))
        //Setting font to text
        flipVerticalLabel.font = UIFont(name: "Helvetica-bold", size: 16)
        //Setting color to text
        flipVerticalLabel.textColor = UIColor.white
        //Setting text to TextView
        flipVerticalLabel.text = NSLocalizedString("Flip_V", comment: "")
        //Adding flipVerticalLabel to flipMenu
        flipMenu.addSubview(flipVerticalLabel)
        
        //Creating flipVTapGesture and add target
        let flipVTapGesture = UITapGestureRecognizer(target: self, action: #selector(flipVertical))
        //Setting to flipVerticalLabel isUserInteractionEnabled true
        flipVerticalLabel.isUserInteractionEnabled = true
        //Adding to flipVerticalLabel gesture
        flipVerticalLabel.addGestureRecognizer(flipVTapGesture)
        
        //Getting the window instance
        let window = UIApplication.shared.keyWindow!
        
        //Adding the flipCover into window
        window.addSubview(flipCover)
    }
    
    @objc func flipHorizontal() {
        
        switch self.flipH {
        case 0:
            self.flipX = -1
            
            cropView?.imageView?.transform = CGAffineTransform(scaleX: CGFloat(self.flipX), y: CGFloat(self.flipY))
            self.flipH = 1
            
        case 1:
            self.flipX = 1
            
            cropView?.imageView?.transform = CGAffineTransform(scaleX: CGFloat(self.flipX), y: CGFloat(self.flipY))
            self.flipH = 0
            
        default:
            break
        }
        
        closeFlip()
    }
    
    @objc func flipVertical() {
        
        switch self.flipV {
        case 0:
            self.flipY = -1
            
            cropView?.imageView?.transform = CGAffineTransform(scaleX: CGFloat(self.flipX), y: CGFloat(self.flipY))
            self.flipV = 1
            
        case 1:
            self.flipY = 1
            
            cropView?.imageView?.transform = CGAffineTransform(scaleX: CGFloat(self.flipX), y: CGFloat(self.flipY))
   
            self.flipV = 0
            
        default:
            break
        }
        
        closeFlip()
    }
    
    @objc func closeFlip() {
        flipCover.removeFromSuperview()
    }
    
    @objc func cancel() {
        delegate?.cropViewControllerDidCancel(self)
    }
    
    @objc func done() {
        if let image = cropView?.croppedImage {
            guard let rotation = cropView?.rotation else {
                return
            }
            guard let rect = cropView?.zoomedCropRect() else {
                return
            }
            delegate?.cropViewController(self, didFinishCroppingImage: image, transform: rotation, cropRect: rect)
        }
    }
    
    // MARK: - Private methods
    fileprivate func adjustCropRect() {
        imageCropRect = CGRect.zero
        
        guard var cropViewCropRect = cropView?.cropRect else {
            return
        }
        cropViewCropRect.origin.x += cropRect.origin.x
        cropViewCropRect.origin.y += cropRect.origin.y
        
        let minWidth = min(cropViewCropRect.maxX - cropViewCropRect.minX, cropRect.width)
        let minHeight = min(cropViewCropRect.maxY - cropViewCropRect.minY, cropRect.height)
        let size = CGSize(width: minWidth, height: minHeight)
        cropViewCropRect.size = size
        cropView?.cropRect = cropViewCropRect
    }
    
}
